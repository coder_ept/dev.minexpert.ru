<div id="crumbsBlock" style="display: none"><p><a style="color: white;" href="/">Главная </a> > Сотрудничество</p></div>
<div class="insidePage" id="insidePartners">
	<div id="insideCont" style="position: absolute; background-color: #fff; text-align: center; margin-top: 80px; display: none" class="wrap">
		
		
<?=$model->body?>
<p style="line-height: 30px;font-size: 25px; margin-bottom: 40px;">
	       Всем заинтересованным предлагаем связаться с нами по электронной почте: 
           <a style="color: red;" href="mailto: info@minexpert.ru">
           <span>info@minexpert.ru</span></a>
           <font style="font-size: 25px;"> или по бесплатной горячей линии <a style="color: red;" href="#callback" role="button" data-toggle="modal">8(800)707-8157</a></font>.
</p>
</div>
<img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/inside_bg1.png" class="insideBg">
</div>
<?php $this->renderPartial('//layouts/onlines');?>
<script type="text/javascript">
	$(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css({
				left: (ww - cw)/2,
				display: 'inline-block'
			});
            $('#crumbsBlock').css({
            	left: ((ww - cw)/2+lw-106),
            	display: 'block'
            });
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300,
				display: 'block'
			})
			$('.wrap').css({
				display: 'inline-block'
			});
		};
		//alert($('#insideCont').height());
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 150));

	})
	$(window).resize(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
            $('#crumbsBlock').css('left', ((ww - cw)/2+lw-106));
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300
			})
		};
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 150));
	})
    
</script>