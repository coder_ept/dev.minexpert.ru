<?php $this->pageTitle = "Стоимость экспертизы. ".$this->pageTitle ?>

<style>
	.next {
		margin-left: 50px;
	}
	#yw1 {
		display: none;
	}
</style>

<div id="crumbsBlock" style="display: none"><p><a style="color: white;" href="/">Главная </a> > Онлайн-сервисы</p></div>
<div class="insidePage" id="">
	<div id="insideCont" style="position: absolute; text-align: center; margin-top: 80px; display: none" class="wrap">
		
<table style="width: 100%; border-bottom: 1px solid #B8B8B8; border-right: 1px solid #B8B8B8;" id="calcBlock">
	<tr>
		<td class="onlinePic">
			<img src="/themes/default/web/images/base/kalkulyator.jpg" />
		</td>
		<td style="background-color: #fff; vertical-align: top; padding-right: 50px;">
			<h2>Калькулятор стоимости экспертизы</h2>
			<?php
            
$this->widget(
    'bootstrap.widgets.TbWizard',
    array(
        'type' => 'tabs', // 'tabs' or 'pills'
        'pagerContent'=>'<ul class="pager wizard" style="">
				<li class="previous first" style="display:none;"><a href="#">Вперед</a></li>
				<li class="previous"><a href="#">Назад</a></li>
				<li class="next last" style="display:none;"><a href="#">Last</a></li>
				<li class="next"><a href="#" style="" id="calc">Вперед</a></li>
			</ul>',
        'options' => array(
            'class' => '',
            'onTabShow' => 'js:function(tab, navigation, index) { if((index+1) > 1) {} }',
            'onTabClick' => 'js:function(tab, navigation, index) {return false;}',
        ),
        'tabs' => array(
            array(
                'label' => 'Тип экспертизы',
                'content' => '<form action="#" id="form1">
                <h3>Выберите тип экспертизы</h3>        			    
    			<label style="margin-top: 50px; margin-bottom: 20px;"><input type="radio" name="count" value="1" checked> <span style="font-size: 20px;">Первичная</span></label>
    			<label style="margin-bottom: 50px;"><input type="radio" name="count" value="2"> <span style="font-size: 20px;">Повторная</span></label>
    
    		</form>',
                'active' => true
            ),
            array('label' => 'Варинт экспертизы', 'content' => '	
            <form action="#" onsubmit="return false;" id="form2">
    
    			<div id="cl_0" style="display: none;">
    				<h3>Выберите вариант экспертизы</h3>
    
    				<label class="eveSt"><input type="radio" name="counting" value="1" checked> <span>Экспертиза результатов инженерных изысканий, выполняемых для строительства, реконструкции, капитального ремонта жилых объектов капитального строительства.</span></label>				
    				<label class="eveSt"><input type="radio" name="counting" value="2"> <span>Экспертиза проектной документации жилых объектов капитального строительства.</span></label>
    				<label class="eveSt"><input type="radio" name="counting" value="3"> <span>Экспертиза проектной документации жилых объектов капитального строительства и результатов инженерных изысканий, выполняемых для подготовки такой проектной документации.</span></label>    				
    				<label class="eveSt"><input type="radio" name="counting" value="4"> <span>Экспертиза проектной документации нежилых объектов капитального строительства и (или) результатов инженерных изысканий, выполняемых для подготовки такой проектной документации.</span></label>    				                   
    			</div>
                 
    		</form>'),
            array('label'=>'Форма','content'=>'
            <form>
            <div class="subCalculate" id="cl_1" style="display: none;">
    					<div><span>Площадь земли, измеряемая в пределах периметра жилого объекта капитального строительства (в&nbsp;м?)</span><br><input value="0" type="text" name="v_xg" maxlength="14"></div>
			</div>
            <div class="subCalculate" id="cl_2" style="display: none;">
    					<div><span>Площадь земли, измеряемая в пределах периметра жилого объекта капитального строительства (в&nbsp;м?)</span><br><input onkeyup="validation(this);" value="0" type="text" id="v_xg1" maxlength="14"></div>
    					<div><span>Общая площадь жилого объекта капитального строительства при его новом строительстве либо общая площадь помещений, подлежащих реконструкции, капитальному ремонту (в&nbsp;м?)</span><br><input onkeyup="validation(this);" type="text" id="v_yg1" maxlength="14"></div>
    
    					<h6>Назначение проектной документации</h6>
    					<label><input type="radio" name="counting1" value="1" checked> <span>проектная документация предназначена для строительства или реконструкции объекта капитального строительства</span></label>
    					<label><input type="radio" name="counting1" value="2"> <span>капитальный ремонт объекта капитального строительства</span></label>
    
			</div>
            <div class="subCalculate" id="cl_3" style="display: none;">
    					<div><span>Площадь земли, измеряемая в пределах периметра жилого объекта капитального строительства (в&nbsp;м?)</span><br><input value="0" onkeyup="validation(this);" type="text" id="v_xg2" maxlength="14"></div>
    					<div><span>Общая площадь жилого объекта капитального строительства при его новом строительстве либо общая площадь помещений, подлежащих реконструкции, капитальному ремонту (в&nbsp;м?)</span><br><input onkeyup="validation(this);" value="0" type="text" id="v_yg2" maxlength="14"></div>
    
    					<h6>Назначение проектной документации</h6>
    					<label><input type="radio" name="counting3" value="1" checked> <span>проектная документация предназначена для строительства или реконструкции объекта капитального строительства</span></label>
    					<label><input type="radio" name="counting3" value="2"> <span>капитальный ремонт объекта капитального строительства</span></label>
    
			</div>
            <div class="subCalculate" id="cl_4" style="display: none; ">
    					'.SpdCategory::getAll().'                        
    					<div id="d_cpd" style="display:none"><span>Стоимость изготовления проектной документации, представленной на экспертизу, в ценах 2001 года без НДС, в руб.</span><br><input  type="text" id="v_cpd" maxlength="14"></div>
    					<div id="d_cig" style="display:none"><span>Стоимость изготовления материалов инженерных изысканий, представленных на экспертизу, в ценах 2001 года без НДС, в руб.</span><br><input type="text" id="v_cig" maxlength="14"></div>
    		</div>
           
            <div class="rows" style="display:none;margin-bottom: 15%;" id="input_data"></div>
                     <div>
                        '.((Yii::app()->user->isGuest)?'
                            <br /><label>Введите email:
                            <input type="email" name="email" required />
                            </label><br />
                            <label>Введите номер телефона:
                            <input type="text" name="phone" required />
                            </label><br />
                        ':'').'                    
                        
                    </div> </form>'),
            array('label' => 'Результат', 'content' => '<div id="cl_result" style="display:none">
        		<h3 id="wo_nds">Стоимость услуги: <span></span></h3>    			
    		</div>'),
        ),
    )
);
?>        	        	
		</td>
	</tr>
</table>		

<table style="width: 100%; border-bottom: 1px solid #B8B8B8; border-right: 1px solid #B8B8B8;" id="calcBlock">
	<tr>
		<td class="onlinePic">
			<img src="/themes/default/web/images/base/Zvonok.jpg" />
		</td>
		<td style="background-color: #fff; vertical-align: top; padding-right: 50px;">
			<h2>Обратный звонок</h2>
			<p style="font-size: 20px; line-height: 24px; margin-bottom: 35px">Уважаемый посетитель сайта,<br> Вы можете заказать у нас обратный
звонок, выбрав удобное для Вас время для связи. Наш специалист
обязательнопозвонит Вам в назначенный час</p>
<a href="#callback" role="button" data-toggle="modal" id="onlineCallLink">Заказать звонок</a>
		</td>	
    </tr>	
</table>    			    

<table style="width: 100%; border-bottom: 1px solid #B8B8B8; border-right: 1px solid #B8B8B8;" id="calcBlock">
	<tr>
		<td class="onlinePic">
			<img src="/themes/default/web/images/base/Konsultatsia.jpg" />
		</td>
		<td style="background-color: #fff; vertical-align: top; padding-right: 50px;">
			<h2>Заказать консультацию</h2>
			<p style="font-size: 20px; line-height: 24px; margin-bottom: 35px">
            Если Вы хотите получить консультацию — воспользуйтесь формой
заказа консультации. В ней вы можете указать свой вопрос, чтобы мы
смогли подготовиться к консультации.</p>
<a href="#consult" role="button" data-toggle="modal" id="onlineCallLink">Заказать консультацию</a>
		</td>	
    </tr>	
</table>

<table style="width: 100%; border-bottom: 1px solid #B8B8B8; border-right: 1px solid #B8B8B8;" id="calcBlock">
	<tr>
		<td class="onlinePic">
			<img src="/themes/default/web/images/base/Elektronnaya_zayavka.jpg" />
		</td>
		<td style="background-color: #fff; vertical-align: top; padding-right: 50px;">
			<h2>Заявка в электронном виде</h2>
			<p style="font-size: 20px; line-height: 24px; margin-bottom: 35px">Если у вас уже имеются результаты расчетов через калькулятор сайта -
Вы можете подать заявку в электронном виде, выбрав в качестве 
исходных данных один из результатов ваших расчетов.</p>
<a href="#request" role="button" data-toggle="modal" id="onlineOrderLink">Подать заявку</a>
		</td>	
    </tr>	
</table>
 
 
	</div>
	<img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/inside_bg1.png" class="insideBg">
</div> 
 
 
 
    <script type="text/javascript">
    $(function() {
        $('#CallbackForm_number').mask('+7(999)999-99-99');
       $('#CallbackForm_time').timepicker({
                minuteStep: 5,
                showInputs: false,
               // showSeconds: true,
                showMeridian:   false
            }); 
		var ww = $(window).width();
		var cw = $('.wrap').width();
		var lw = $('#logo').width();
if (ww > cw) {
			$('.wrap').css({
				left: (ww - cw)/2,
				display: 'inline-block'
			});
            $('#crumbsBlock').css({
            	left: ((ww - cw)/2+lw-106),
            	display: 'block'
            });
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300,
				display: 'block'
			})
            $('.wrap').css({
                display: 'inline-block'
            });
		};
		
	})
	$(window).resize(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
		var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
			$('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300
			})
		};
		
	})
</script>