﻿<?php $this->pageTitle = "Негосударственная экспертиза сметной документации - МИНЭКС" ?>

<div id="crumbsBlock" style="display: none; margin-top: -20px;"><p><a style="color: white;" href="/">Главная </a> > Услуги</p></div>
<div class="insidePage" id="insideServList" style="margin-top: -20px;">
	<div id="insideCont" style="position: absolute; text-align: center; margin-top: 80px; display: none" class="wrap">

<?php foreach($category as $value):?>
    <?php if($value->parent_id==0):?>
        <?php if($value->id==6):?>
    <h1><?=$value->name?></h1>
        <table class="servicesTBlock" cellspacing="4">
            <?php foreach($value->categories as $subcat):?>
            <tr>
        		<td style="width: 76%;height: 220px;background-color: #fff; border-right: 1px solid #BABABA; border-bottom: 1px solid #BABABA; padding-top: 30px">
        			<table style="margin-bottom: 0">
        				<tr>
        					<td style="vertical-align: top">
        						<a href="/services/<?=$subcat->alias?>"><img src="<?php echo Yii::app() -> request -> baseUrl; ?>/uploads/category/<?=$subcat->image?>" style="height: 187px; max-width: 287px; width: 287px;"></a>
        					</td>
        					<td style="vertical-align: top">
        						<a href="/services/<?=$subcat->alias?>"><?=$subcat->name?></a>
        						<p style="margin-top: 20px"><?php $text=explode('.',strip_tags($subcat->short_description));
                                echo $text[0];
                                ?></p>
        					</td>
        				</tr>
        			</table>
        		</td>
        		<td style="width: 250px; background-color: #fff; border-right: 1px solid #BABABA; border-bottom: 1px solid #BABABA">
        			<a href="#callback" role="button" data-toggle="modal" style="display: block; font-size: 17px; font-weight: bold; color: #191919"><img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/rasschiatat.png" style="padding-right: 3px">Звонок</a>
        			<a href="#consult" role="button" data-toggle="modal" style="display: block; font-size: 17px; font-weight: bold; color: #191919"><img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/ostavit_zayavku.png" style="padding-right: 3px">Консультация</a>
        		</td>                
        	</tr>
            <?php endforeach;?>
        </table> 
        <?php else:?>
            <h1><?=$value->name?></h1>
        <table class="servicesTBlock" cellspacing="4">
            <?php foreach($value->categories as $subcat):?>
            <tr>
                <td style="width: 76%;height: 220px;background-color: #fff; border-right: 1px solid #BABABA; border-bottom: 1px solid #BABABA; padding-top: 30px">
                    <table style="margin-bottom: 0">
                        <tr>
                            <td style="vertical-align: top">
                                <a href="/services/<?=$subcat->alias?>"><img src="<?php echo Yii::app() -> request -> baseUrl; ?>/uploads/category/<?=$subcat->image?>" style="height: 187px; max-width: 287px; width: 287px;"></a>
                            </td>
                            <td style="vertical-align: top">
                                <a href="/services/<?=$subcat->alias?>"><?=$subcat->name?></a>
                                <p style="margin-top: 20px"><?php $text=explode('.',strip_tags($subcat->short_description));
                                echo $text[0];
                                ?></p>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 250px; background-color: #fff; border-right: 1px solid #BABABA; border-bottom: 1px solid #BABABA">
                    <a href="/online" style="display: block; font-size: 17px; font-weight: bold; color: #191919"><img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/rasschiatat.png" style="padding-right: 3px">Рассчитать стоимость</a>
                    <a href="/statement/index" role="button" data-toggle="modal" style="display: block; font-size: 17px; font-weight: bold; color: #191919"><img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/ostavit_zayavku.png" style="padding-right: 3px">Оставить заявку</a>
                </td>                
            </tr>
            <?php endforeach;?>
        </table> 



<?php endif;?>
    <?php endif;?>




<?php endforeach;?>
<table class="servicesTBlock" cellspacing="4">
<?php foreach($sub_cat as $value):?>    
        <tr>
            <td style="width: 76%;height: 220px;background-color: #fff; border-right: 1px solid #BABABA; border-bottom: 1px solid #BABABA; padding-top: 30px">
    			<table style="margin-bottom: 0">
    				<tr>
    					<td style="vertical-align: top">
    						<a href="/services/<?=$value->alias?>"><img src="<?php echo Yii::app() -> request -> baseUrl; ?>/uploads/category/<?=$value->image?>" style="height: 187px; max-width: 287px; width: 287px;"></a>
    					</td>
    					<td style="vertical-align: top">
    						<a href="/services/<?=$value->alias?>"><?=$value->name?></a>
    						<p style="margin-top: 20px"><?php $text=explode('.',strip_tags($value->short_description));
                            echo $text[0];
                            ?></p>
    					</td>
    				</tr>
    			</table>
    		</td>
    		<td style="width: 250px; background-color: #fff; border-right: 1px solid #BABABA; border-bottom: 1px solid #BABABA">
    			<a href="#callback" role="button" data-toggle="modal" style="display: block; font-size: 17px; font-weight: bold; color: #191919"><img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/rasschiatat.png" style="padding-right: 3px">Звонок</a>
    			<a href="#consult" role="button" data-toggle="modal" style="display: block; font-size: 17px; font-weight: bold; color: #191919"><img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/ostavit_zayavku.png" style="padding-right: 3px">Консультация</a>
    		</td>
        </tr>    
<?php endforeach;?>
</table>

<!--div id="mainServices">
	<div id="mainServicesCont" class="wrap">
		<span class="mainBlockLabel">Услуги института</span>
		<table>
			<tr>
				<td style="width: 400px">
					<ul id="sm">            
                        <?php foreach($category as $cat):?>
                            <?php if($cat->parent_id==0): ?>  
                                <li class="sm"><?=(count($cat->categories)>0)?'<img src="/themes/default/web/images/base/bt.jpg" style="margin-left: -10px; margin-top: -4px; padding-right: 7px">'.$cat['name']:CHtml::link($cat['name'],array("/site/services?alias=".$cat->alias),array('rel'=>$cat->alias));?></li>
                                  <?php if (count($cat->categories)>0):?>                          
                                    <ul>                            
                                        <?php foreach ($cat->categories as $value):?>
                                              <li id="msb1" class="ss "> <?=CHtml::link($value['name'],array("/services/".$value->alias),array('class'=>'msb','rel'=>$value->alias));?></li>        
                                        <?php endforeach; ?>
                                    </ul>
                                  <?php endif;?>
                            <?php endif;?>
                        <?php endforeach;?>    				
                    </ul>
				</td>
				<td>
					<?=$body?>                 		
				</td>
			</tr>
		</table>
	</div>
</div-->

</div>
	<img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/inside_bg1.png" class="insideBg">
</div>
<?php $this->renderPartial('//layouts/onlines');?>


<script>
/*function get(name){
   if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
      return decodeURIComponent(name[1]);
}*/
$(document).ready(function(){
    var ww = $(window).width();
		var cw = $('.wrap').width();
		var lw = $('#logo').width();
		if (ww > cw) {
            $('.wrap').css({
                left: (ww - cw)/2,
                display: 'inline-block'
            });
            $('#crumbsBlock').css({
                left: ((ww - cw)/2+lw-106),
                display: 'block'
            });
        } else {
            $('#crumbsBlock').css({
                left: lw-106,
                width: 300,
                display: 'block'
            })
            $('.wrap').css({
                display: 'inline-block'
            });            
        };	
        var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 100));
/* if (get('alias')==null){
    $('.msb:eq(0)').addClass('mainActiveServBut');
    $('.msb:eq(0)').parent().css('background-color','rgb(247, 247, 247)');
 }else
 {
    $('a.msb[rel="'+get('alias')+'"]').addClass('mainActiveServBut');
    $('a.msb[rel="'+get('alias')+'"]').parent().css('background-color','rgb(247, 247, 247)');
 }
*/    
});
$(window).resize(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
		var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
			$('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 200
			})
		};
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 100));
	})
</script>