<?php
$this->pageTitle   = 'Контакты';
?>
<script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCRX-iSNj7XPnZ4Ip_egadFOCaC-ugMKvo&sensor=true">
    </script>
    <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true&libraries=visualization">
    </script>
<style>
    #aboutMenu {margin-top: -90px!important;}
	#insideCont {margin-top: 140px!important;}
	#aboutMI4 {background-color: #474747!important;}
	#aboutMI4 a {color: #fff!important; border-bottom: none}
 </style>
 <div id="crumbsBlock"><p><a style="color: white;" href="/">Главная </a> > Контакты</p></div>
 
<div class="insidePage" id="contactsPage" style="background-color: #d9d9d9">
	<div id="insideCont" style="position: absolute; background-color: #fff; text-align: center; margin-top: 80px; text-align: left" class="wrap">
    
    <ul id="aboutMenu">
    	<li id="aboutMI1"><a href="/pages/about">О компании</a></li>
    	<li id="aboutMI2"><a href="/pages/oficialno">Официально</a></li>
    	<li id="aboutMI3"><a href="/pages/eksperty">Эксперты</a></li>
    	<li id="aboutMI4"><a href="/pages/kontakty">Контакты</a></li>
    </ul>
    <div id="map_canvas"  style="width:90%; height:500px; margin-left: 5%; margin-top: 100px"></div>
    <div style="width:90%;margin-left: 5%; margin-top: 30px; margin-bottom: 50px">
    <?php foreach($models as $value):?>
    	<h3><?=$value->name?></h3>
    	<table>
    		<tr>
    			<td style="padding: 0 25px 0 0; vertical-align: top "><img src="/uploads/map/<?=$value->image?>" style="height: 211px; max-width: 280px; width: 280px;"></td>
    			<td style="padding: 0; vertical-align: top; font-size: 20px; line-height: 26px;">
    				<?=$value->description?>
    			</td>
    		</tr>
    	</table>
    	<?php endforeach;?>    
    </div>
</div>
<img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/inside_bg1.png" class="insideBg">
</div>
<?php $this->renderPartial('//layouts/onlines');?>
<script type="text/javascript">



function initialize() {
	
						/*var styles = [
  {
    "featureType": "landscape",
    "stylers": [
      { "hue": "#ff0011" },
      { "saturation": -37 }
    ]
  },{
    "featureType": "water",
    "stylers": [
      { "saturation": -100 },
      { "lightness": -31 }
    ]
  }
];*/

        var centremap = new google.maps.LatLng(56.8375237, 60.6032718);
        var mapOptions = {
          center: centremap,
          zoom: 8,
          minZoom: 3,
          maxZoom: 18,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
	        			var styledMapOptions = {
					    name: 'Custom Style'
					  	};
					  	//map.setOptions({styles: styles});
        var t = new Date().getTime();
	var ctaLayer = new google.maps.KmlLayer({
            url: 'http://minexpert.ru/uploads/documents/structure-all.kml?' + t
            });
            
       ctaLayer.setMap(map);
      }
window.onload = initialize;

	$(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
		var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
			$('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 200
			})
      $('.wrap').css({
        display: 'inline-block'
      });
		};
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 200));
		
	})
	$(window).resize(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
		var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
			$('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 200
			})
		};
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 200));
		
	})
</script>