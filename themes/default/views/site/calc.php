﻿<?php $this->pageTitle = "Стоимость проведения экспертизы проектной документации - МИНЭКС" ?>
<style>
span.combosex_main.combosex_with_append.combosex_text {
width: 268px;
}
.combosex_input_span .combosex_ph {
position: absolute;
left: 44px;
top: 8px;
}
</style>
<script src="/themes/default/web/js/jquery.combosex.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="/themes/default/web/css/jquery.combosex.css" />
<div id="crumbsBlock" style="display: none; margin-top: -20px;"><p><a style="color: white;" href="/">Главная </a> > Онлайн-сервисы</p></div>
<div class="insidePage" style="margin-top: -20px;">
	<div id="insideCont" style="position: absolute; text-align: center; margin-top: 80px; display: none" class="wrap">
		
<table ng-controller="CalcController" style="width: 100%; border-bottom: 1px solid #B8B8B8; border-right: 1px solid #B8B8B8;" id="calcBlock">
	<tr>
		<td class="onlinePic">
			<img src="/themes/default/web/images/base/kalkulyator.jpg" />
		</td>
		<td style="background-color: #fff; vertical-align: top; padding-right: 50px;">
			<h2>Калькулятор стоимости экспертизы</h2>
            
			<?php
            
$this->widget(
    'bootstrap.widgets.TbWizard',
    array(
        'type' => 'tabs', // 'tabs' or 'pills'
        'pagerContent'=>'<ul class="pager wizard" style="">
				<li class="previous first" style="display:none;"><a href="#">Вперед</a></li>
				<li class="previous"><a href="#">Назад</a></li>
				<li class="next last" style="display:none;"><a>Last</a></li>
				<li class="next"><a style="" id="calc">Вперед</a></li>
			</ul>',
        'options' => array(
            'class' => '',
            'onTabShow' => 'js:function(tab, navigation, index) { if((index+1) > 1) {} }',
            'onTabClick' => 'js:function(tab, navigation, index) {return false;}',
            //'showNext'=>'js:alert(123)'
        ),
        'tabs' => array(
            array(
                'label' => 'Тип экспертизы',
                'content' => '<form action="#" id="form1">
                <h3>Выберите тип экспертизы</h3>        			    
    			<label style="margin-top: 50px; margin-bottom: 20px;"><input type="radio" name="count" value="1" checked> <span style="font-size: 20px;">Первичная</span></label>
    			<label style="margin-bottom: 50px;">
                <input type="radio" name="count" value="2"> <span style="font-size: 20px;">Повторная</span></label>
    
    		</form>',
                'active' => true
            ),
            array(
                'label' => 'Тип экспертизы',
                'content' => '<form action="#" id="form11">
                <h3>Выберите вид объекта</h3>        			    
                
    			<label style="margin-top: 50px; margin-bottom: 20px;">
                <input type="radio" name="object" value="1" ng-click="setCategory(1)"  checked> 
                    <span style="font-size: 20px;">Жилой</span>
                </label>    	
                <label style="margin-bottom: 20px;">	
                <input type="radio" name="object" ng-click="setCategory(2)" value="2"> 
                    <span style="font-size: 20px;">Не жилой</span>
                </label>    
                <label style="margin-bottom: 20px;">		
                <input type="radio" name="object" ng-click="setCategory(3)" value="3"> 
                    <span style="font-size: 20px;">Линейный<small style="font-size: 77%;">(Данный вид расчета оговаривается индивидуально)</small></span>
                </label>    		
    		</form>'                
            ),
            array('label' => 'Варинт экспертизы', 'content' => '	
            <form action="#" onsubmit="return false;" id="form2">
    
    			<div id="cl_0" style="display: none;">
    				<h3>Выберите вариант экспертизы</h3>
                    <div class="live" ng-show="category==1">
                        <label class="eveSt"><input type="radio" name="counting" value="1" checked> <span>Результаты инженерных изысканий.</span></label>				
        				<label class="eveSt"><input type="radio" name="counting" value="2"> <span>Проектная документация без сметы.</span></label>
        				<label class="eveSt"><input type="radio" name="counting" value="3"> <span>Проектная документация и результаты инженерных изысканий.</span></label>
          				<label class="eveSt"><input type="radio" name="counting"  ng-click="setVariant(4);" value="4"> <span>Смета.<small>(Данный вид расчета оговаривается индивидуально)</small></span></label>
                    </div>
                    <div class="not_live" ng-show="category==2">
        				<label class="eveSt"><input type="radio" name="counting" value="1" ng-click="setVariant(3)"> <span>Результаты инженерных изысканий.<small>(Данный вид расчета оговаривается индивидуально)</small></span></label>				  
                        <label class="eveSt"><input type="radio" name="counting" ng-click="setVariant(2)" value="4"> <span>Проектная документация без сметы.</span></label>  				    				
        				<label class="eveSt"><input type="radio" name="counting" ng-click="setVariant(1)" value="4"> <span>Проектная документация и результаты инженерных изысканий.</span></label>
                        <label class="eveSt"><input type="radio" name="counting" onclick="return false;" ng-click="setVariant(4);" value="4"> <span>Смета.<small>(Данный вид расчета оговаривается индивидуально)</small></span></label>
                    </div>                        				                   
    			</div>
                 
    		</form>'),
            array('label'=>'Форма','content'=>$this->renderPartial('_calc',false,true)),
            
                    array('label'=>'Город','content'=>'
                    <h3>Выберите место строительства</h3>    
                    <div id="seism_zone">
                        <div class="row">
                        <span class="span2">Выберите регион:</span>
                            '.CHtml::dropDownList('region',array(),
                                CHtml::listData(SeismicZonesRegion::model()->findAll(array('order'=>'name')),'id','name'),
                                array("class"=>'span3')).'
                        </div><br>
                         <div class="row">
                        <span class="span2">Выберите город:</span>
                            '.CHtml::dropDownList('zones',array(),
                                CHtml::listData(SeismicZones::model()->findAll("region_id = 20"),'id','city'),
                                array("class"=>'span3','style'=>'display:none')).'
                               <input type="text" id="zones_1" placeholder="Выберите город" />  
                        </div>
                    </div>
                    '),
                    
                    array('label'=>'Пользовательские данные','content'=>'
                     <h3>Введите основные параметры проекта</h3>  
                     <div>
                        '.((Yii::app()->user->isGuest)?'
                            <br /><label>Введите email:
                            <input type="email" name="email" required />
                            </label><br />
                            <label>Введите номер телефона:
                            <input type="text" name="phone" required />
                            </label><br />
                        ':'').'                    
                        
                    </div>
                    '),
                    
            array('label' => 'Результат', 'content' => '<div id="cl_result" style="display:none">
        		<h3 id="wo_nds">Стоимость: <span></span></h3>
                <h4>
                Ваш расчёт добавлен в <a href="/user/cabinet/index">Личный кабинет</a><br>
                Расчет является ориентировочным!<br> 
                Точные данные будут после рассмотрения пакета документов.<br>
                
                </h4> 			
    		</div>'),
        ),
    )
);
?>        	        	
		</td>
	</tr>
</table>		

<table style="width: 100%; border-bottom: 1px solid #B8B8B8; border-right: 1px solid #B8B8B8;" id="calcBlock">
	<tr>
		<td class="onlinePic">
			<img src="/themes/default/web/images/base/Zvonok.jpg" />
		</td>
		<td style="background-color: #fff; vertical-align: top; padding-right: 50px;">
			<h2>Обратный звонок</h2>
			<p style="font-size: 20px; line-height: 24px; margin-bottom: 35px">Уважаемый посетитель сайта,<br> Вы можете заказать у нас обратный
звонок, выбрав удобное для Вас время для связи. Наш специалист
обязательно позвонит Вам в назначенный час</p>
<a href="#callback" role="button" data-toggle="modal" id="onlineCallLink">Заказать звонок</a>
		</td>	
    </tr>	
</table>    			    

<table style="width: 100%; border-bottom: 1px solid #B8B8B8; border-right: 1px solid #B8B8B8;" id="calcBlock">
	<tr>
		<td class="onlinePic">
			<img src="/themes/default/web/images/base/Konsultatsia.jpg" />
		</td>
		<td style="background-color: #fff; vertical-align: top; padding-right: 50px;">
			<h2>Заказать консультацию</h2>
			<p style="font-size: 20px; line-height: 24px; margin-bottom: 35px">
            Если Вы хотите получить консультацию — воспользуйтесь формой
заказа консультации. В ней вы можете указать свой вопрос, чтобы мы
смогли подготовиться к консультации.</p>
<a href="#consult" role="button" data-toggle="modal" id="onlineCallLink">Заказать консультацию</a>
		</td>	
    </tr>	
</table>

<table style="width: 100%; border-bottom: 1px solid #B8B8B8; border-right: 1px solid #B8B8B8;" id="calcBlock">
	<tr>
		<td class="onlinePic">
			<img  src="/themes/default/web/images/hand.png" />
		</td>
		<td style="background-color: #fff; vertical-align: top; padding-right: 50px;">
			<h2>Заявка в электронном виде</h2>
			<p style="font-size: 20px; line-height: 24px; margin-bottom: 35px">Если у вас уже имеются результаты расчетов через калькулятор сайта -
Вы можете подать заявку в электронном виде, выбрав в качестве 
исходных данных один из результатов ваших расчетов.</p>
<a href="/statement/index" id="onlineOrderLink">Подать заявку</a>
		</td>	
    </tr>	
</table>
 
 
	</div>
	<img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/inside_bg1.png" class="insideBg">
</div> 
 
 
 
    <script type="text/javascript">
    $(function() {
                var ww = $(window).width();
        var cw = $('.wrap').width();
        var lw = $('#logo').width();
if (ww > cw) {
            $('.wrap').css({
                left: (ww - cw)/2,
                display: 'inline-block'
            });
            $('#crumbsBlock').css({
                left: ((ww - cw)/2+lw-106),
                display: 'block'
            });
        } else {
            $('#crumbsBlock').css({
                left: lw-106,
                width: 300,
                display: 'block'
            })
            $('.wrap').css({
                display: 'inline-block'
            });            
        };
        $('#zones_chzn').remove();
    <?php if(!Yii::app()->user->isGuest):?>
    $('#yw0_tab_6').remove();
	var counter=1;
    $('.tab-content div.tab-pane').each(function(){
    $(this).attr('id','yw0_tab_'+counter);counter++;
    });
    valid_tab=5;
    redirect_tab=6;	
    <?php endif;?>
      var zones=[];
var counter=0;
$.each($('#zones option'),
  function(key,val){
  zones[counter]=$(val).text();
  counter++;
  }
);
zones=JSON.parse(JSON.stringify(zones));
$('#zones_1').combosex({options:zones});

	})
	$(window).resize(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
		var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
			$('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300
			})
		};
		
	})
</script>