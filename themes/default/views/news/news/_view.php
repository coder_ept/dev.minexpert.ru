<a href="/story/<?=$data->alias?>" style="color:black">
<table class="insideDocsBlock">
	<tr>
		<td style="vertical-align: top">
			<img src="/uploads/news/<?=$data->image?>" style="max-width: 350px; width: 350px; height: 250px; padding: 10px 0 10px 10px;" />
		</td>
		<td style="vertical-align: top">
			<h3><?=$data->title?></h3>
            <span class="icon-calendar"> <b><?=Registr::rdate($data->date).' '.date('Y',strtotime($data->date)).' г.'?></b></span><br /><br />            
            <p><?=mb_substr(strip_tags($data->full_text),0,760,'UTF-8').((strlen($data->full_text)>760)?'...':'')?></p> 
		</td>
		<td style="width: 10px; padding: 0; background-color: rgba(0,0,0,0); background-image: url('<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/triangle_white.png');"></td>
	</tr>
	<tr><td colspan="3" style="padding: 0; height: 10px; background-color: rgba(0,0,0,0)"><img style="width: 100%; opacity: .8" src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/shadow_right.png"></td></tr>
</table>
</a>


