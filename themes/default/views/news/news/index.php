<?php $this->pageTitle = "Новости. ".$this->pageTitle ?>

<div id="crumbsBlock" style="display: none"><p><a style="color: white;" href="/">Главная </a> > Новости</p></div>

<div class="insidePage" id="insideNewsList">
	<div id="insideCont" style="position: absolute; text-align: center; margin-top: 80px; display: none" class="wrap">




    <?php $this->widget('zii.widgets.CListView', array(
          'dataProvider' => $dataProvider,
          'itemView' => '_view',
          'template'=>'{items}<br>{pager}'
    )); ?>


	</div>
	<img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/inside_bg1.png" class="insideBg">
</div>

   <script type="text/javascript">
	$(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
		var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css({
				left: (ww - cw)/2,
				display: 'inline-block'
			});
            $('#crumbsBlock').css({
            	left: ((ww - cw)/2+lw-106),
            	display: 'block'
            });
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300,
				display: 'block'
			})
			$('.wrap').css({
				display: 'inline-block'
			});			
		};
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 200));
		
	})
	$(window).resize(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
		var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
			$('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 200
			})
		};
		
	})
</script>