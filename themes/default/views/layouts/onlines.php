<script src="/themes/default/web/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/themes/default/web/js/bootstrap-timepicker.min.js"></script>
<div id="mainOnlineS">
<span class="mainBlockLabel">Онлайн сервисы</span>
	<ul>
		<li>
			<a href="#callback" role="button" data-toggle="modal" >
			<div class="mainOnlineText" style="padding-top: 10px;">Заказать звонок</div>
			<img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/onlineserv_phone.png">
			</a>
		</li>
		<li>
			<a href="/online">
			<div class="mainOnlineText">Рассчитать<br>стоимость</div>
			<img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/onlineserv_calc.png">
			</a>
		</li>
		<li>
			<a href="/statement/index">
			<div class="mainOnlineText">Подать заявку<br>в электронном виде</div>
			<img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/onlineserv_order1.png">
			</a>
		</li>
		<li>
			<a href="#consult" role="button" data-toggle="modal">
			<div class="mainOnlineText" style="padding-top: 10px;">Заказать консультацию</div>
			<img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/onlineserv_cons.png">
			</a>
		</li>
	</ul>
</div>
<script>
$(function(){
    //$('#CallbackForm_number, #ConsultForm_number').mask('+7(999)999-99-99');
    $('#CallbackForm_time').timepicker({
                minuteStep: 5,
                showInputs: false,
              //showSeconds: true,
                showMeridian:   false
            });
   
});
</script>