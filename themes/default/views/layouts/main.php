﻿<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" ng-app>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />
	<meta name="keywords" content="<?php echo $this->keywords; ?>"/>
	<meta name="description" content="<?php echo $this->description; ?>"/>
	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/css/print.css" media="print" />
	<!--[if lte IE 8]>
	<link rel="stylesheet" href="/themes/default/web/css/ie.css" media="screen, projection"/>
	<![endif]-->
<link type="text/css" href="/themes/default/web/css/bootstrap-timepicker.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/css/form.css" />
    <link type="text/css"  rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/js/smslider/smslider.css" />	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/js/sliderkit/lib/css/sliderkit-core.css" media="screen, projection" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/js/sliderkit/lib/css/sliderkit-demos.css" media="screen, projection" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/js/combosex/jquery.combosex.css" />    
    
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/js/smslider/jquery.smslider.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/js/sliderkit/lib/js/external/jquery.easing.1.3.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/js/sliderkit/lib/js/external/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/js/sliderkit/lib/js/sliderkit/jquery.sliderkit.1.9.2.pack.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/js/combosex/jquery.combosex.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/js/main.js"></script>
<link rel="stylesheet" type="text/css" href="/themes/default/web/css/chosen.css" />
<?php
$mainAssets = Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('application.modules.yupe.extensions.booster.assets'));
	 //Yii::app()->clientScript->registerScriptFile($mainAssets . '/js/bootstrap.timepicker.js');
?>
</head>
<body>
<style>
div.bootstrap-timepicker-widget.dropdown-menu.timepicker-orient-left.timepicker-orient-top.open {
    min-width: 100px;
}
</style>
<?php
Yii::app()->clientScript->registerScriptFile('/themes/default/web/js/chosen.jquery.js');
 $this->renderPartial('//layouts/callform');?>
<div style="display:none;"><?php $this->widget('application.modules.yupe.widgets.YAdminPanel'); ?></div>



<div id="menuWrap">
<div id="mainmenu" class="wrap">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Главная', 'url'=>array('/site/index')),
				array('label'=>'О компании', 'url'=>array('/pages/about')),
                array('label'=>'Услуги', 'url'=>array('/site/services')),
                array('label'=>'Документы', 'url'=>array('/documents')),
                array('label'=>'Реестр', 'url'=>array('/reestr')),
                array('label'=>'Сотрудничество', 'url'=>array('/partnership')),
                array('label'=>'Онлайн-сервисы', 'url'=>array('/online')),
                array('label'=>'Новости', 'url'=>array('/news')),
                array('label'=>'FAQ', 'url'=>array('/feedback/faq')),
                array('label'=>'Контакты', 'url'=>array('/kontakty')),
                //array('url'=>Yii::app()->getModule('user')->loginUrl, 'label'=>Yii::app()->getModule('user')->t("Login"), 'visible'=>Yii::app()->user->isGuest),
                //array('url'=>Yii::app()->getModule('user')->registrationUrl, 'label'=>Yii::app()->getModule('user')->t("Register"), 'visible'=>Yii::app()->user->isGuest),
                //array('url'=>Yii::app()->getModule('user')->profileUrl, 'label'=>Yii::app()->getModule('user')->t("Profile"), 'visible'=>!Yii::app()->user->isGuest),
                //array('url'=>Yii::app()->getModule('user')->logoutUrl, 'label'=>Yii::app()->getModule('user')->t("Logout").' ('.Yii::app()->user->name.')', 'visible'=>!Yii::app()->user->isGuest),

				//array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				//array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	<!--form id="searchForm">
		<input placeholder="Поиск"/>
	</form-->	
	</div><!-- mainmenu -->
</div>	
<div id="page">
	<div id="headerWrap">
	<div id="header" class="wrap">
        <div id="logo"><a href="/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/logo_head2.png" alt="Межрегиональный институт экспертизы 'МИНЭКС'"/></a></div>
        <div id="headerRight">
        	<div id="headerLinks">
		 
		<?php if (Yii::app()->user->isGuest):?>
			
			<a class="headerPict" href='/login' title="Войти" style="color: #FF351D; border: 1px solid #FF351D; border-radius: 6px; padding: 5px 10px">ВОЙТИ<img style="margin-left: 5px" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/login_r.png"></a>
			<a href='/registration' title="Регистрация"><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/register.png"></a>
		<?php else:?>
			
			<a  class="headerPict" style="color: #333; border: 1px solid #fff; border-radius: 6px; padding: 5px 10px" href='/user/cabinet/index' title="Личный кабинет">КАБИНЕТ<img style="margin-left: 5px"  src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/cabinet.png"><a/>
				<a href='/logout' title="Выход"><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/logout.png"></a>
		<?php endif;?>
		<a href='/feedback/index' title="Обратная связь"><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/feed.png"></a>
		</div>
        	<div id="headerPhone"><a href="#callback" role="button" data-toggle="modal" > +7 (800) 707-81-57</a></div>
        	
        		
        </div>
        
        



	</div><!-- header -->
</div>
	
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>
<script src="/themes/default/web/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/themes/default/web/js/bootstrap-timepicker.min.js"></script>
	<?php echo $content; ?>
 

	<div class="clear"></div>

	<div id="footer">
        <div id="footerCont" class="wrap">
        <table>
            <tr>
                <td>
                    
                    <b>Услуги</b><br>
                    Негосударственная экспертиза:<br>
                    <a href="http://minexpert.ru/services/ekspertiza-proektnoj-dokumentacii-i-rezultatov-inzhenernyh-izyskanij">- проектной документации и результатов инженерных изысканий</a><br>
                    <a href="http://minexpert.ru/services/proektnoj-dokumentacii">- проектной документации</a><br>
                    <a href="http://minexpert.ru/services/rezultatov-inzhenernyh-izyskanij">- результатов инженерных изысканий</a><br>
                    <a href="http://minexpert.ru/services/smetnoj-dokumentacii">- сметной документации</a><br>
                    <a href="http://minexpert.ru/services/audit-i-soprovozhdenie-dokumentacii">Аудит и сопровождение документации</a><br>
                    <a href="http://minexpert.ru/services/sluzhba-tehnicheskogo-zakazchika">Служба технического заказчика</a><br>
                    
                </td>
                <td>
                    <b>Документы</b><br>
                    <a href="/pages/reglament">Регламент</a><br>
                    <a href="/pages/organy-upravlenija">Органы управления</a><br>
                    <a href="/documents">Нормативная документация</a><br>
                    <a href="/documents">Документы для заявителей</a><br>
                    
                </td>
                <td>
                    <b>Онлайн-сервисы</b><br>
                    <a href="#callback" role="button" data-toggle="modal" >Заказать звонок</a><br>
                    <a href="/online">Онлайн-калькулятор</a><br>
                    <a href="/statement/index">Подать заявку в электронном виде</a><br>
                    <a href="#consult" role="button" data-toggle="modal">Заказать консультацию</a><br><br>
                </td>
                <td>
                	<a href="/pages/about"><b>О компании</b></a><br>
                	<a href="/pages/oficialno">Официально</a><br>
                    <a href="/pages/eksperty">Наши эксперты</a><br>
                    <a href="/reestr">Реестр выданных заключений</a><br>
                    <a href="/news"><b>Новости</b></a><br>
                    <a href="/partnership"><b>Сотрудничество</b></a><br>
                    <a href="/feedback/faq"><b>FAQ</b></a><br>
                    <a href="/kontakty"><b>Контакты</b></a><br>
                </td>

            </tr>
            <tr>
                <td colspan="5">ОО "МИНЭКС", Центральный офис: г.Москва, ул. Садовники, д.2, оф.1111, ИНН 7719849195 ОГРН 1137746552041 email: <a href="mailto: info@minexpert.ru">info@minexpert.ru</a> &copy;<?=date('Y');?><br />Проведение негосударственной экспертизы проектной документации так же в следующих филиалах: Санкт-Петербург, Тверь, Нижний Новгород, Киров, Пенза, Владивосток, Пермь, Екатеринбург, Череповец, Ростов-на-Дону, Ессентуки, Крым</td>            </tr>
        </table>
        </div>
	</div><!-- footer -->

</div><!-- page -->

  <script  type="text/javascript">
$(document).ready(function(){
    
    $('#CallbackForm_city').chosen();
    $('#ConsultForm_city').chosen();
   // $('#RegistrationForm_location').chosen();
    $('#CallbackForm_number, #ConsultForm_number').mask('+7 (9999) 99-99-99');
    $('#CallbackForm_time').timepicker({
                minuteStep: 5,
                showInputs: false,
           //     showSeconds: true,
                showMeridian:false
            }); 
    var currentPath = window.location.pathname;
  $("#mainmenu ul li a").each(function() {
     var src = $(this).attr("href");
     if (src == currentPath) {
       $(this).parent().addClass('active');
     }
     
  });
  
});
</script>


</body>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter24623507 = new Ya.Metrika({id:24623507,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/24623507" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</html>
