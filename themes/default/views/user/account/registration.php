<?php
$this->pageTitle = Yii::t('user', 'Регистрация нового пользователя');

?>
<style>
#RegistrationForm_location_chzn a span
{
    color:black;
}
</style>
<div id="crumbsBlock"><p><a style="color: white;" href="/">Главная </a> > Регистрация</p></div>
<div class="insidePage" id="insideFaq">
	<div id="insideCont" style="position: absolute; background-color: #fff; text-align: center; margin-top: 80px;" class="wrap">

<h1 style="font-weight: normal; margin-bottom: 50px; margin-top: 30px;">Регистрация пользователя</h1>

<?php $this->widget('application.modules.yupe.widgets.YFlashMessages'); ?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array('id' => 'registration-form'));?>

    <?php echo $form->errorSummary($model); ?>


    <div class="row">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email', array('style'=>'width: 400px')) ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'first_name'); ?>
        <?php echo $form->textField($model, 'first_name', array('style'=>'width: 400px')) ?>
        <?php echo $form->error($model, 'first_name'); ?>
    </div>    
	<div class="row">
        <?php echo $form->labelEx($model, 'middle_name'); ?>
        <?php echo $form->textField($model, 'middle_name', array('style'=>'width: 400px')) ?>
        <?php echo $form->error($model, 'middle_name'); ?>
    </div>    
	<div class="row">
        <?php echo $form->labelEx($model, 'last_name'); ?>
        <?php echo $form->textField($model, 'last_name', array('style'=>'width: 400px')) ?>
        <?php echo $form->error($model, 'last_name'); ?>
    </div>
	<div class="row">
        <?php echo $form->labelEx($model, 'location'); ?>
        <?php echo $form->dropDownList($model, 'location',City::All(),array('empty'=>'Выберите город','class'=>'chzn-select','style'=>'width: 425px')) ?>
        <?php echo $form->error($model, 'location'); ?>
    </div>
	<div class="row">
        <?php echo $form->labelEx($model, 'company'); ?>
        <?php echo $form->textField($model, 'company', array('style'=>'width: 400px')) ?>
        <?php echo $form->error($model, 'company'); ?>
    </div>
    	<div class="row">
        <?php echo $form->labelEx($model, 'phone'); ?>
        <?php echo $form->textField($model, 'phone', array('style'=>'width: 400px')) ?>
        <?php echo $form->error($model, 'phone'); ?>
        </div>



    <div class="row-fluid control-group <?php echo $model->hasErrors('birth_date') ? 'error' : ''; ?>">
        <?php echo $form->labelEx($model, 'birth_date'); ?>
        <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'birth_date',
               // 'value'=>date('d.m.Y',strtotime($model->birth_date)),
               'language'=>Yii::app()->getLanguage(),
                 'htmlOptions' => array(
                    'style' => 'width: 400px',
                    'size' => '10',         // textField size
                    'maxlength' => '10',    // textField maxlength
                ), 
                'options'=>array(
                    'dateFormat' => 'dd.mm.yy', // save to db format
                   // 'altField' => '#self_pointing_id',
                    //'altFormat' => 'dd-mm-yy', // show to user format
					'changeMonth' => 'true',
					'changeYear'=>'true',                   
                    'yearRange'=> "1914:2014",
                    
                ), 
            ));
            ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'password'); ?>
        <?php echo $form->passwordField($model, 'password', array('style'=>'width: 400px'));?>
        <?php echo $form->error($model, 'password'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'cPassword'); ?>
        <?php echo $form->passwordField($model, 'cPassword', array('style'=>'width: 400px'));?>
        <?php echo $form->error($model, 'cPassword'); ?>
    </div>

    <div class="row submit">
        <?php echo CHtml::submitButton('Зарегистрироваться', array('id'=>'onlineOrderLink', 'style'=>'border: none; height: 44px; width: 240px; margin-top: 30px;')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->
</div>
	<img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/inside_bg1.png" class="insideBg">
</div>

<script type="text/javascript">
	$(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
            $('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		};
		$('.docm').css('width', (cw - 170));
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 150));

	})
	$(window).resize(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
            $('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		};
		$('.docm').css('width', (cw - 170));
	})
</script>