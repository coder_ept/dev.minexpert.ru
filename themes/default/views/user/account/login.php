<?php
$this->pageTitle = Yii::t('user', 'Авторизация');
?>
<div id="crumbsBlock"><p><a style="color: white;" href="/">Главная </a> > Авторизация</p></div>
<div class="insidePage" id="insideFaq">
	<div id="insideCont" style="position: absolute; background-color: #fff; text-align: center; margin-top: 80px;" class="wrap">
<?php Yii::app()->clientScript->registerScriptFile('http://connect.facebook.net/ru_RU/all.js'); ?>



<h1 style="font-weight: normal; margin-bottom: 50px; margin-top: 30px;">Авторизация</h1>




<?php $this->widget('application.modules.yupe.widgets.YFlashMessages'); ?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
                                                         'id' => 'login-form',
                                                         'enableClientValidation' => true
                                                    ));?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email') ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'password'); ?>
        <?php echo $form->passwordField($model, 'password') ?>
        <?php echo $form->error($model, 'password'); ?>
    </div>

    <?php if($this->getModule()->sessionLifeTime > 0):  ?>
    <div class="row rememberMe">
        <?php echo $form->checkBox($model, 'remember_me', array('style'=>'width: 16px!important; height: 16px!important')); ?>
        <?php echo $form->labelEx($model, 'remember_me'); ?>
        <?php echo $form->error($model, 'remember_me'); ?>
    </div>
    <?php endif; ?>

    <?php if (Yii::app()->user->getState('badLoginCount', 0) >= 3): ?>
        <div class='row'>
            <?php if (CCaptcha::checkRequirements('gd')): ?>
                <?php echo $form->labelEx($model, 'verifyCode'); ?>
                <div class='row-fluid'>
                    <?php $this->widget('CCaptcha', array('showRefreshButton' => true)); ?>
                </div>
                <div class='row-fluid'>
                    <?php echo $form->textField($model, 'verifyCode'); ?>
                    <?php echo $form->error($model, 'verifyCode'); ?>
                </div>
                <div class="hint">
                    <?php echo Yii::t('UserModule.user', 'Введите текст указанный на картинке'); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <p class="hint">
            <?php echo CHtml::link(Yii::t('user', "Регистрация"), array('/user/account/registration'), array('style'=>'color: #111')); ?>
            | <?php echo CHtml::link(Yii::t('user', "Восстановление пароля"), array('/user/account/recovery'), array('style'=>'color: #111')) ?>
        </p>
    </div>

    <div class="row submit">
        <?php echo CHtml::submitButton('Войти', array('style'=>'height: 40px; margin-top: 20px')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->

	</div>
	<img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/inside_bg1.png" class="insideBg">
</div>

<script type="text/javascript">
	$(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
            $('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300
			})
		};
		$('.docm').css('width', (cw - 170));
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 150));

	})
	$(window).resize(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
            $('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300
			})
		};
		$('.docm').css('width', (cw - 170));
	})
</script>