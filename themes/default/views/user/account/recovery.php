<?php
$this->pageTitle = Yii::t('user', 'Восстановление пароля');
?>
<div id="crumbsBlock"><p><a style="color: white;" href="/">Главная </a> > Восстановление пароля</p></div>
<div class="insidePage" id="insideFaq">
	<div id="insideCont" style="position: absolute; background-color: #fff; text-align: center; margin-top: 80px;" class="wrap">

<h1 style="font-weight: normal; margin-bottom: 50px; margin-top: 30px;">Восстановление пароля</h1>

<?php $this->widget('application.modules.yupe.widgets.YFlashMessages'); ?>


<p>Для восстановления пароля - введите email, указанный при регистрации.</p>

<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
                                                         'id' => 'recovery-password-form',
                                                         'enableClientValidation' => true
                                                    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email') ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>

    <div class="row submit">
        <?php echo CHtml::submitButton('Восстановить пароль', array('style'=>'height: 40px; margin-top: 20px')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->
</div>
	<img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/inside_bg1.png" class="insideBg">
</div>

<script type="text/javascript">
	$(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
            $('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300
			})
		};
		$('.docm').css('width', (cw - 170));
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 150));

	})
	$(window).resize(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
            $('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300
			})
		};
		$('.docm').css('width', (cw - 170));
	})
</script>