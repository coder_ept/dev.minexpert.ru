<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>Заявление</title>
		<meta name="description" content="">
		<meta name="author" content="AlekseyVB">

		<meta name="viewport" content="width=device-width; initial-scale=1.0">

		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<link rel="shortcut icon" href="/favicon.ico">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">
        <style>
        body {
            text-align: center;
        }
        </style>
	</head>

	<body>
		<div style="width: 100%; display: inline-block; text-align: left">

<div >
    <div style="float: right; width: 220px;">
        <p>
            <b>Генеральному директору</b><br>
            ООО «МИНЭКС»<br>
            Решетникову М.Ю.
        </p>
    </div>
</div>

      <?php
      $x = $model->full_type_project_organization;
      switch ($x) {
          case "Общество с ограниченной ответственностью":
              $model->full_type_project_organization="Обществом с ограниченной ответственностью";
              break;
          case "Открытое акционерное общество":
              $model->full_type_project_organization="Открытым акционерным обществом";
              break;
          case "Закрытое акционерное общество":
              $model->full_type_project_organization="Закрытым акционерным обществом";
              break;
          case "Индивидуальным предпринимателем":
              $model->full_type_project_organization="Индивидуальным предпринимателем";
              break;
      }
      ?>

<div style="clear:both; text-align: center;margin-bottom: 10px;">
    <strong>ЗАЯВЛЕНИЕ<br>
    о проведении негосударственной экспертизы<br>
    <?=$model->getTypeDocumention()?></strong>
</div>

<?=$model->a1_name_full?> "<?=$model->a1_name_short?>"
направляет на негосударственную экспертизу проектную документацию объекта
капитального строительства: "<?=$model->name_object_capital_construction?>", расположенного по адресу: "<?=$model->address_object?>", разработанную <?=$model->full_type_project_organization?> "<?=$model->full_name_project_organization?>"<br>
<br>
Первичность рассмотрения: <?=$model->firstTime?><br>
Оплата стоимости проведения негосударственной экспертизы гарантируется.<br>
Привлечение кредитных средств в Сбербанке РФ: <?=($model->source_of_finance==3)?"планируется" : "не планируется"?><br>
<br>
<strong>Платежные реквизиты:<br></strong>ИНН: <?=$model->plat_inn?><br>
<?php if($model->plat_kpp):?>
КПП: <?=$model->plat_kpp?><br>
<?php endif;?>    
ОГРН: <?=$model->plat_ogrn?><br>
Р/с: <?=$model->plat_rs?><br>
Банк: <?=$model->plat_bank?><br>
К/с: <?=$model->plat_ks?><br>
БИК: <?=$model->plat_bik?><br><br>

<strong>Приложения:</strong>
<ol style="font-size: 9px;">
    <li>Проектная документация на объект капитального строительства, соответствующая требованиям частей 12-14 статьи 48 Градостроительного кодекса и постановлению Правительства Российской Федерации от 16.02.2008 г. № 87, по описи на _____ л. в 1 экз.</li>
    <li>Результаты инженерных изысканий в соответствии с требованиями, установленными<br>
        законодательством Российской Федерации, по описи на  _____  л. в 1 экз.</li>
    <li>Анкета Заказчика на _____ л. в 1 экз.</li>
    <li>Заверенная копия задания на проектирование на _____  л. в 1 экз.</li>
    <li>Заверенная копия задания на выполнение инженерных изысканий на _____ л. в 1 экз.</li>
    <li>Копии свидетельств СРО проектировщиков и сопроектировщиков на _____ л. в 1 экз.</li>
    <li>Копия ГПЗУ (ППТ для линейных объектов) на _____  л. в 1 экз.</li>
    <li>Копии правоустанавливающих документов на земельный участок, объект капитального строительства</li>
    <li>Копии Технических условий на подключение к инженерным сетям на _____  л. в 1 экз.</li>
    <li>Акт обследования технического состояния объекта (при реконструкции и капитальном ремонте) и прилегающих зданий на _____  л. в 1 экз.</li>
    <li>Проектная документация, отчеты по инженерным изысканиям и исходно-разрешительная документация на электронном носителе или ссылка на скачивание полного комплекта документации.</li>
</ol>
<br>
<table>
<tr>
<td style="vertical-align: top;">Генеральный директор</td>
<td style="text-align: center">__________________________<br>(подпись)</td>
<td style="text-align: center"><?=$model->ceo_company?><br>(Ф.И.О.)</td>
</tr>
<tr>
<td style="vertical-align: top;">Главный бухгалтер</td>
<td style="text-align: center">__________________________<br>(подпись)</td>
<td style="text-align: center"><?=$model->chief_buhgalter?><br>(Ф.И.О.)</td>
</tr>
<tr><td style="text-align: center; padding-top: 20px;" colspan="3">М.П.</td>
</table>
          
         
 			
		</div>
	</body>
</html>
