<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>Доверенность</title>
		<meta name="description" content="">
		<meta name="author" content="AlekseyVB">

		<meta name="viewport" content="width=device-width; initial-scale=1.0">

		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<link rel="shortcut icon" href="/favicon.ico">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">
        <style>
        body {
            text-align: center;
        }
        #tab, #tab td, #tab th {
            border: 1px solid #555;
            border-collapse: collapse;
            padding: 5px 15px;
        }
        </style>
	</head>

	<body>
		<div style="width: 100%; display: inline-block; text-align: left">



<div style="text-align: center">
	<?=$model->name_object_capital_construction?><br><br>
	<?=$model->address_object?>
	
</div> 

<div style="text-align: center; font-weight: bold; margin-top:  30px">
	ОПИСЬ<br>
проектной документации и (или) результатов инженерных изысканий, передаваемых<br>
для проведения негосударственной экспертизы
</div>
 
 
<table id="tab" style="margin: 50px auto;width: 100%;">
	<tr>
		<th style="width: 15px;text-align: center">№ п/п</th>
		<th>Наименование документа</th>
		<th style="width: 50px;text-align: center">Количество экземпляров</th>
	</tr>	
	<?php  for($i=1;$i<=count(CJSON::decode($model->json_inventory));$i++){?>
		<tr>
			<td><?=$i?></td>
			<td><?=CJSON::decode($model->json_inventory)[$i]['nameDoc']?></td>
			<td><?=CJSON::decode($model->json_inventory)[$i]['countItems']?></td>
		</tr>
	<?php }?>
</table>




<table>
	<tr>
		<td><?=$model->a1_chiefpostt?></td>
		<td>________________</td>
		<td>&nbsp;</td>
		<td><?=$model->ceo_company?></td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td width="100">&nbsp;</td>
		<td>Ф.И.О.</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td style="text-align: center">М.П.</td>
		<td>&nbsp;</td>
	</tr>
</table>




		</div>
	</body>
</html>
