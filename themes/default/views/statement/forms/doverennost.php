<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>Доверенность</title>
		<meta name="description" content="">
		<meta name="author" content="AlekseyVB">

		<meta name="viewport" content="width=device-width; initial-scale=1.0">

		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<link rel="shortcut icon" href="/favicon.ico">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">
        <style>
        body {
            text-align: center;
        }
        #tab, #tab td, #tab th {
            border: 1px solid #555;
            border-collapse: collapse;
            padding: 5px 15px;
        }
        </style>
	</head>

	<body>
		<div style="width: 100%; display: inline-block; text-align: left">


<div style="text-align: center; font-weight: bold">
	ДОВЕРЕННОСТЬ № <?=$model->procuratory?>
</div><br>

<div style="float: right; margin-bottom: 20px;">

	<?php
	if($model->date_procuratory != '0000-00-00') {
		echo date('d.m.Y',strtotime($model->date_procuratory));
		} else echo "«____»_________ 201___ г." 
	?> 
</div>

<div style="clear: both">
<?=$model->a1_name_full?> <?=$model->a1_name_short?><br>
в лице <?=$model->a1_chieffio?><br>
действующего на основании <?=$model->doc_company?><br>
доверяет <?=$model->name_ur_person_procuratory?><br>
в лице <?=$model->post_fio_person_procuratory?><br>
действующего на основании <?=$model->document_person_procuratory?><br>

выступать от имени представляемого в качестве заявителя при обращении  в Общество
 с ограниченной ответственностью «Межрегиональный институт экспертизы»
 с заявлением о проведении негосударственной экспертизы {полное наименование проектной документации}<br>
с правом заключения, изменения, исполнения, расторжения договора на проведение негосударственной экспертизы, 
подписи актов сдачи-приемки выполненных работ, оплаты экспертных работ, получения сведений, заключения по объекту.<br>
<br>
Срок действия доверенности 	<?php
	if($model->date_procuratory != '0000-00-00') {
		echo date('d.m.Y',strtotime($model->date_procuratory));
		} else echo "«____»_________ 201___ г." 
	?> <br>

<br><br>
<table>
	<tr>
		<td>______________________</td>
		<td>______________________</td>
	</tr>
	<tr>
		<td></td>
		<td>М.П.</td>
	</tr>
</table>
 <br>
	         									
									<br><br>







</div>
		</div>
	</body>
</html>
