<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>Анкета заявителя</title>
		<meta name="description" content="">
		<meta name="author" content="AlekseyVB">

		<meta name="viewport" content="width=device-width; initial-scale=1.0">

		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<link rel="shortcut icon" href="/favicon.ico">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">
        <style>
        body {
            text-align: center;
        }
        #tab, #tab td, #tab th {
            border: 1px solid #555;
            border-collapse: collapse;
            padding: 5px 15px;
        }
        </style>
	</head>

	<body>
		<div style="width: 100%; display: inline-block; text-align: left">




<div style="clear:both; text-align: center">
    <strong>Приложение к заявлению на проведение негосударственной экспертизы<br>
    Анкета заказчика<br></strong>
</div>

<br>
<table id="tab">

<tr>
<th>Наименование сведений</th>
<th>Информация</th>
</tr>

<tr>
<td colspan="2" style="text-align: center; font-weight: bold">I. Сведения о заявителе (заказчике услуг негосударственной экспертизы)</td>
</tr>


<tr>
<td>
Полное и сокращенное наименование юридического лица</td>
<td>"<?=$model->a1_name_full.'  '.$model->a1_name_short?>"</td>
</tr>

<tr>
<td>Адрес организации</td>
<td><?=$model->a1_address?></td>
</tr>

<tr>
<td>Телефон организации</td>
<td><?=$model->a1_phone?></td>
</tr>

<tr>
<td>Контактное лицо от заявителя (менеджер объекта)</td>
<td><?=$model->a1_manager?></td>
</tr>

<tr>
<td>телефон контактного лица</td>
<td><?=$model->a1_manager_phone?></td>
</tr>

<tr>
<td>e-mail контактного лица (для направления замечаний)</td>
<td><?=$model->a1_manager_email?></td>
</tr>

<tr>
<td colspan="2" style="text-align: center; font-weight: bold">II. Сведения об объекте капитального строительства</td>
</tr>

<tr>
<td>Наименование объекта капитального строительства (вместе с адресом)</td>
<td><?=$model->name_object_capital_construction. '<br>'.$model->address_object?></td>
</tr>

<tr>
<td>Вид документации</td>
<td><?=$model->getTypeDocumention()?></td>
</tr>

<tr>
<td>Вид строительства</td>
<td><?=$model->getTypeConstruction()?></td>
</tr>

<tr>
<td>Стадия проектирования</td>
<td><?=$model->getDesignStage()?></td>
</tr>

<tr>
<td>Источник финансирования</td>
<td><?=$model->getSourceFinance()?></td>
</tr>

<tr>
<td>Застройщик</td>
<td><?=$model->organization_customer_buider?></td>
</tr>
<?php var_dump($model);?>
<?php if (!empty($model->short_legal_entity)):?>
<tr>
<td colspan="2" style="text-align: center; font-weight: bold">III. Сведения о заказчике проектных работ (заполняется в случае, если заказчик экспертизы и заказчик проекта разные организации)</td>
</tr>

<tr>
<td>Полное и сокращенное наименование юридического лица</td>
<td><?=$model->legal_entity.'	'.$model->short_legal_entity?></td>
</tr>

<tr>
<td>Адрес организации</td>
    <td><?=$model->address_legal_entity_organization?></td>
</tr>

<tr>
<td>Телефон организации</td>

    <td><?=$model->phone_legal_entity_organization?></td>
</tr>

<tr>
<td>Контактное лицо от заявителя (менеджер объекта)</td>
<td><?=$model->manager_object_legal_entity_organization?></td>
</tr>

<tr>
<td>телефон контактного лица</td>
<td><?=$model->phone_legal_entity_organization_contact?></td>
</tr>

<tr>
<td>e-mail контактного лица</td>
<td><?=$model->email_object_legal_entity_organization?></td>
</tr>
<?php endif;?>

<tr>
<td colspan="2" style="text-align: center; font-weight: bold">IV. Сведения об исполнителях (и соисполнителях) проектных работ:</td>
</tr>

<?php if($model->full_type_project_organization):?>
<tr>
<td><b>Полное наименование юридического лица</b></td>
<td><b><?=$model->full_type_project_organization.' '.$model->full_name_project_organization?></b></td>
</tr>
<tr>
<td>Свидетельство СРО</td>
<td><?=$model->statement_sro_project_organization.' от '.date('d.m.Y',strtotime($model->statement_sro_start_project_organization))?></td>
</tr>
<tr>
<td>ФИО ГИПа</td>
<td><?=$model->fio_gipa?></td>
</tr>
<tr>
<td>Адрес:</td>
<td><?=$model->mail_address_project_organization?></td>
</tr>
<?php endif;?>


<?php if($model->survey_organization):?>
<tr>
<td><b>Полное наименование юридического лица</b></td>
<td><b><?=$model->survey_organization?></b></td>
</tr>
<tr>
<td>Свидетельство СРО</td>
<td><?=$model->statement_sro_survey_organization.' от '.date('d.m.Y',strtotime($model->statement_sro_start_survey_organization))?></td>
</tr>
<tr>
<td>ФИО ГИПа</td>
<td></td>
</tr>
<tr>
<td>Адрес:</td>
<td><?=$model->mail_address_survey_organization?></td>
</tr>
<?php endif;?>

<?php if($model->survey2_organization):?>
<tr>
<td><b>Полное наименование юридического лица</b></td>
<td><b><?=$model->survey2_organization?></b></td>
</tr>
<tr>
<td>Свидетельство СРО</td>
<td><?=$model->statement_sro_survey2_organization.' от '.date('d.m.Y',strtotime($model->statement_sro_start_survey2_organization))?></td>
</tr>
<tr>
<td>ФИО ГИПа</td>
<td></td>
</tr>
<tr>
<td>Адрес:</td>
<td><?=$model->mail_address_survey2_organization?></td>
</tr>
<?php endif;?>

<?php if($model->full_name_accomplice1):?>
<tr>
<td>Полное наименование юридического лица</td>
<td><?=$model->full_name_accomplice1?></td>
</tr>
<tr>
<td>Свидетельство СРО</td>
<td><?=$model->statement_sro_accomplice1.' от '.date('d.m.Y',strtotime($model->statement_sro_start_accomplice1))?></td>
</tr>
<tr>
<td>ФИО ГИПа</td>
<td><?=$model->fio_gipa_accomplice1?></td>
</tr>
<tr>
<td>Адрес:</td>
<td><?=$model->address_accomplice1?></td>
</tr>
<?php endif;?>

<?php if($model->full_name_accomplice2):?>
<tr>
<td>Полное наименование юридического лица</td>
<td><?=$model->full_name_accomplice2?></td>
</tr>
<tr>
<td>Свидетельство СРО</td>
<td><?=$model->statement_sro_accomplice2.' от '.date('d.m.Y',strtotime($model->statement_sro_start_accomplice2))?></td>
</tr>
<tr>
<td>ФИО ГИПа</td>
<td><?=$model->fio_gipa_accomplice2?></td>
</tr>
<tr>
<td>Адрес:</td>
<td><?=$model->address_accomplice2?></td>
</tr>
<?php endif;?>

<?php if($model->full_name_accomplice3):?>
<tr>
<td>Полное наименование юридического лица</td>
<td><?=$model->full_name_accomplice3?></td>
</tr>
<tr>
<td>Свидетельство СРО</td>
<td><?=$model->statement_sro_accomplice3.' от '.date('d.m.Y',strtotime($model->statement_sro_start_accomplice3))?></td>
</tr>
<tr>
<td>ФИО ГИПа</td>
<td><?=$model->fio_gipa_accomplice3?></td>
</tr>
<tr>
<td>Адрес:</td>
<td><?=$model->address_accomplice3?></td>
</tr>
<?php endif;?>




</table>

<br>
<table>
<tr>
<td style="vertical-align: top;"><?=$model->a1_chiefpostt?></td>
<td style="text-align: center">__________________________<br>(подпись)</td>
<td style="text-align: center"><?=$model->ceo_company?><br>(Ф.И.О.)</td>
</tr>
<tr><td style="text-align: center; padding-top: 20px;" colspan="3">М.П.</td>
</table>




 			
		</div>
	</body>
</html>
