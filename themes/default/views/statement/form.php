<?php
/*
126 строка - добавлен option 0 =>"Проектная документация без сметы и результаты инженерных изысканий"
Обращение к данным калькулятора:
$_POST['calc']['firstTime'] - первичность рассмотрения
$_POST['calc']['region']
$_POST['calc']['city']
В доках вывод названия организаций надо поправить:
a1_name_full.' '.a1_name_short
legal_entity.' '.short_legal_entity
full_type_project_organization.' '.full_name_project_organization

Новое поле full_type_project_organization (189)
*/
?>
<style>
    #form input[type="text"] {
        width: 600px;
    }

    select {
        width: 620px;
        height: 35px;
    }

    th {
        font-size: 17px;
    }

    #tabs li {
        font-size: 20px;
    }

    #tabs a {
        color: #474747;
    }
</style>
<div id="crumbsBlock" style="display: none"><p><a style="color: white;" href="/">Главная </a> > <a style="color: white;"
                                                                                                   href="/user/cabinet/index">Личный
    кабинет </a> > Форма заявки</p></div>
<div class="insidePage" id="insidePartners">
<div id="insideCont"
     style="position: absolute; background-color: #fff; text-align: center; margin-top: 80px; display: none"
     class="wrap">


<div class="rows-fluids">
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array('id' => 'form', 'enableAjaxValidation' => false, 'enableClientValidation' => true, 'type' => 'vertical', 'htmlOptions' => array('class' => 'well', 'style' => 'margin: 0!important'), 'inlineErrors' => true,));
?>
<div class="inner-column">
<div class="inner-column-right" style="padding:20px;">
<div id="tabs">
<ul>
    <li><a href="#tabs-1">Заявитель и заказчики</a></li>
    <li><a href="#tabs-2">Исполнители</a></li>
    <li><a href="#tabs-3">Сведения об объекте</a></li>
</ul>
<div id="tabs-1">
    <table>
        <tr>
            <td>
                Сведения о заявителе (заказчике услуг негосударственной экспертизы)
            </td>
            <td>
                <select style="margin-top: -3px;height: 40px" type="checkbox"
                        id="zaycheck" onchange="zay()">
                    <option value="1">Новая организация</option>
                    <option value="2">Организация 1</option>
                    <option value="3">Организация 2</option>
                </select>
            </td>
        </tr>
        <tr class="zay">
            <td>Форма юридического лица:</td>
            <td><?=$form->dropDownList($model, 'a1_name_full', array(
                "Общество с ограниченной ответственностью" => "Общество с ограниченной ответственностью",
                "Открытое акционерное общество" => "Открытое акционерное общество",
                "Закрытое акционерное общество" => "Закрытое акционерное общество",
                "Индивидуальным предпринимателем" => "Индивидуальным предпринимателем"

//                    "Общество с ограниченной ответственностью" => "Обществом с ограниченной ответственностью",
//                "Открытое акционерное общество" => "Открытым акционерным обществом»,
//                "Закрытое акционерное общество" => "Закрытым акционерным обществом»,
//                "Индивидуальный предприниматель" => "Индивидуальным предпринимателем»

            ), array('onchange' => 'plat()'));?>
            </td>
        </tr>
        <tr class="zay">
            <td>Название юридического лица:</td>
            <td><?=$form->textField($model, 'a1_name_short'); ?></td>
        </tr>
        <tr class="zay">
            <td>Адрес организации (юридический):</td>
            <td>
            <table class="adress_table">
                <tr>
                    <td>
                        <input name="a1_index_jur" id="a1_index_jur" type="text" class="index_field" placeholder="Индекс">
                    </td>
                    <td><select name="a1_region_jur" id="a1_region_jur">
                        <option>Регион</option>
                        <option>Регион 1</option>
                        <option>Регион 2</option>
                    </select></td>
                    <td><select name="a1_city_jur" id="a1_city_jur">
                        <option>Город</option>
                        <option>Москва</option>
                        <option>Санкт-Питербург</option>
                    </select></td>
                    <td><select name="a1_street_jur" id="a1_street_jur">
                        <option>Улица</option>
                        <option>ул. Ленина</option>
                    </select></td>
                    <td><select name="a1_house_jur" id="a1_house_jur">
                        <option>Дом</option>
                        <option>1</option>
                        <option>2</option>
                    </select></td>
                    <td><select name="a1_corpus_jur" id="a1_corpus_jur">
                        <option>Корпус</option>
                        <option>а</option>
                        <option>б</option>
                    </select></td>
                    <td><select name="a1_apt_jur" id="a1_apt_jur">
                        <option>Квартира</option>
                        <option>1</option>
                        <option>2</option>
                    </select></td>
                </tr>
            </table>
            </td>
            <td class="nodisplay"><?=$form->textField($model, 'a1_address'); ?></td>
        </tr>
        <tr class="zay">
            <td>Адрес организации (почтовый):</td>
            <td><input type="checkbox" name="a1_post" value="1" id="a1_post" onchange="a1post()">Почтовый адрес совпадает с юридическим
            <table class="a1_post adress_table">
                <tr>
                    <td>
                        <input name="a1_index" id="a1_index_post" type="text" class="index_field" placeholder="Индекс">
                    </td>
                    <td><select name="a1_region_post" id="a1_region_post">
                        <option>Регион</option>
                        <option>Регион 1</option>
                        <option>Регион 2</option>
                    </select></td>
                    <td><select name="a1_city_post" id="a1_city_post">
                        <option>Город</option>
                        <option>Москва</option>
                        <option>Санкт-Питербург</option>
                    </select></td>
                    <td><select name="a1_street_post" id="a1_street_post">
                        <option>Улица</option>
                        <option>ул. Ленина</option>
                    </select></td>
                    <td><select name="a1_house_post" id="a1_house_post">
                        <option>Дом</option>
                        <option>1</option>
                        <option>2</option>
                    </select></td>
                    <td><select name="a1_corpus_post" id="a1_corpus_post">
                        <option>Корпус</option>
                        <option>а</option>
                        <option>б</option>
                    </select></td>
                    <td><select name="a1_apt_post" id="a1_apt_post">
                        <option>Квартира</option>
                        <option>1</option>
                        <option>2</option>
                    </select></td>
                </tr>
            </table>
            </td>
            <td class="nodisplay"><?=$form->textField($model, 'a1_address_post'); ?></td>
        </tr>
        <tr class="zay">
            <td>Телефон организации:</td>
            <td><?=$form->textField($model, 'a1_phone'); ?></td>
        </tr>

        <tr class="zay">
            <td>Контактное лицо от заявителя (менеджер объекта):</td>
            <td><?=$form->textField($model, 'a1_manager'); ?></td>
        </tr>

<!--        <tr class="zay">-->
<!--            <td>Контактное лицо от заявителя (Имя):</td>-->
<!--            <td>--><?//=$form->textField($model, 'a1_manager_lastname'); ?><!--</td>-->
<!--        </tr>-->
<!--        <tr class="zay">-->
<!--            <td>Контактное лицо от заявителя (Фамилия):</td>-->
<!--            <td>--><?//=$form->textField($model, 'a1_manager_firstname'); ?><!--</td>-->
<!--        </tr>-->




        <tr class="zay">
            <td>Телефон контактного лица:</td>
            <td><?=$form->textField($model, 'a1_manager_phone'); ?></td>
        </tr>
        <tr class="zay">
            <td>E-mail контактного лица <br>(для направления замечаний):</td>
            <td><?=$form->textField($model, 'a1_manager_email'); ?></td>
        </tr>
        <tr class="zay">
            <td colspan="2">Руководитель:</td>
        </tr>
        <tr class="zay">
            <td>Фамилия:</td>
            <td><input type="text" name="ceo_surname" id="ceo_surname"></td>
            <td class="nodisplay"><?=$form->textField($model, 'ceo_company'); ?></td>
        </tr>
        <tr class="zay">
            <td>Имя:</td>
            <td><input type="text" name="ceo_name" id="ceo_name"></td>
        </tr>
        <tr class="zay">
            <td>Отчество:</td>
            <td><input type="text" name="ceo_patronimic" id="ceo_patronimic"></td>
        </tr>
        <tr class="zay">
            <td>Должность руководителя:</td>
            <td><?=$form->textField($model, 'a1_chiefpostt'); ?></td>
        </tr>
        <tr class="zay">
            <td colspan="2">Главный бухгалтер:</td>
        </tr>
        <tr class="zay">
            <td>Фамилия:</td>
            <td><input type="text" name="buhgalter_surname" id="buhgalter_surname"></td>
            <td class="nodisplay"><?=$form->textField($model, 'chief_buhgalter'); ?></td>
        </tr>
        <tr class="zay">
            <td>Имя:</td>
            <td><input type="text" name="buhgalter_name" id="buhgalter_name"></td>
        </tr>
        <tr class="zay">
            <td>Отчество:</td>
            <td><input type="text" name="buhgalter_patronimic" id="buhgalter_patronimic"></td>
        </tr>
        <tr class="zay">
            <th colspan="2"> Платежные реквизиты организации</th>
        </tr>
        <tr class="zay">
            <td>ИНН:</td>
            <td><?=$form->textField($model, 'plat_inn', array('maxlength' => '12')); ?></td>
        </tr>
        <tr id="kpp" class="zay">
            <td>КПП:</td>
            <td><?=$form->textField($model, 'plat_kpp', array('maxlength' => '9')); ?></td>
        </tr>
        <tr class="zay">
            <td>ОГРН:</td>
            <td><?=$form->textField($model, 'plat_ogrn', array('maxlength' => '13')); ?></td>
        </tr>
        <tr class="zay">
            <td>Р/С:</td>
            <td><?=$form->textField($model, 'plat_rs', array('maxlength' => '20')); ?></td>
        </tr>
        <tr class="zay">
            <td>Банк:</td>
            <td><?=$form->textField($model, 'plat_bank'); ?></td>
        </tr>
        <tr class="zay">
            <td>К/С:</td>
            <td><?=$form->textField($model, 'plat_ks', array('maxlength' => '20')); ?></td>
        </tr>
        <tr class="zay">
            <td>БИК:</td>
            <td><?=$form->textField($model, 'plat_bik', array('maxlength' => '9')); ?></td>
        </tr>
        <!---->
        <tr>
            <td>
                Организация заказчик проектных работ
            </td>
            <td style="padding-bottom: 20px;">
                <select
                style="margin-top: -3px; height: 40px;" id="zprcheck" type="checkbox"
                onchange="zpr()">
                    <option value="1">Новая организация</option>
                    <option value="2">Заказчик 1</option>
                    <option value="3">Заказчик 2</option>
                </select>
            </td>
        </tr>
        <tr class="zpr">
            <th colspan="2">Сведения о заказчике проектных работ</th>
        </tr>
        <tr class="zpr">
            <td>Полное наименование юридического лица:</td>
            <td><?=$form->dropDownList($model, 'legal_entity', array(
                "Общество с ограниченной ответственностью" => "Общество с ограниченной ответственностью",
                "Открытое акционерное общество" => "Открытое акционерное общество",
                "Закрытое акционерное общество" => "Закрытое акционерное общество",
                "Индивидуальный предприниматель" => "Индивидуальный предприниматель"
            ));?>
            </td>
        </tr>
        <tr class="zpr">
            <td>Сокращенное наименование юридического лица:</td>
            <td><?=$form->textField($model, 'short_legal_entity'); ?></td>
        </tr>
        <tr class="zpr">
            <td>Адрес организации:</td>
            <td><?=$form->textField($model, 'address_legal_entity_organization'); ?></td>
        </tr>
        <tr class="zpr">
            <td>Телефон организации:</td>
            <td><?=$form->textField($model, 'phone_legal_entity_organization'); ?></td>
        </tr>
        <tr>
            <td>Контактное лицо от заявителя (менеджер объекта):</td>
        </tr>
        <tr class="zpr">
            <td>Фамилия:</td>
            <td><input type="text" name="manager_surname" id="manager_surname"></td>
            <td class="nodisplay"><?=$form->textField($model, 'manager_object_legal_entity_organization'); ?></td>
        </tr>
        <tr class="zpr">
            <td>Имя:</td>
            <td><input type="text" name="manager_name" id="manager_name"></td>
        </tr>
        <tr class="zpr">
            <td>Отчество:</td>
            <td><input type="text" name="manager_patronimic" id="manager_patronimic"></td>
        </tr>
        <tr class="zpr">
            <td>Телефон контактного лица:</td>
            <td><?=$form->textField($model, 'phone_legal_entity_organization_contact'); ?></td>
        </tr>
        <tr class="zpr">
            <td>E-mail контактного лица:</td>
            <td><?=$form->textField($model, 'email_object_legal_entity_organization'); ?></td>
        </tr>
        <tr>
            <th colspan="2"> Заказчик-застройщик</th>
        </tr>
        <tr>
            <td>Полное наименование организации заказчика-застройщика:</td>
            <td> <?=$form->textField($model, 'organization_customer_buider'); ?></td>
        </tr>
        <tr>
            <td>Почтовый адрес:</td>
            <td><?=$form->textField($model, 'mail_address_customer_buider'); ?></td>
        </tr>
        <tr>
            <td>Свидетельство СРО:</td>
            <td><?=$form->textField($model, 'statement_sro_customer_buider', array('style' => 'width: 460px')); ?> от
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'statement_sro_start_customer_buider',
                    'value' => date('d-m-Y'),
                    'language' => 'ru',
                    'htmlOptions' => array(
                        'style' => 'width: 90px', // textField size
                        'maxlength' => '10', // textField maxlength
                        'placeholder' => 'гггг-мм-дд',
                    ),
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'changeMonth' => 'true',
                        'changeYear' => 'true',
                        'yearRange' => "1950:2014",
                    ),
                ));
                ?></td>
        </tr>
        <tr>
            <td>Телефон:</td>
            <td><?=$form->textField($model, 'phone_customer_buider'); ?></td>
        </tr>
        <tr>
            <td>Факс:</td>
            <td><?=$form->textField($model, 'fax_customer_buider'); ?></td>
        </tr>
        <tr>
            <td>Email:</td>
            <td><?=$form->textField($model, 'email_customer_buider'); ?></td>
        </tr>
        <tr>
            <td>Сайт:</td>
            <td><?=$form->textField($model, 'site2'); ?></td>
        </tr>
        <tr>
            <td>Контактное лицо:</td>
            <td><?=$form->textField($model, 'contact_person_customer_buider'); ?></td>
        </tr>
        <tr>
            <td>телефон контактного лица:</td>
            <td><?=$form->textField($model, 'phone_contact_person_customer_buider'); ?></td>
        </tr>


        <tr>
            <td>
               Организация застройщик
            </td>
            <td>
                <select style="margin-top: -3px;height: 40px" type="checkbox"
                                   id="ztzcheck" onchange="ztz()">
                    <option value="1">Новая организация</option>
                    <option value="2">Организация 1</option>
                    <option value="3">Организация 2</option>
                </select>
            </td>
        </tr>
        <tr class="ztz">
            <td colspan="2">
                <span class="big-red">Заявителем и Заказчиком по заявке являются разные организации. Поэтому требуется доверенность, по которой Заказчик поручает Заявителю представлять свои интересы по экспертизе. Скачать форму доверенности можно <a href="#">по ссылке</a></span>
            </td>
        </tr>
        <tr class="ztz">
            <td>Полное наименование застройщика:</td>
            <td><?=$form->textField($model, 'applicant'); ?></td>
        </tr>
        <tr class="ztz">
            <td>Почтовый адрес:</td>
            <td><?=$form->textField($model, 'email_address_applicant'); ?></td>
        </tr>
        <tr class="ztz">
            <td>Свидетельство СРО:</td>
            <td><?=$form->textField($model, 'statement_sro_applicant', array('style' => 'width: 460px')); ?> от
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'statement_sro_start_applicant',
                    'value' => date('d-m-Y'),
                    'language' => 'ru',
                    'htmlOptions' => array(
                        'style' => 'width: 90px', // textField size
                        'maxlength' => '10', // textField maxlength
                        'placeholder' => 'гггг-мм-дд',
                    ),
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'changeMonth' => 'true',
                        'changeYear' => 'true',
                        'yearRange' => "1950:2014",
                    ),
                ));
                ?></td>
        </tr>
        <tr class="ztz">
            <td>Телефон:</td>
            <td><?=$form->textField($model, 'phone_applicant'); ?></td>
        </tr>
        <tr class="ztz">
            <td>Факс:</td>
            <td><?=$form->textField($model, 'fax_applicant'); ?></td>
        </tr>
    </table>
</div>
<div id="tabs-2">
<table>
<tr>
    <th colspan="2"> Сведения о заявителе и исполнителях работ по разработке проектной документации и выполнении
        инженерных изысканий :
    </th>
</tr>
<tr>
    <td>Организационно-правовая форма:</td>
    <td><?=$form->dropDownList($model, 'full_type_project_organization', array(
        "Общество с ограниченной ответственностью" => "Общество с ограниченной ответственностью",
        "Открытое акционерное общество" => "Открытое акционерное общество",
        "Закрытое акционерное общество" => "Закрытое акционерное общество",
        "Индивидуальный предприниматель" => "Индивидуальный предприниматель"
    ));?>
    </td>
</tr>
<tr>
    <td>Сокращенное название проектной организации:</td>
    <td><?=$form->textField($model, 'full_name_project_organization'); ?></td>
</tr>
<tr>
    <td>Почтовый адрес:</td>
    <td><?=$form->textField($model, 'mail_address_project_organization'); ?></td>
</tr>
<tr>
    <td>Свидетельство СРО:</td>
    <td><?=$form->textField($model, 'statement_sro_project_organization', array('style' => 'width: 460px')); ?>&nbsp;от&nbsp;<?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'statement_sro_start_project_organization',
            'value' => date('d-m-Y'),
            'language' => 'ru',
            'htmlOptions' => array(
                'style' => 'width: 90px', // textField size
                'maxlength' => '10', // textField maxlength
                'placeholder' => 'гггг-мм-дд',
            ),
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth' => 'true',
                'changeYear' => 'true',
                'yearRange' => "1950:2014",
            ),
        ));
        ?></td>
</tr>
<tr>
    <td>Телефон:</td>
    <td><?=$form->textField($model, 'phone_project_organization'); ?></td>
</tr>
<tr>
    <td>Факс:</td>
    <td><?=$form->textField($model, 'fax_project_organization'); ?></td>
</tr>
<tr>
    <td>Email:</td>
    <td><?=$form->textField($model, 'email_project_organization'); ?></td>
</tr>
<tr>
    <td>Сайт:</td>
    <td><?=$form->textField($model, 'site'); ?></td>
</tr>
<tr>
    <td colspan="2">ФИО ГИПа:</td>
</tr>
<tr>
    <td>Фамилия:</td>
    <td><input type="text" name="gipa_surname" id="gipa_surname"></td>
    <td class="nodisplay"><?=$form->textField($model, 'fio_gipa'); ?></td>
</tr>
<tr>
    <td>Имя:</td>
    <td><input type="text" name="gipa_name" id="gipa_name"></td>
</tr>
<tr>
    <td>Отчество:</td>
    <td><input type="text" name="gipa_patronimic" id="gipa_patronimic"></td>
</tr>
<tr>
    <td>Телефон ГИПа:</td>
    <td><?=$form->textField($model, 'phone_gipa'); ?></td>
</tr>


<tr>
    <th colspan="2"> Инженерно-геологические изыскания</th>
</tr>
<tr>
    <td>Организационно-правовая форма:</td>
    <td><?=$form->dropDownList($model, 'full_type_survey_organization', array(
        "Общество с ограниченной ответственностью" => "Общество с ограниченной ответственностью",
        "Открытое акционерное общество" => "Открытое акционерное общество",
        "Закрытое акционерное общество" => "Закрытое акционерное общество",
        "Индивидуальный предприниматель" => "Индивидуальный предприниматель"
    ));?>
    </td>
</tr>
<tr>
    <td>Полное наименование изыскательской организации:</td>
    <td><?=$form->textField($model, 'survey_organization'); ?></td>
</tr>
<tr>
    <td>Почтовый адрес:</td>
    <td><?=$form->textField($model, 'mail_address_survey_organization'); ?></td>
</tr>
<tr>
    <td>Свидетельство СРО:</td>
    <td><?=$form->textField($model, 'statement_sro_survey_organization', array('style' => 'width: 460px')); ?>&nbsp;от&nbsp;<?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'statement_sro_start_survey_organization',
            'value' => date('d-m-Y'),
            'language' => 'ru',
            'htmlOptions' => array(
                'style' => 'width: 90px', // textField size
                'maxlength' => '10', // textField maxlength
                'placeholder' => 'гггг-мм-дд',
            ),
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth' => 'true',
                'changeYear' => 'true',
                'yearRange' => "1950:2014",
            ),
        ));
        ?></td>
</tr>
<tr>
    <td>Телефон:</td>
    <td><?=$form->textField($model, 'phone_survey_organization'); ?></td>
</tr>
<tr>
    <td>Факс:</td>
    <td><?=$form->textField($model, 'fax_survey_organization'); ?></td>
</tr>


<tr>
    <th colspan="2">Инженерно-геодезические изыскания</th>
</tr>
<tr>
    <td>Организационно-правовая форма:</td>
    <td><?=$form->dropDownList($model, 'full_type_survey2_organization', array(
        "Общество с ограниченной ответственностью" => "Общество с ограниченной ответственностью",
        "Открытое акционерное общество" => "Открытое акционерное общество",
        "Закрытое акционерное общество" => "Закрытое акционерное общество",
        "Индивидуальный предприниматель" => "Индивидуальный предприниматель"
    ));?>
    </td>
</tr>
<tr>
    <td>Полное наименование изыскательской организации:</td>
    <td><?=$form->textField($model, 'survey2_organization'); ?></td>
</tr>
<tr>
    <td>Почтовый адрес:</td>
    <td><?=$form->textField($model, 'mail_address_survey2_organization'); ?></td>
</tr>
<tr>
    <td>Свидетельство СРО:</td>
    <td><?=$form->textField($model, 'statement_sro_survey2_organization', array('style' => 'width: 460px')); ?>&nbsp;от&nbsp;<?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'statement_sro_start_survey2_organization',
            'value' => date('d-m-Y'),
            'language' => 'ru',
            'htmlOptions' => array(
                'style' => 'width: 90px', // textField size
                'maxlength' => '10', // textField maxlength
                'placeholder' => 'гггг-мм-дд',
            ),
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth' => 'true',
                'changeYear' => 'true',
                'yearRange' => "1950:2014",
            ),
        ));
        ?></td>
</tr>
<tr>
    <td>Телефон:</td>
    <td><?=$form->textField($model, 'phone_survey2_organization'); ?></td>
</tr>
<tr>
    <td>Факс:</td>
    <td><?=$form->textField($model, 'fax_survey2_organization'); ?></td>
</tr>

<tr>
    <th colspan="2" style="padding-bottom: 20px;"><input
        style="margin-right: 10px; margin-top: -3px; width: 20px; height: 20px" id="soispcheck" type="checkbox"
        onchange="soisp()">Добавить организацию-соисполнителя
    </th>
</tr>
<tr class="soisp">
    <th colspan="2"> Организация соисполнитель</th>
</tr>
<tr class="soisp">
    <td>Полное наименование юридического лица:</td>
    <td><?=$form->textField($model, 'full_name_accomplice1'); ?></td>
</tr>
<tr class="soisp">
    <td>Свидетельство СРО:</td>
    <td><?=$form->textField($model, 'statement_sro_accomplice1', array('style' => 'width: 460px')); ?> от
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'statement_sro_start_accomplice1',
            'value' => date('d-m-Y'),
            'language' => 'ru',
            'htmlOptions' => array(
                'style' => 'width: 90px', // textField size
                'maxlength' => '10', // textField maxlength
                'placeholder' => 'гггг-мм-дд',
            ),
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth' => 'true',
                'changeYear' => 'true',
                'yearRange' => "1950:2014",
            ),
        ));
        ?></td>
</tr>
<tr class="soisp">
    <td colspan="2">ФИО ГИПа:</td>
</tr>
<tr class="soisp">
    <td>Фамилия:</td>
    <td><input type="text" name="gipa_surname_accomplice1" id="gipa_surname_accomplice1"></td>
    <td class="nodisplay"><?=$form->textField($model, 'fio_gipa_accomplice1'); ?></td>
</tr>
<tr class="soisp">
    <td>Имя:</td>
    <td><input type="text" name="gipa_name_accomplice1" id="gipa_name_accomplice1"></td>
</tr>
<tr class="soisp">
    <td>Отчество:</td>
    <td><input type="text" name="gipa_patronimic_accomplice1" id="gipa_patronimic_accomplice1"></td>
</tr>
<tr class="soisp">
    <td>Адрес:</td>
    <td><?=$form->textField($model, 'address_accomplice1'); ?></td>
</tr>


<tr class="soisp">
    <th colspan="2"> Организация соисполнитель</th>
</tr>
<tr class="soisp">
    <td>Полное наименование юридического лица:</td>
    <td> <?=$form->textField($model, 'full_name_accomplice2'); ?></td>
</tr>
<tr class="soisp">
    <td>Свидетельство СРО:</td>
    <td><?=$form->textField($model, 'statement_sro_accomplice2', array('style' => 'width: 460px')); ?>
        &nbsp;от&nbsp;<?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'statement_sro_start_accomplice2',
            'value' => date('d-m-Y'),
            'language' => 'ru',
            'htmlOptions' => array(
                'style' => 'width: 90px', // textField size
                'maxlength' => '10', // textField maxlength
                'placeholder' => 'гггг-мм-дд',
            ),
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth' => 'true',
                'changeYear' => 'true',
                'yearRange' => "1950:2014",
            ),
        ));
        ?></td>
</tr>
<tr class="soisp">
    <td>ФИО ГИПа:</td>
</tr>
<tr class="soisp">
    <td>Фамилия:</td>
    <td><input type="text" name="gipa_surname_accomplice2" id="gipa_surname_accomplice2"></td>
    <td class="nodisplay"><?=$form->textField($model, 'fio_gipa_accomplice2'); ?></td>
</tr>
<tr class="soisp">
    <td>Имя:</td>
    <td><input type="text" name="gipa_name_accomplice2" id="gipa_name_accomplice2"></td>
</tr>
<tr>
    <td>Отчество:</td>
    <td><input type="text" name="gipa_patronimic_accomplice2" id="gipa_patronimic_accomplice2"></td>
</tr>
<tr class="soisp">
    <td>Адрес:</td>
    <td><?=$form->textField($model, 'address_accomplice1'); ?></td>
</tr>

<tr class="soisp">
    <td>Адрес:</td>
    <td> <?=$form->textField($model, 'address_accomplice2'); ?></td>
</tr>


<tr class="soisp">
    <th colspan="2"> Организация соисполнитель</th>
</tr>
<tr class="soisp">
    <td>Полное наименование юридического лица:</td>
    <td><?=$form->textField($model, 'full_name_accomplice3'); ?></td>
</tr>
<tr class="soisp">
    <td>Свидетельство СРО:</td>
    <td><?=$form->textField($model, 'statement_sro_accomplice3', array('style' => 'width: 460px')); ?> от
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'statement_sro_start_accomplice3',
            'value' => date('d-m-Y'),
            'language' => 'ru',
            'htmlOptions' => array(
                'style' => 'width: 90px', // textField size
                'maxlength' => '10', // textField maxlength
                'placeholder' => 'гггг-мм-дд',
            ),
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth' => 'true',
                'changeYear' => 'true',
                'yearRange' => "1950:2014",
            ),
        ));
        ?></td>
</tr>
<tr class="soisp">
    <td>ФИО ГИПа:</td>
    <td><?=$form->textField($model, 'fio_gipa_accomplice3'); ?></td>
</tr>
<tr class="soisp">
    <td>Фамилия:</td>
    <td><input type="text" name="gipa_surname_accomplice3" id="gipa_surname_accomplice3"></td>
    <td class="nodisplay"><?=$form->textField($model, 'fio_gipa_accomplice3'); ?></td>
</tr>
<tr class="soisp">
    <td>Имя:</td>
    <td><input type="text" name="gipa_name_accomplice3" id="gipa_name_accomplice3"></td>
</tr>
<tr class="soisp">
    <td>Отчество:</td>
    <td><input type="text" name="gipa_patronimic_accomplice3" id="gipa_patronimic_accomplice3"></td>
</tr>

<tr class="soisp">
    <td>Адрес:</td>
    <td><?=$form->textField($model, 'address_accomplice3'); ?></td>
</tr>
</table>
</div>
<div id="tabs-3">
    <table>
        <tr>
            <th colspan="2">Сведения об объекте капитального строительства</th>
        </tr>
        <tr>
            <td>Первичность рассмотрения:</td>
            <?php if (isset($_POST['calc'])): ?>
            <td><?=$form->textField($model, 'firstTime', array("value" => $_POST['calc']['firstTime'], "disabled" => "disabled"));?></td>
            <?php else: ?>
            <td><?=$form->dropDownList($model, 'firstTime', array("впервые" => "впервые", "повторно" => "повторно"));?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td>Полное наименование объекта капитального строительства (вместе с адресом):</td>
            <td><?=$form->textField($model, 'name_object_capital_construction'); ?></td>
        </tr>
        <tr>
            <td>Вид документации:</td>
            <td><?=$form->dropDownList($model, 'type_documention', array(0 => "Проектная документация без сметы и результаты инженерных изысканий", 1 => "Проектная документация со сметой", 2 => "Проектная документация без сметы", 3 => "Разделы проектной документации", 4 => "Сметная документация", 5 => "Результаты инженерных изысканий")); ?></td>
        </tr>
        <tr>
            <td>Вид строительства:</td>
            <td><?=$form->dropDownList($model, 'type_construction', array(1 => "Новое", 2 => "Реконструкция", 3 => "Капительный ремонт", 4 => "Модернизация")); ?></td>
        </tr>
        <tr>
            <td>Стадии проектирования:</td>
            <td><?=$form->dropDownList($model, 'design_stage', array(1 => "Проектная документация", 2 => "проектная и рабочая документация",)); ?></td>
        </tr>
        <tr>
            <td>Источник финансирования:</td>
            <td><?=$form->dropDownList($model, 'source_of_finance', array(1 => "Собственные средства заказчика", 2 => "Кредитные средства", 3 => "Кредитные средства Сбербанк")); ?></td>
        </tr>
        <tr id="credit_loan" style="display: none;">
            <td>Банк-кредитор:</td>
            <td><?=$form->textField($model, 'loan_funds', array()); ?></td>
        </tr>
        <tr>
            <td>Заявленная сметная стоимость:</td>
            <td><?=$form->textField($model, 'claimed_estimated_cost'); ?></td>
        </tr>
        <tr>
            <td>в т.ч. СМР:</td>
            <td><?=$form->textField($model, 'v_t_4_SMR'); ?></td>
        </tr>
        <tr>
            <th colspan="2"> Технико-экономические характеристики объекта капитального строительства с учетом его вида,
                функционального назначения и характерных особенностей
            </th>
        </tr>
        <tr>
            <td>Площадь земельного участка:</td>
            <td><?=$form->textField($model, 'land_area'); ?></td>
        </tr>
        <tr>
            <td>Площадь застройки:</td>
            <td><?=$form->textField($model, 'built_up_area'); ?></td>
        </tr>
        <tr>
            <td>Коэффициент застройки:</td>
            <td><?=$form->textField($model, 'building_factor'); ?> </td>
        </tr>
        <tr>
            <td>Коэффициент плотности застройки:</td>
            <td> <?=$form->textField($model, 'building_density_factor'); ?></td>
        </tr>
        <tr>
            <td>Площадь здания:</td>
            <td><?=$form->textField($model, 'building_area'); ?></td>
        </tr>
        <tr>
            <td>Количество квартир (для жилых зданий):</td>
            <td> <?=$form->textField($model, 'count_apartament'); ?></td>
        </tr>
        <tr>
            <td>Количество этажей:</td>
            <td><?=$form->textField($model, 'count_floor'); ?></td>
        </tr>
        <tr>
            <td>Этажность здания:</td>
            <td><?=$form->textField($model, 'floors'); ?></td>
        </tr>
        <tr>
            <td>Строительный объем в т.ч. ниже отметки 0,000 (подземная часть):</td>
            <td><?=$form->textField($model, 'building_volume'); ?></td>
        </tr>
        <tr>
            <td>Площадь встроенных общественных помещений (для жилых зданий):</td>
            <td><?=$form->textField($model, 'area_built_public_buildings'); ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
<fieldset>
    <legend>Опись документов, представленных на экспертизу</legend>
    <table class="inventory">
        <thead>
        <tr>
            <th style="font-size: 15px;">№ п/п</th>
            <th style="font-size: 15px;">Наименование документа</th>
            <th style="font-size: 15px;">Кол-во экземплярова</th>
            <th style="font-size: 15px;"></th>
        </tr>
        </thead>
        <?php
        ?>
        <tbody>
        <?php if (!$model->isNewRecord): ?>
            <?php
            for ($i = 1; $i <= count(CJSON::decode($model->json_inventory)); $i++) {
                ?>
            <tr>
                <td><?=$i?></td>
                <td><input name="nameDoc[<?=$i?>]" value="<?//=CJSON::decode($model->json_inventory)[$i]['nameDoc']?>"
                           type="text" style="width: 700px"></td>
                <td><input name="countItems[<?=$i?>]"
                           value="<?//=CJSON::decode($model->json_inventory)[$i]['countItems']?>" type="text"
                           style="width: 50px"></td>
                <?php if ($i > 1): ?>
                <td><a style="cursor: pointer" onclick="del(this)">
                    <div class="icon-minus"></div>
                    Удалить</a></td>
                <?php endif;?>
            </tr>
                <?php
            }
            ?>
            <?php else: ?>
        <tr>
            <td>1</td>
            <td><input name="nameDoc[1]" type="text" style="width: 700px"></td>
            <td><input name="countItems[1]" type="text" style="width: 50px"></td>
            <td></td>
        </tr>
            <?php endif;?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="4"><a style="cursor: pointer" onclick="add()">
                <div class="icon-plus"></div>
                Добавить еще строку</a></td>
        </tr>
        </tfoot>
    </table>
</fieldset>


<input type="submit" value="Сохранить" id="onlineOrderLink" style="border: none"/>
<?php $this->endWidget(); ?>
</div>


</div>
<img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/inside_bg1.png" class="insideBg">
</div>

<script type="text/javascript">
    json_data = {};
    var address = "";
    <?php if ($id > 0): ?>
    var json_data =<?php echo Calculation::model()->findByPk($id)->json_data?>;
    var address = "<?php echo Calculation::model()->findByPk($id)->data?>";
        <?php endif;?>
</script>
<script type="text/javascript" src="/themes/default/web/js/form.js"></script>