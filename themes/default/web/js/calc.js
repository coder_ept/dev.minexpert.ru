var mode_0 = 0;
var mode_1 = 0;
var mode_2 = 0;
var res = 0;
function removeDisabled(obj)
{
    $(obj).removeClass('disabled');
}
function CalcController($scope)
{
    $scope.category = 1;
    $scope.variant = 2;
    $scope.setCategory = function(id)
    {
        if (id==3)        
            $('#callback').modal('show');            
        $scope.category=id;             
    }       
    $scope.setVariant = function(id)
    {                                   
        if (id==3 && $scope.category==2)
            $('#callback').modal('show');
        else      
        if (id==4)
            $('#callback').modal('show');
        $scope.variant=id; 
    }    
}
function validate(){
    var is_error=false;
    var msg={};
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    /*if (mode_1==4){
        if (parseInt($('#selSBY option:selected').val())==0)
        {
            msg={'variant':'Выберите один из вариантов'};
            is_error=true;
        }
        if(parseInt($('#getset select option:selected').val())>0){
            msg={'type_variant':'Выберите тип варианта'};
            is_error=true;
        }
    }*/
    if ($('input[name=email]').length > 0) {
        if ($('input[name=email]').val() == "") {
             is_error=true;
             msg['email']='Введите email';   
        }else if (!regex.test($('input[name=email]').val())){
             is_error=true;
             msg['email']='Email введен не верно';
        }
    }
    if ($('input[name=phone]').length > 0) {
        if ($('input[name=phone]').val() == "") {
             is_error=true;
             msg['phone']='Введите номер телефона';   
        }
    }
    if (is_error){
            $('#errors').remove();
            $('<div class="alert alert-error" id="errors"><ul></ul></div>').insertBefore($('#yw0'));
            $.each(msg,function(i,k){
                $('#errors ul').append('<li>'+k+'</li>');
            });
        return false;    
    }else{
        $('#errors').hide();
        return true;    
    }
        
}
var valid_tab=6;
var redirect_tab=7;


$(function () {
  
    $('#zones_chzn').remove();
        $('#region').change(function(){
            $('#zones').empty();
            $('.combosex_main').remove();
            $('#zones_chzn').remove();
            $.ajax({
                url:'/site/getzone?id='+$(this).val(),
                dataType:'json',
                success:function(res)
                {
                    $.each(res,function(key,value){
                         $('#zones').append('<option value="'+key+'">'+value+'</option>');
                    });
                     zones=[];
                     counter=0;
                     $.each($('#zones option'),
                      function(key,val){
                      zones[counter]=$(val).text();
                      counter++;
                      }
                     );
                    zones=JSON.parse(JSON.stringify(zones));
                    $('#zones_1').removeClass('combosexed');
                    $('#zones_1').combosex({options:zones});
                }
            });
            
            });
            $('#CallbackForm_number').mask('+7(999)999-99-99');
           $('#CallbackForm_time').timepicker({
                    minuteStep: 5,
                    showInputs: false,
                    showSeconds: true,
                    showMeridian:   false
                }); 
                
		var ww = $(window).width();
		var cw = $('.wrap').width();
		var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
			$('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300
			})
		};
        
    if ($('input[name=phone]').length > 0) {
        $('input[name=phone]').mask('+7(999)999-99-99');
    }
    var cont = new Array($('#cl_0'), $('#cl_1'), $('#cl_2'), $('#cl_3'), $('#cl_4'));
    cont[0].hide();
    cont[1].hide();
    cont[2].hide();
    cont[3].hide();
    cont[4].hide();
    mode_0 = $('#form1 input:radio:checked').val();
    cont[0].show();
    mode_1 = $('#form2 input[name="counting"]:radio:checked').val();
    show_block(mode_1);
    $('#form1 input[name="count"]').click(function () {
        mode_0 = $('#form1 input:radio:checked').val();
        cont[0].show()
    });
    $('#form2 input[name="counting"]').click(function () {
        mode_1 = $('#form2 input[name="counting"]:radio:checked').val();
        $('.rows#input_data').empty();
        $('.rows#input_data').hide();
        show_block(mode_1)
    });
    $('#form2 input[name="counting5"]:radio').click(function () {
        var mode_3 = $('#form2 input[name="counting5"]:radio:checked').val()
    });
    $('#calc').click(function () {
        if ($('.tab-pane.fade.active.in').attr('id') == 'yw0_tab_'+valid_tab) {
            if (validate()){            
                calc();    
                                       
            }else{
                return false;
            }
        }else if ($('.tab-pane.fade.active.in').attr('id') == 'yw0_tab_'+redirect_tab) {
            window.location.href="/user/cabinet/index";
        }
    });
    
    $('.tab-content select').chosen();
    $('select[name="counting5"]').on('change', function () {
        $.ajax({
            type: 'post',
            url: '/site/GetSettings',
            data: 'cat_id=' + $('select[name="counting5"] option:selected').val(),
            success: function (data) {
                $('#getset').remove();
                $('.rows#input_data').empty();
                $('.rows#input_data').hide();
                $('.subCalculate#cl_4').append(data);
                $('select').chosen()
            }
        })
    });
    $('input[value="4"]').click(function () {
        $('input[name="counting5"]:not(:checked)').parent().show();
        $('input[name="counting5"]').parent().find('div').empty();
        $('input[name="counting5"]').parent().find('div').hide()
    })
});

function getInputData(id) {
    if (id > 0) {
        $('#d_cig').remove();
        $('#d_x').remove();
        $.ajax({
            type: 'post',
            data: 'id=' + id,
            url: '/site/GetInputData',
            success: function (data) {
                $('.rows#input_data').append(data);
                $('.rows#input_data').show()
            },
            complete: function()
            {
                if (angular.element('#calcBlock').scope().variant==2)
                {
                    $('#d_cig').hide();
                    $('#v_cig').hide()
                }else
                {
                    $('#d_cig').show();
                    $('#v_cig').show()
                } 
            }
        })
    }
}
function handleChange(input) 
{
    if (input.value < parseInt($(input).attr('min'))) input.value = parseInt($(input).attr('min'));
    if (input.value > parseInt($(input).attr('max'))) input.value = parseInt($(input).attr('max'));
}  
function validation(f)
{    
    if (f.value.match(/[^0-9]/g)) {
        f.value = f.value.replace(/[^0-9\.]/g, '');
    }
}
function getData(data) {
    var result_json = {};
    var intermediate = $('form').serializeArray();
    result_json['data']=data;            
    if (mode_0 == 1) result_json['etap'] = 'Первичная экспертиза';
        else result_json['etap'] = 'Вторичная экспертиза'; 
            
    if (mode_1 == 1) {
        result_json['event'] = 'Экспертиза результатов инженерных изысканий жилых объектов.';
        result_json['intermediate'] = 'Площадь земли в пределах периметра объекта(в м2): ' + Math.abs(parseFloat($('input[name="v_xg"]').val()));
//        result_json['data']['xg'] = Math.abs(parseFloat($('input[name="v_xg"]').val()));
//        result_json['data']['etap'] = mode_0;
//        result_json['data']['event'] = 1;
    } else if (mode_1 == 2) {
        result_json['event'] = 'Экспертиза проектной документации жилых объектов.';
        result_json['intermediate'] = 'Площадь земли в пределах периметра объекта(в м2): ' + $('#v_xg1').val() + '<br> Общая площадь объекта или помещений, подлежащих реконструкции, капитальному ремонту (в м2): ' + $('#v_yg1').val();
//        result_json['data']['xg'] = Math.abs(parseFloat($('input#v_xg1').val()));
//        result_json['data']['yg'] = Math.abs(parseFloat($('input#v_yg1').val()));
//        result_json['data']['etap'] = mode_0;
//        result_json['data']['event'] = 2;
//        result_json['data']['t1'] = $('#form2 input[name="counting1"]:radio:checked').val();
        
        if ($('#form2 input[name="counting1"]:checked').length) {
            if ($('#form2 input[name="counting1"]:checked').val() == 1) {
                result_json['intermediate'] = result_json['intermediate'] + '<br> Проектная документация предназначена для строительства или реконструкции объекта капитального строительства'
            } else if ($('#form2 input[name="counting1"]:checked').val() == 2) {
                result_json['intermediate'] = result_json['intermediate'] + '<br> Капитальный ремонт объекта капитального строительства'
            }
        }
        
    } else if (mode_1 == 3) {
        result_json['intermediate'] = 'Площадь земли в пределах периметра объекта(в м2): ' + $('#v_xg2').val() + '<br> Общая площадь объекта или помещений, подлежащих реконструкции, капитальному ремонту (в м2): ' + $('#v_yg2').val();
        if ($('#form2 input[name="counting3"]:checked').length) {
            if ($('#form2 input[name="counting3"]:checked').val() == 1) {
                result_json['intermediate'] = result_json['intermediate'] + '<br> Проектная документация предназначена для строительства или реконструкции объекта капитального строительства'
            } else if ($('#form2 input[name="counting3"]:checked').val() == 2) {
                result_json['intermediate'] = result_json['intermediate'] + '<br> Капитальный ремонт объекта капитального строительства'
            }
        }
       // result_json['data']['xg'] = Math.abs(parseFloat($('input#v_xg2').val()));
//        result_json['data']['yg'] = Math.abs(parseFloat($('input#v_yg2').val()));
//        result_json['data']['etap'] = mode_0;
//        result_json['data']['event'] = 3;
//        result_json['data']['t1'] = $('#form2 input[name="counting1"]:radio:checked').val();
        
        result_json['event'] = 'Экспертиза проектной документации и результатов инженерных изысканий жилых объектов.'
    } else if (mode_1 == 4) {
        result_json['event'] = 'Экспертиза проектной документации и результатов инженерных изысканий нежилых объектов.'
        result_json['intermediate']="Стоимость изготовления материалов инженерных изысканий, представленных на экспертизу, в ценах 2001 года без НДС, в руб.: "+Math.abs(parseFloat($('input#v_cig').val()))+"<br>Основной показатель проектируемого объекта: "+Math.abs(parseFloat($('input#v_x').val()));       
        result_json['settings_id']=Math.abs($('#getset select option:selected').val());
//        result_json['data']['exp_table'] = Math.abs($('#getset select option:selected').val());
//        result_json['data']['sig'] = Math.abs(parseFloat($('input#v_cig').val()));
//        result_json['data']['etap'] = mode_0;
//        result_json['data']['event'] = 4;
//        result_json['data']['x'] = Math.abs(parseFloat($('input#v_x').val())); 
    }
    result_json['cost'] = res;
    result_json['make_id']=mode_1;
    result_json['zone_id']=parseInt($('#zones option:selected').val());
    result_json['zone']=$('.combosex_input_span input').val();
    result_json['region']=$('#region option:selected').text();
    if ($('input[name=email]').length > 0) {        
            result_json['email'] = $('input[name=email]').val();
            result_json['phone'] = $('input[name=phone]').val();        
    }
    return JSON.parse(JSON.stringify(result_json))
}

function calc() {
    if (mode_0 != 0 || mode_1 != 0) {
        if (mode_1 == 1) {
            ajax_request_arr = {};
            ajax_request_arr['xg'] = Math.abs(parseFloat($('input[name="v_xg"]').val()));
            ajax_request_arr['etap'] = mode_0;
            ajax_request_arr['event'] = 1;
            ajax_request_arr['zone_id']=parseInt($('#zones option:selected').val());
            ajax_request_arr['zone']=$('.combosex_input_span input').val();
            ajax_request_arr['region']=$('#region option:selected').text();
            $.ajax({
                type: 'post',
                url: '/site/calculational',
                data: JSON.parse(JSON.stringify(ajax_request_arr)),
                success: function (data) {
                    res = data;
                    $('#wo_nds span').html(showCost(res) + ' руб.')
                },
                complete: function () {
                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        data: getData(ajax_request_arr),
                        url: ''
                    })
                }
            })
        } else if (mode_1 == 2) {
            ajax_request_arr = {};
            ajax_request_arr['xg'] = Math.abs(parseFloat($('input#v_xg1').val()));
            ajax_request_arr['yg'] = Math.abs(parseFloat($('input#v_yg1').val()));
            ajax_request_arr['etap'] = mode_0;
            ajax_request_arr['event'] = 2;
            ajax_request_arr['t1'] = $('input[name="counting1"]:radio:checked').val();
            ajax_request_arr['zone_id']=parseInt($('#zones option:selected').val());
            ajax_request_arr['zone']=$('.combosex_input_span input').val();
            ajax_request_arr['region']=$('#region option:selected').text();
            $.ajax({
                type: 'post',
                url: '/site/calculational',
                data: JSON.parse(JSON.stringify(ajax_request_arr)),
                success: function (data) {
                    res = data;
                    $('#wo_nds span').html(showCost(res) + ' руб.')
                },
                complete: function () {
                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        data: getData(ajax_request_arr),
                        url: ''
                    })
                }
            })
        } else if (mode_1 == 3) {
            ajax_request_arr = {};
            ajax_request_arr['xg'] = Math.abs(parseFloat($('input#v_xg2').val()));
            ajax_request_arr['yg'] = Math.abs(parseFloat($('input#v_yg2').val()));
            ajax_request_arr['etap'] = mode_0;
            ajax_request_arr['event'] = 3;
            ajax_request_arr['t1'] = $('input[name="counting1"]:radio:checked').val();
            ajax_request_arr['zone_id']=parseInt($('#zones option:selected').val());
            ajax_request_arr['zone']=$('.combosex_input_span input').val();
            ajax_request_arr['region']=$('#region option:selected').text();
            $.ajax({
                type: 'post',
                url: '/site/calculational',
                data: JSON.parse(JSON.stringify(ajax_request_arr)),
                success: function (data) {
                    res = data;
                    $('#wo_nds span').html(showCost(res) + ' руб.')
                },
                complete: function () {
                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        data: getData(ajax_request_arr),
                        url: ''
                    })
                }
            })
        } else if (mode_1 == 4) {
            ajax_request_arr = {};
            ajax_request_arr['exp_table'] = Math.abs($('#getset select option:selected').val());
            ajax_request_arr['sig'] = Math.abs(parseFloat($('input#v_cig').val()));
            ajax_request_arr['etap'] = mode_0;
            ajax_request_arr['event'] = 4;
            ajax_request_arr['x'] = Math.abs(parseFloat($('input#v_x').val()));   
            ajax_request_arr['zone_id']=parseInt($('#zones option:selected').val());
            ajax_request_arr['zone']=$('.combosex_input_span input').val();
            ajax_request_arr['region']=$('#region option:selected').text();
            $.ajax({
                type: 'post',
                url: '/site/calculational',
                data: JSON.parse(JSON.stringify(ajax_request_arr)),
                success: function (data) {
                    res = data;
                    $('#wo_nds span').html(showCost(res)+ ' руб.')
                },
                complete: function () {
                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        data: getData(ajax_request_arr),
                        url: ''
                    })
                }
            })
        }
        $('#cl_result').show()
    }
    return false
}

function show_block(k) {
    var cont = new Array($('#cl_0'), $('#cl_1'), $('#cl_2'), $('#cl_3'), $('#cl_4'));
    for (var i in cont) {
        if (!cont.hasOwnProperty(i)) continue;
        if (i != 0 && i != k) {
            cont[i].hide()
        }
    }
    cont[k].show();
    return false
}
function showCost(str_cost)
{
    return str_cost.replace('.',',');
}