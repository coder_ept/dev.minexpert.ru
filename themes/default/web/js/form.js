
//FIO form split
//fio on init
$(document).ready(function(){
    //rukovoditel
    var fio1 = $("#Statement_ceo_company").val().split(" ");
    $('#ceo_surname').val(fio1[0]);
    $('#ceo_name').val(fio1[1]);
    $('#ceo_patronimic').val(fio1[2]);
    //buhgalter
    var fio2 = $("#Statement_chief_buhgalter").val().split(" ");
    $('#buhgalter_surname').val(fio2[0]);
    $('#buhgalter_name').val(fio2[1]);
    $('#buhgalter_patronimic').val(fio2[2]);
    //manager
    var fio3 = $("#Statement_manager_object_legal_entity_organization").val().split(" ");
    $('#manager_surname').val(fio3[0]);
    $('#manager_name').val(fio3[1]);
    $('#manager_patronimic').val(fio3[2]);
    //fio gipa
    var fio41 = $("#Statement_fio_gipa").val().split(" ");
    $('#gipa_surname').val(fio41[0]);
    $('#gipa_name').val(fio41[1]);
    $('#gipa_patronimic').val(fio41[2]);
    //fio gipa1
    var fio4 = $("#Statement_fio_gipa_accomplice1").val().split(" ");
    $('#gipa_surname_accomplice1').val(fio4[0]);
    $('#gipa_name_accomplice1').val(fio4[1]);
    $('#gipa_patronimic_accomplice1').val(fio4[2]);
    //fio gipa2
    var fio5 = $("#Statement_fio_gipa_accomplice2").val().split(" ");
    $('#gipa_surname_accomplice2').val(fio5[0]);
    $('#gipa_name_accomplice2').val(fio5[1]);
    $('#gipa_patronimic_accomplice2').val(fio5[2]);
    //fio gipa3
    var fio6 = $("#Statement_fio_gipa_accomplice3").val().split(" ");
    $('#gipa_surname_accomplice3').val(fio6[0]);
    $('#gipa_name_accomplice3').val(fio6[1]);
    $('#gipa_patronimic_accomplice3').val(fio6[2]);

});
//rukovoditel on change
var fio_ruk;
$('#ceo_surname').focusout(function()
{
    fio_ruk = $(this).val()+' '+$("#ceo_name").val()+' '+$("#ceo_patronimic").val();
    $("#Statement_ceo_company").val(fio_ruk);
});
$('#ceo_name').focusout(function()
{
    fio_ruk = $("#ceo_surname").val()+' '+$(this).val()+' '+$("#ceo_patronimic").val();
    $("#Statement_ceo_company").val(fio_ruk);
});
$('#ceo_patronimic').focusout(function()
{
    fio_ruk = $("#ceo_surname").val()+' '+$("#ceo_name").val()+' '+$(this).val();
    $("#Statement_ceo_company").val(fio_ruk);
});
//rukovoditel end
//buhgalter on change chief_buhgalter
var fio_buh;
$('#buhgalter_surname').focusout(function()
{
    fio_buh = $(this).val()+' '+$("#buhgalter_name").val()+' '+$("#buhgalter_patronimic").val();
    $("#Statement_chief_buhgalter").val(fio_buh);
});
$('#ceo_name').focusout(function()
{
    fio_ruk = $("#buhgalter_surname").val()+' '+$(this).val()+' '+$("#buhgalter_patronimic").val();
    $("#Statement_chief_buhgalter").val(fio_buh);
});
$('#buhgalter_patronimic').focusout(function()
{
    fio_buh = $("#buhgalter_surname").val()+' '+$("#buhgalter_name").val()+' '+$(this).val();
    $("#Statement_chief_buhgalter").val(fio_buh);
});
//buhgalter end
//manager on change
var fio_man;
$('#manager_surname').focusout(function()
{
    fio_man = $(this).val()+' '+$("#manager_name").val()+' '+$("#manager_patronimic").val();
    $("#Statement_manager_object_legal_entity_organization").val(fio_man);
});
$('#manager_name').focusout(function()
{
    fio_man = $("#manager_surname").val()+' '+$(this).val()+' '+$("#manager_patronimic").val();
    $("#Statement_manager_object_legal_entity_organization").val(fio_man);
});
$('#manager_patronimic').focusout(function()
{
    fio_man = $("#manager_surname").val()+' '+$("#manager_name").val()+' '+$(this).val();
    $("#Statement_manager_object_legal_entity_organization").val(fio_man);
});
//manager end
//fio gipa on change
var fio_man;
$('#manager_surname').focusout(function()
{
    fio_man = $(this).val()+' '+$("#manager_name").val()+' '+$("#manager_patronimic").val();
    $("#Statement_manager_object_legal_entity_organization").val(fio_man);
});
$('#manager_name').focusout(function()
{
    fio_man = $("#manager_surname").val()+' '+$(this).val()+' '+$("#manager_patronimic").val();
    $("#Statement_manager_object_legal_entity_organization").val(fio_man);
});
$('#manager_patronimic').focusout(function()
{
    fio_man = $("#manager_surname").val()+' '+$("#manager_name").val()+' '+$(this).val();
    $("#Statement_manager_object_legal_entity_organization").val(fio_man);
});
//fio gipa end
//fio gipa1 on change
var fio_man;
$('#manager_surname').focusout(function()
{
    fio_man = $(this).val()+' '+$("#manager_name").val()+' '+$("#manager_patronimic").val();
    $("#Statement_manager_object_legal_entity_organization").val(fio_man);
});
$('#manager_name').focusout(function()
{
    fio_man = $("#manager_surname").val()+' '+$(this).val()+' '+$("#manager_patronimic").val();
    $("#Statement_manager_object_legal_entity_organization").val(fio_man);
});
$('#manager_patronimic').focusout(function()
{
    fio_man = $("#manager_surname").val()+' '+$("#manager_name").val()+' '+$(this).val();
    $("#Statement_manager_object_legal_entity_organization").val(fio_man);
});
//fio gipa1 end
//fio gipa2 on change
var fio_man;
$('#manager_surname').focusout(function()
{
    fio_man = $(this).val()+' '+$("#manager_name").val()+' '+$("#manager_patronimic").val();
    $("#Statement_manager_object_legal_entity_organization").val(fio_man);
});
$('#manager_name').focusout(function()
{
    fio_man = $("#manager_surname").val()+' '+$(this).val()+' '+$("#manager_patronimic").val();
    $("#Statement_manager_object_legal_entity_organization").val(fio_man);
});
$('#manager_patronimic').focusout(function()
{
    fio_man = $("#manager_surname").val()+' '+$("#manager_name").val()+' '+$(this).val();
    $("#Statement_manager_object_legal_entity_organization").val(fio_man);
});
//fio gipa2 end
//fio gipa3 on change
var fio_man;
$('#manager_surname').focusout(function()
{
    fio_man = $(this).val()+' '+$("#manager_name").val()+' '+$("#manager_patronimic").val();
    $("#Statement_manager_object_legal_entity_organization").val(fio_man);
});
$('#manager_name').focusout(function()
{
    fio_man = $("#manager_surname").val()+' '+$(this).val()+' '+$("#manager_patronimic").val();
    $("#Statement_manager_object_legal_entity_organization").val(fio_man);
});
$('#manager_patronimic').focusout(function()
{
    fio_man = $("#manager_surname").val()+' '+$("#manager_name").val()+' '+$(this).val();
    $("#Statement_manager_object_legal_entity_organization").val(fio_man);
});
//fio gipa3 end

function zay() {
    if($('#zaycheck').val()!=1){
        $('.zay').hide();
    }
    else{
        $('.zay').show();
    }
}
function zpr() {
    if($('#zprcheck').val()!=1){
        $('.zpr').hide();
    }
    else{
        $('.zpr').show();
    }
}
function a1post() {
    if ($("#a1_post").prop("checked")) {
        $('.a1_post').hide();
    } else {
        $('.a1_post').show();
    }
}
function soisp() {
    if ($("#soispcheck").prop("checked")) {
        $('.soisp').show();
    } else {
        $('.soisp').hide();
    }
}

function ztz() {
    if($('#ztzcheck').val()!=1){
        $('.ztz').hide();
    }
    else{
        $('.ztz').show();
    }
}

function isEmptyObject(obj) {
    for (var name in obj) {
        return false
    }
    return true
}

$(function () {
    $("#tabs").tabs();
    if (!isEmptyObject(json_data)) {
    $('#Statement_firstTime').attr('disabled', 'true');
    // $('#Statement_claimed_estimated_cost').attr('readonly','true');
    $('#Statement_firstTime option:eq(' + (json_data.etap - 1) + ')').attr('selected', 'selected');
    // $('#Statement_claimed_estimated_cost').val(json_data.cost);
    var div = document.createElement("div");
    div.innerHTML = address;
    address = (div.innerText);
    $('#Statement_address_object').val(address.match("Место строительства: (.*)")[1]);
    if (json_data.event == 2 || json_data.event == 3) {
    $('#Statement_type_construction option[value="' + (parseInt(json_data.t1) + 1) + '"]').attr('selected', 'selected');
    $('#Statement_type_construction').attr('disabled', 'true');
    }
}
var ww = $(window).width();
var cw = $('.wrap').width();
var lw = $('#logo').width();
if (ww > cw) {
    $('.wrap').css({
        left:(ww - cw) / 2,
        display:'inline-block'
    });
$('#crumbsBlock').css({
    left:((ww - cw) / 2 + lw - 106),
    display:'block'
    });
} else {
    $('#crumbsBlock').css({
        left:lw - 106,
        width:300,
        display:'block'
    })
}
;
//alert($('#insideCont').height());
var ch = $('#insideCont').height();
$('.insidePage').css('height', (ch + 150));
$('.soisp').hide();
//$('.zpr').hide();
//$('.ztz').hide();
})
$(window).resize(function () {
    var ww = $(window).width();
    var cw = $('.wrap').width();
    var lw = $('#logo').width();
    if (ww > cw) {
    $('.wrap').css('left', (ww - cw) / 2);
    $('#crumbsBlock').css('left', ((ww - cw) / 2 + lw - 106));
    } else {
    $('#crumbsBlock').css({
        left:lw - 106,
        width:300
    })
}
;
var ch = $('#insideCont').height();
$('.insidePage').css('height', (ch + 150));
})
function add() {
    incID = parseInt($('table.inventory tbody tr:last td:first').html()) + 1;
    $('<tr><td>' + incID + '</td><td><input name="nameDoc[' + incID + ']" type="text" style="width: 700px"></td><td><input name="countItems[' + incID + ']" type="text" style="width: 50px"></td><td><a style="cursor: pointer" onclick="del(this)"><div class="icon-minus"></div> Удалить</a></td></tr>').insertAfter($('table.inventory tbody tr:last'));
    }
function del(object) {
    $(object).parent().parent().remove();
    var counter = 1;
    $('table.inventory tbody tr').each(function () {
    $(this).find('td:first').html(counter);
    $(this).find('td:eq(1) input').attr('name', 'nameDoc[' + counter + ']');
    $(this).find('td:eq(2) input').attr('name', 'countItems[' + counter + ']');
    counter++;
    });
}
$(document).ready(function () {
    $('.zpr').show();
    $('#Statement_a1_phone, #Statement_phone_legal_entity_organization, #Statement_phone_legal_entity_organization_contact, #Statement_phone_project_organization, #Statement_phone_survey_organization, #Statement_phone_gipa, #Statement_phone_survey2_organization, #Statement_phone_contact_person_customer_buider, #Statement_phone_applicant, #Statement_phone_company, #Statement_phone_contact_person, #Statement_phone_contact_person, #Statement_phone_customer_buider, #Statement_a1_manager_phone').mask('+7 (9999) 99-99-99');

    $('#Statement_plat_inn').on("keyup",function(){
    if(isNaN(parseInt($(this).val()))){
    $(this).val('');
    }
else {
    $(this).val(parseInt($(this).val()));
    }
if(parseInt($(this).val()).toString().length>12) {
    $(this).val(parseInt($(this).val()).toString().substring(0,12));
    }
});
$('#Statement_plat_inn').focusout(function(){
    if(parseInt($(this).val()).toString().length==10||parseInt($(this).val()).toString().length==12){

    }
else {
    console.log(parseInt($(this).val()).toString().length);
    $(this).val('');
    }
});
$('#Statement_plat_kpp').mask('999999999');
$('#Statement_plat_ogrn').mask('9999999999999');
$('#Statement_plat_rs').mask('99999999999999999999');
$('#Statement_plat_ks').mask('99999999999999999999');
$('#Statement_plat_bik').mask('999999999');
$('.index_field').mask('999999');

$('#Statement_source_of_finance').change(function () {
    var optionSelected = $(this).find("option:selected");
    if (optionSelected.val() == 2) {
    $('#credit_loan').show();
    } else {
    $('#credit_loan').hide();
    }

});
});
function plat() {
    if ($('#Statement_a1_name_full').val() == 'Индивидуальный предприниматель') {
    $('#Statement_plat_inn').attr('maxlength', 10);
    $('#kpp').hide();

    } else {
    $('#Statement_plat_inn').attr('maxlength', 12);
    $('#kpp').show();
    }
;
}