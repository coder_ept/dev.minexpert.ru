$(document).ready(function(){
$.validator.setDefaults({
    ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input'
});
$("#registration-form").validate({
    rules: {
        "RegistrationForm[email]": {
            required: true,
            email: true
        },
        "RegistrationForm[first_name]": {
            required: true,
            minlength: 2
        },
        "RegistrationForm[middle_name]": {
            required: true,
            minlength: 2
        },
        "RegistrationForm[last_name]": {
            required: true,
            minlength: 2
        },
        "RegistrationForm[location]": {
            required: true
        },
        "RegistrationForm[company]": {
            required: true,
            minlength: 2
        },
        "RegistrationForm[phone]": {
            required: true,
            digits: true,
            minlength: 11
        },
        "RegistrationForm[birth_date]": {
            required: true
        },
        "RegistrationForm[password]":{
            required: true,
            minlength: 6
        },
        "RegistrationForm[cPassword]": {
            required: true,
            equalTo: "#RegistrationForm_password"
        }
    },
    messages: {
        "RegistrationForm[email]": {
            required: "Пожалуйста, укажите Ваш контактный адрес электронной почты",
            email: "Введите действительный почтовый ящик"
        },
        "RegistrationForm[first_name]": {
            required: "Пожалуйста, укажите Ваше имя",
            minlength: "Имя должно содержать хотя бы 2 символа"
        },
        "RegistrationForm[middle_name]": {
            required: "Пожалуйста, укажите Ваше отчество",
            minlength: "Отчество должно содержать хотя бы 2 символа"
        },
        "RegistrationForm[last_name]": {
            required: "Пожалуйста, укажите Вашу фамилию",
            minlength: "Фамилия должна содержать хотя бы 2 символа"
        },
        "RegistrationForm[location]": {
            required: "Пожалуйста, укажите город в котором проживаете"
        },
        "RegistrationForm[company]": {
            required: "Пожалуйста, укажите название компании, которую Вы представляете",
            minlength: "В названии компании должно быть хотя бы 2 символа"
        },
        "RegistrationForm[phone]": {
            required: "Пожалуйста, укажите Ваш контактный номер телефона",
            digits: "Телефон должен состоять только из цифр, например: 79999999999",
            minlength: "Телефон должен содержать 11 цифр"
        },
        "RegistrationForm[birth_date]": {
            required: "Укажите дату рождения"
        },
        "RegistrationForm[password]": {
            required: "Введите пароль для Вашей учетной записи",
            minlength: "Пароль должен содержать минимум 6 символов"
        },
        "RegistrationForm[cPassword]": {
            required: "Введите пароль для Вашей учетной записи",
            equalTo: "Пароли должны совпадать"
        }
    }
});
    $("#RegistrationForm_phone").focusout(function(){
if($('.phone_check').length!=0){

}
        else{
    $('input[type=submit]', this).attr('disabled', 'disabled');
    //$('#registration-form').on('click',function(e){e.preventDefault();});
       $(this).after('<div class="phone_check">' +
           '<p>Проверка контактного номера телефона.<br/>' +
           'Убедитесь в правильности, введенного Вами в поле выше телефона, и нажмите "Отправить СМС", полученный код введите в поле ниже.</p>' +
           '<div style="align:center;"><a class="send_sms" style="background-color:#786b59;font-size:8pt;border:0px;cursor:pointer;padding:5px;">Отправить СМС</a> </div> <label id="lo" style="color:#000;"></label> <br/>' +
           //'<div style="align:center;"><a class="send_sms" style="background-color:#786b59;font-size:8pt;border:0px;cursor:pointer;padding:5px;">Отправить СМС</a> </div> <label id="lo" style="color:#000;"></label> <br/>' +
           '<label for="phone_check">Проверочный код:</label>' +
           '<input type="text" name="phone_check" id="phone_check2"/>' +
                '<div style="align:center;"><a  style="background-color:#786b59;font-size:8pt;border:0px;cursor:pointer;padding:5px;" id="phone_check">Проверка</a> </div> <label id="ph" style="color:#000;"></label> <br/>' +
           '</div>'+
           '<div class="msg"></div>');
        $('.send_sms').click(function()
        {
            $(this).hide();
            var phone = $("#RegistrationForm_phone").val();
            $("#lo").html("Отправка ...");
            $.ajax(
                {
                    url : "/sms.php",
                    type: "POST",
                    data : "phone="+phone,
                    success:function(data)
                    {
                        console.log(data);
                        $("#lo").html("Отправлено! Ожидайте от 5 до 10 минут.");
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("Возникла ошибка, попробуйте еще раз");
                        $("#lo").html("");
                    }
                });
        });
        //$('#phone_check').focusout(function(){
        $('#phone_check').on('click',function(){
            var phone = $("#RegistrationForm_phone").val();
            var sms_code = $("#phone_check2").val();
            console.log(sms_code);
            console.log(phone);
            $("#ph").html("Проверка ...");
            $.ajax(
                {
                    url : "/sms.php",
                    type: "POST",
                    data : "phone="+phone+"&smscode="+sms_code,
                    success:function(data)
                    {
                        console.log(data);
                        if(data=="wright"){
                            $(".msg").html('Код введен верно');
                            $('.phone_check').hide();
                            $('input[type=submit]', this).removeAttr('disabled', 'disabled');
                            //$('#registration-form').unbind('submit');
                        }
                        else {
                            $(".msg").html('Код введен неверно');
                            $('input[type=submit]', this).attr('disabled', 'disabled');
                            //$('#registration-form').bind('submit',function(e){e.preventDefault();});
                        }
                        $("#ph").html("");
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("Возникла ошибка, попробуйте еще раз");
                    }
                });
        });
}
    });

});

