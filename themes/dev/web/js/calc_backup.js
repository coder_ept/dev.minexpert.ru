var mode_0 = 0;
var mode_1 = 0;
var mode_2 = 0;
var res = 0;
$(function () {
    var cont = new Array($('#cl_0'), $('#cl_1'), $('#cl_2'), $('#cl_3'), $('#cl_4'));
    cont[0].hide();
    cont[1].hide();
    cont[2].hide();
    cont[3].hide();
    cont[4].hide();
    mode_0 = $('#form1 input:radio:checked').val();
    cont[0].show();
    mode_1 = $('#form2 input[name="counting"]:radio:checked').val();
    show_block(mode_1);
    $('#form1 input[name="count"]').click(function () {
        mode_0 = $('#form1 input:radio:checked').val();
        cont[0].show()
    });
    $('#form2 input[name="counting"]').click(function () {
        mode_1 = $('#form2 input[name="counting"]:radio:checked').val();
        $('.rows#input_data').empty();
        $('.rows#input_data').hide();
        show_block(mode_1)
    });
    $('#form2 input[name="counting5"]:radio').click(function () {
        var mode_3 = $('#form2 input[name="counting5"]:radio:checked').val()
    });
    $('#calc').click(function () {
        if ($('.tab-pane.fade.active.in').attr('id') == 'yw0_tab_3') {
            calc()
        }
    });
    $('select').chosen();
    var ww = $(window).width();
    var cw = $('.wrap').width();
    var lw = $('#logo').width();
    if (ww > cw) {
        $('.wrap').css('left', (ww - cw) / 2);
        $('#crumbsBlock').css('left', ((ww - cw) / 2 + lw - 126))
    };
    var ch = $('#insideCont').height();
    $('.insidePage').css('height', (ch + 200));
    $('select[name="counting5"]').on('change', function () {
        $.ajax({
            type: 'post',
            url: '/site/GetSettings',
            data: 'cat_id=' + $('select[name="counting5"] option:selected').val(),
            success: function (data) {
                $('#getset').remove();
                $('.rows#input_data').empty();
                $('.rows#input_data').hide();
                $('.subCalculate#cl_4').append(data);
                $('select').chosen()
            }
        })
    });
    $('input[value="4"]').click(function () {
        $('input[name="counting5"]:not(:checked)').parent().show();
        $('input[name="counting5"]').parent().find('div').empty();
        $('input[name="counting5"]').parent().find('div').hide()
    })
});

function getInputData(id) {
    if (id > 0) {
        $('#d_cig').remove();
        $('#d_x').remove();
        $.ajax({
            type: 'post',
            data: 'id=' + id,
            url: '/site/GetInputData',
            success: function (data) {
                $('.rows#input_data').append(data);
                $('.rows#input_data').show()
            }
        })
    }
}

function getData() {
    var result_json = {};
    var intermediate = $('form').serializeArray();
    if (mode_0 == 1) result_json['etap'] = 'Первичная экспертиза';
    else result_json['etap'] = 'Вторичная экспертиза'; if (mode_1 == 1) {
        result_json['event'] = 'Экспертиза результатов инженерных изысканий, выполняемых для строительства, реконструкции, капитального ремонта жилых объектов капитального строительства.';
        result_json['intermediate'] = 'Площадь земли, измеряемая в пределах периметра жилого объекта капитального строительства (в м2): ' + Math.abs(parseFloat($('input[name="v_xg"]').val()))
    } else if (mode_1 == 2) {
        result_json['event'] = 'Экспертиза проектной документации жилых объектов капитального строительства.';
        result_json['intermediate'] = 'Площадь земли, измеряемая в пределах периметра жилого объекта капитального строительства (в м2): ' + $('#v_xg1').val() + '<br> Общая площадь жилого объекта капитального строительства при его новом строительстве либо общая площадь помещений, подлежащих реконструкции, капитальному ремонту (в м2): ' + $('#v_yg1').val();
        if ($('#form2 input[name="counting1"]:checked').length) {
            if ($('#form2 input[name="counting1"]:checked').val() == 1) {
                result_json['intermediate'] = result_json['intermediate'] + '<br> Проектная документация предназначена для строительства или реконструкции объекта капитального строительства'
            } else if ($('#form2 input[name="counting1"]:checked').val() == 2) {
                result_json['intermediate'] = result_json['intermediate'] + '<br> Капитальный ремонт объекта капитального строительства'
            }
        }
    } else if (mode_1 == 3) {
        result_json['intermediate'] = 'Площадь земли, измеряемая в пределах периметра жилого объекта капитального строительства (в м2): ' + $('#v_xg2').val() + '<br> Общая площадь жилого объекта капитального строительства при его новом строительстве либо общая площадь помещений, подлежащих реконструкции, капитальному ремонту (в м2): ' + $('#v_yg2').val();
        if ($('#form2 input[name="counting3"]:checked').length) {
            if ($('#form2 input[name="counting3"]:checked').val() == 1) {
                result_json['intermediate'] = result_json['intermediate'] + '<br> Проектная документация предназначена для строительства или реконструкции объекта капитального строительства'
            } else if ($('#form2 input[name="counting3"]:checked').val() == 2) {
                result_json['intermediate'] = result_json['intermediate'] + '<br> Капитальный ремонт объекта капитального строительства'
            }
        }
        result_json['event'] = 'Экспертиза проектной документации жилых объектов капитального строительства и результатов инженерных изысканий, выполняемых для подготовки такой проектной документации.'
    } else if (mode_1 == 4) {
        result_json['event'] = 'Экспертиза проектной документации нежилых объектов капитального строительства и (или) результатов инженерных изысканий, выполняемых для подготовки такой проектной документации.'
    }
    result_json['cost'] = res;
    if ($('input[name=email]').length > 0) {
        if ($('input[name=email]').val() != "") {
            result_json['email'] = $('input[name=email]').val()
        }
    }
    return JSON.parse(JSON.stringify(result_json))
}

function calc() {
    if (mode_0 != 0 || mode_1 != 0) {
        if (mode_1 == 1) {
            ajax_request_arr = {};
            ajax_request_arr['xg'] = Math.abs(parseFloat($('input[name="v_xg"]').val()));
            ajax_request_arr['etap'] = mode_0;
            ajax_request_arr['event'] = 1;
            $.ajax({
                type: 'post',
                url: '/site/calculational',
                data: JSON.parse(JSON.stringify(ajax_request_arr)),
                success: function (data) {
                    res = data;
                    $('#wo_nds span').html(res + ' руб.')
                },
                complete: function () {
                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        data: getData(),
                        url: ''
                    })
                }
            })
        } else if (mode_1 == 2) {
            ajax_request_arr = {};
            ajax_request_arr['xg'] = Math.abs(parseFloat($('input#v_xg1').val()));
            ajax_request_arr['yg'] = Math.abs(parseFloat($('input#v_yg1').val()));
            ajax_request_arr['etap'] = mode_0;
            ajax_request_arr['event'] = 2;
            ajax_request_arr['t1'] = $('#form2 input[name="counting1"]:radio:checked').val();
            $.ajax({
                type: 'post',
                url: '/site/calculational',
                data: JSON.parse(JSON.stringify(ajax_request_arr)),
                success: function (data) {
                    res = data;
                    $('#wo_nds span').html(res + ' руб.')
                },
                complete: function () {
                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        data: getData(),
                        url: ''
                    })
                }
            })
        } else if (mode_1 == 3) {
            ajax_request_arr = {};
            ajax_request_arr['xg'] = Math.abs(parseFloat($('input#v_xg2').val()));
            ajax_request_arr['yg'] = Math.abs(parseFloat($('input#v_yg2').val()));
            ajax_request_arr['etap'] = mode_0;
            ajax_request_arr['event'] = 3;
            ajax_request_arr['t1'] = $('#form2 input[name="counting1"]:radio:checked').val();
            $.ajax({
                type: 'post',
                url: '/site/calculational',
                data: JSON.parse(JSON.stringify(ajax_request_arr)),
                success: function (data) {
                    res = data;
                    $('#wo_nds span').html(res + ' руб.')
                },
                complete: function () {
                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        data: getData(),
                        url: ''
                    })
                }
            })
        } else if (mode_1 == 4) {
            ajax_request_arr = {};
            ajax_request_arr['exp_table'] = Math.abs($('#getset select option:selected').val());
            ajax_request_arr['sig'] = Math.abs(parseFloat($('input#v_cig').val()));
            ajax_request_arr['etap'] = mode_0;
            ajax_request_arr['event'] = 4;
            ajax_request_arr['x'] = Math.abs(parseFloat($('input#v_x').val()));
            
            $.ajax({
                type: 'post',
                url: '/site/calculational',
                data: JSON.parse(JSON.stringify(ajax_request_arr)),
                success: function (data) {
                    res = data;
                    $('#wo_nds span').html(res + ' руб.')
                },
                complete: function () {
                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        data: getData(),
                        url: ''
                    })
                }
            })
        }
        $('#cl_result').show()
    }
    return false
}

function show_block(k) {
    var cont = new Array($('#cl_0'), $('#cl_1'), $('#cl_2'), $('#cl_3'), $('#cl_4'));
    for (var i in cont) {
        if (!cont.hasOwnProperty(i)) continue;
        if (i != 0 && i != k) {
            cont[i].hide()
        }
    }
    cont[k].show();
    return false
}