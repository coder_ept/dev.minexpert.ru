
        <?php if(isset($models) && $models != array()): ?>
            <ul>
                <?php foreach ($models as $model): ?>                    
                    
                    <li><a href="/story/<?=$model->alias?>" title="">
                    	<div class="mainNewsItem">
                    		<div class="mainNewsItemCont">
                    			<div class="mainNewsItemDate"><?=Registr::rdate($model->date).' '.date('Y',strtotime($model->date))?></div>
                    			<div class="mainNewsItemHeader"><?=$model->title?></div>
                    			<div class="mainNewsItemPic"><img src="/uploads/news/<?=$model->image?>" style="height: 150px;width: 245px;" /></div>
                    			<div class="mainNewsItemText"><?=mb_substr(strip_tags($model->short_text),1,150,'UTF-8').'...'?></div>
                    		</div>
                    		<img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/evt_bg.png">
                    	</div>
                    </a></li>
                <?php endforeach;?>
            </ul>
        <?php endif; ?>
                    