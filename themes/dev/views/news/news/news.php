<?php
/**
 * Отображение для ./themes/default/views/news/news/news.php:
 * 
 *   @category YupeView
 *   @package  YupeCMS
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
 ?>
<?php $this->pageTitle = $news->title.". ".$this->pageTitle ?>

<div id="crumbsBlock" style="overflow: hidden; display: none"><p><a style="color: white;" href="/">Главная </a> > 
<a href="/news" style="color: white;">Новости</a> > <?=CHtml::encode($news->title)?></p></div>
<div class="insidePage">
	<div id="insideCont" style="position: absolute; text-align: center; margin-top: 80px; display: none" class="wrap">

<table>
	<tr>
		<td style="vertical-align: top; background-color: #fff; padding: 30px;">
			<?php echo (!empty($news->image) ? CHtml::image(Yii::app()->request->baseUrl . '/uploads/news/' . $news->image, $news->title, array('style' => 'width: 370px; max-width: 370px; height: 243px; float: left; padding: 0 30px 15px 0')) : '');?>
			<h1 style="font-weight: normal; font-size: 25px; text-align: left; padding: 0; margin-top: -6px; line-height: 30px"><?php echo $news->title; ?></h1>
			<?php echo $news->full_text; ?>
		</td>
		<td style="width: 10px; padding: 0; background-color: rgba(0,0,0,0); background-image: url('<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/triangle_white.png');"></td>
	</tr>
</table>



 
    <!--div class="content">
    <div class="span4"><?php echo (!empty($news->image) ? CHtml::image(Yii::app()->request->baseUrl . '/uploads/news/' . $news->image, $news->title, array('style' => 'width: 370px; max-width: 370px; height: 243px')) : '');?></div>
    <div class="span8"><p><?php echo $news->full_text; ?></p></div-->    
        



</div>
	<img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/inside_bg1.png" class="insideBg">
</div>




<?php //$this->widget('application.modules.comment.widgets.CommentsListWidget', array('model' => $news, 'modelId' => $news->id)); ?>

<?php //$this->widget('application.modules.comment.widgets.CommentFormWidget', array('redirectTo' => $news->getPermaLink(), 'model' => $news, 'modelId' => $news->id)); ?>

   <script type="text/javascript">
	$(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css({
				left: (ww - cw)/2,
				display: 'inline-block'
			});
            $('#crumbsBlock').css({
            	left: ((ww - cw)/2+lw-106),
            	display: 'block'
            });
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 650,
				display: 'block'
			})
			$('.wrap').css({
				display: 'inline-block'
			});		
			$('#crumbsBlock p').css('font-size', '14px');		
		};
		$('.docm').css('width', (cw - 170));
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 150));

	})
	$(window).resize(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
            $('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-126,
				width: 650
			})
			$('#crumbsBlock p').css('font-size', '14px');	
		};
		$('.docm').css('width', (cw - 170));

	})
</script>