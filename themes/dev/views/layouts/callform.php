 <? 
    // определяем referer
 $referer ='';
 if (isset($_SERVER['HTTP_REFERER'])){
    $referer = $_SERVER['HTTP_REFERER'];    
 }

$phrase='';
$crawler='';
$search='';
// ищем в referer адреса поисковиков и присваиваем переменным
// $search и $crawler соответствующие значения
if (stristr($referer, 'yandex.ua')) { $search = 'text='; $crawler = 'Yandex'; }
if (stristr($referer, 'yandex.ru')) { $search = 'text='; $crawler = 'Yandex'; }
if (stristr($referer, 'rambler.ru')) { $search = 'query='; $crawler = 'Rambler'; }
if (stristr($referer, 'google.ru')) { $search = 'q='; $crawler = 'Google'; }
if (stristr($referer, 'google.com')) { $search = 'q='; $crawler = 'Google'; }
if (stristr($referer, 'mail.ru')) { $search = 'q='; $crawler = 'Mail.Ru'; }
if (stristr($referer, 'bing.com')) { $search = 'q='; $crawler = 'Bing'; }
if (stristr($referer, 'qip.ru')) { $search = 'query='; $crawler = 'QIP'; }

// если посетитель пришел с поисковика то выполняем следующий код

if (isset($crawler) && !empty($crawler))
{    
// здесь мы приводим referer в понятный для человека вид
$phrase = urldecode($referer);

// ищем ключевое слово в referer
eregi($search.'([^&]*)', $phrase.'&', $phrase2);

$phrase = $phrase2[1];
Yii::app()->session['crawler']=$crawler;
Yii::app()->session['phrase']=$phrase;
}
?>
 <!-- Модальное окно для заказа консультации -->
 <div id="consult" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Заказать консультацию</h3>
      </div>
      <div class="modal-body">        
        <?php $consultModal=new ConsultForm;?>
        <div class="form">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'consult_form',
            'action'=>'#',
            'enableAjaxValidation'=>true,
            'enableClientValidation'=>true,
            'focus'=>array($consultModal,'firstName'),
            
        )); ?>
         
            <div id="consult_form_es_" class="errorSummary" style="display:none"><ul></ul></div>
         
            <div class="row-fluid">
                <?php echo $form->label($consultModal,'name',array('class'=>'span4')); ?>
                <?php echo $form->textField($consultModal,'name') ?>
            </div>    
             <div class="row-fluid">
                <?php echo $form->label($consultModal,'number',array('class'=>'span4')); ?>
                <?php echo $form->textField($consultModal,'number') ?>
            </div>
            <div class="row-fluid">
                <?php echo $form->label($consultModal,'email',array('class'=>'span4')); ?>
                <?php echo $form->textField($consultModal,'email') ?>
            </div>     
            <input name="referer" type="hidden" value="<?=Yii::app()->session['crawler']?>">
            <input name="phrase" type="hidden" value="<?=Yii::app()->session['phrase']?>">
            <div class="row-fluid">
                <?php echo $form->label($consultModal,'city',array('class'=>'span4')); ?>
                <?php echo $form->dropDownList($consultModal,'city',City::All(),array('empty'=>'Выберите город','class'=>'chzn-select')) ?>
            </div>
            <div class="row-fluid">
                <?php echo $form->label($consultModal,'company',array('class'=>'span4')); ?>
                <?php echo $form->textField($consultModal,'company') ?>
            </div>
            <div class="row-fluid">
                <?php echo $form->label($consultModal,'theme',array('class'=>'span4')); ?>
                <?php echo $form->textArea($consultModal,'theme', array('style'=>'height: 90px')) ?>
            </div>
                  
         	<br>
            <div class="row-fluid">
<?=CHtml::ajaxSubmitButton('Отправить','/site/call',array('type'=>'post','dataType'=>'json','success'=>'
function(res){
    if (res.error==true){
        $("#consult_form_es_ ul").empty();
        $.each(res.msg,function(key,value){
            $("#consult_form_es_ ul").append("<li>"+value+"</li>");    
        });                
        $("#consult_form_es_").show();
    }else if(res.error==false){
        $("#consult").modal("hide");
        alert(res.msg);
    }
}'),array('id'=>'sendreq','class'=>'btn btn-large btn-info span12','data-loading-text'=>'Отправка'))?>
            </div>         
            <?php $this->endWidget(); ?>
        </div>
      </div>           
    </div>
    
    <!--Модальное окно для заказа обратного звонка -->
      <div id="callback" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Заказать звонок</h3>
      </div>
      <div class="modal-body">        
        <?php $callbackModal=new CallbackForm;?>
        <div class="form">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'callback_form',
            'action'=>'#',
            'enableAjaxValidation'=>true,
            'enableClientValidation'=>true,
            'focus'=>array($callbackModal,'firstName'),

        )); ?>
         
            <div id="callback_form_es_" class="errorSummary" style="display:none"><ul></ul></div>
         
            <div class="row-fluid">
                <?php echo $form->label($callbackModal,'name',array('class'=>'span4')); ?>
                <?php echo $form->textField($callbackModal,'name') ?>
            </div>       
        <input name="referer" type="hidden" value="<?=Yii::app()->session['crawler']?>">
            <input name="phrase" type="hidden" value="<?=Yii::app()->session['phrase']?>">
            <div class="row-fluid ">            
                <?php echo $form->label($callbackModal,'time',array('class'=>'span4')); ?>                
                <?php echo $form->textField($callbackModal,'time') ?>                
            </div>              
            <div class="row-fluid">
                <?php echo $form->label($callbackModal,'city',array('class'=>'span4')); ?>
                <?php echo $form->dropDownList($callbackModal,'city',City::All(),array('empty'=>'Выберите город')) ?>
            </div>
            <div class="row-fluid">
                <?php echo $form->label($callbackModal,'number',array('class'=>'span4')); ?>
                <?php echo $form->textField($callbackModal,'number') ?>
            </div>
            <br><br>
            <div class="row-fluid">
<?=CHtml::ajaxSubmitButton('Отправить','/site/call',array('type'=>'post','dataType'=>'json','success'=>'
function(res){
    if (res.error==true){
        $("#callback_form_es_ ul").empty();
        $.each(res.msg,function(key,value){
            $("#callback_form_es_ ul").append("<li>"+value+"</li>");    
        });                
        $("#callback_form_es_").show();
    }else if(res.error==false){
        $("#callback").modal("hide");
        alert(res.msg);
    }
}'),array('id'=>'sendcall','class'=>'btn btn-large btn-info span12','data-loading-text'=>'Отправка'))?>
            </div>         
            <?php $this->endWidget(); ?>
        </div>
      </div>           
    </div>
    <!--Модальное окно для подачи заявки -->
    <div id="request" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Подать заявку</h3>
      </div>
      <div class="modal-body">
        <?php if(Yii::app()->user->isGuest):?>
            <p class="alert alert-error">Извините за неудобство.
            Заявка доступна для авторизированных пользователей. <a href="/login">Авторизироваться ?</a></p>
        <?php else:?>
            <p><?php
            $userModel=User::model()->findByPk(Yii::app()->user->id);
            if (!Calculation::model()->count("user_id = ".Yii::app()->user->id)>0):?>            
                <p class="alert alert-error">Вам необходимо сначала сделать расчет.</p>
            <?php else:?>            
                <a href="/user/cabinet/index#tabs-2">Перейти к расчетам</a>
        <?php endif;?></p>  
        <?php endif;?>
      </div>
    </div>
    <!--Модальное окно для изображений -->
    <div id="imagemodal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>            
          </div>
          <div class="modal-body">
            <img src="#" id="image_src" style="width:auto" />
          </div>          
    </div>

<style>
.bootstrap-timepicker-widget.dropdown-menu.open {
  display: inline-block;
  z-index:1151;
}
</style>