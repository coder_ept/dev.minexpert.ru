<?php $this->pageTitle = $model->name; ?>

<div id="crumbsBlock" style="display: none"><p><a style="color: white;" href="/">Главная </a> > <a style="color: white;" href="/services">Услуги</a> > <?=$model->name?></p></div>
<div class="insidePage" id="insideServList">
	<div id="insideCont" style="position: absolute; text-align: center; margin-top: 80px; display: none" class="wrap">
    <h1 style="z-index: 2;
width: 740px;"><?=$model->name?></h1>
    <table style="z-index: 1;" class="servicesTBlock" cellspacing="4">
        <tbody>
            <tr>
        		<td style="height: 220px;background-color: #fff; border-right: 1px solid #BABABA; border-bottom: 1px solid #BABABA; padding-top: 30px">
        			<table style="margin-bottom: 0">
        				<tbody><tr>
        					<td style="vertical-align: top">
        						<a href="/services?alias=ekspertiza-proektnoj-dokumentacii-i-rezultatov-inzhenernyh-izyskanij"><img src="/uploads/category/<?=$model->image?>" style="height: 187px; max-width: 287px; width: 287px;"></a>
        					</td>
        					<td style="vertical-align: top">           						
        						<p style="margin-top: 20px"><?=$model->short_description?></p>
        					</td>
        				</tr>
        			</tbody></table>
        		</td>               
        	</tr>
        </tbody>
    </table>
</div>
	<img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/inside_bg1.png" class="insideBg">
</div>
<?php $this->renderPartial('//layouts/onlines');?>
<script>
/*function get(name){
   if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
      return decodeURIComponent(name[1]);
}*/
$(document).ready(function(){
    var ww = $(window).width();
		var cw = $('.wrap').width();
		var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css({
				left: (ww - cw)/2,
				display: 'inline-block'
			});
            $('#crumbsBlock').css({
            	left: ((ww - cw)/2+lw-106),
            	display: 'block'
            });
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 650,
				display: 'block'
			})
			$('.wrap').css({
				display: 'inline-block'
			});	
			$('#crumbsBlock p').css('font-size', '14px');		
		};
        var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 100));
/* if (get('alias')==null){
    $('.msb:eq(0)').addClass('mainActiveServBut');
    $('.msb:eq(0)').parent().css('background-color','rgb(247, 247, 247)');
 }else
 {
    $('a.msb[rel="'+get('alias')+'"]').addClass('mainActiveServBut');
    $('a.msb[rel="'+get('alias')+'"]').parent().css('background-color','rgb(247, 247, 247)');
 }
*/    
});
$(window).resize(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
		var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
			$('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 650
			})
			$('#crumbsBlock p').css('font-size', '14px');
		};
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 100));
	})
</script>