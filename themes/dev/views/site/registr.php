<?php $this->pageTitle = "Заключения по экспертизе. ".$this->pageTitle ?>
<div id="crumbsBlock" style="display: none"><p><a style="color: white;" href="/">Главная </a> > Реестр</p></div>
<div class="insidePage">
	<div id="insideCont" style="position: absolute; background-color: #fff; text-align: center; margin-top: 80px; display: none" class="wrap">
		
<?php if(Yii::app()->user->isGuest):?> 
<h3 style="font-size: 30px; font-weight: normal; margin-top: 50px;">Внимание!</h3>
<p style="font-size: 20px; margin-bottom: 50px">Реестр выданных заключений доступен только авторизованным пользователям!</p>
<a href="/login" id="onlineCallLink">Авторизоваться на сайте</a><br><br><br><br><br>
<?php else:?>
<?php
if ($model!=null){
 foreach($model as $key=>$value):?>
	<table class="reestrTab">
		<tr>
			<td colspan="5" style="text-align: center; border: none">
				<div style="display: inline-block; width: 160px; height: 78px; background-image: url('<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/year_bg.jpg');; color: #fff; font-size: 40px; line-height: 80px;"><?=$key ?></div>
			</td>
		</tr>
		<tr>
			<th style="width: 80px">Дата выдачи</td>
			<th style="width: 120px">Номер заключения</td>			
			<th>Объект капитального строительства</td>
			<th>Объект негосударственной экспертизы</td>
            <th>Заказчик</td>
		</tr>
		<?php foreach($value as $reestr):?>
		<tr>
			<td><?=Registr::rdate(date('d.m', strtotime($reestr -> date_out))) ?></td>
			<td><?=$reestr -> number_made ?></td>
			<td style="text-align: left"><?=$reestr -> address ?></td>
			<td><?=$reestr -> obj_negos_exp ?></td>
			<td><?=$reestr -> customer ?></td>
		</tr>
		<?php endforeach; ?>	
	</table>
<?php endforeach; }?>
	
<?php endif;?>	
	
    



		
</div>
<img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/inside_bg1.png" class="insideBg">
</div>
<?php $this->renderPartial('//layouts/onlines');?>
<script type="text/javascript">
	$(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css({
				left: (ww - cw)/2,
				display: 'inline-block'
			});
            $('#crumbsBlock').css({
            	left: ((ww - cw)/2+lw-106),
            	display: 'block'
            });
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300,
				display: 'block'
			})
			$('.wrap').css({
				display: 'inline-block'
			});			
		};
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 200));
		$('.reestrTab').css('width', (cw-80));

	})
	$(window).resize(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
            $('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 200
			})
		};	
		$('.reestrTab').css('width', (cw-80));
	})
</script>