<?php $this->pageTitle = "Документы для экспертизы. ".$this->pageTitle ?>
<div id="crumbsBlock" style="display: none"><p><a style="color: white;" href="/">Главная </a> > Документы</p></div>
<div class="insidePage">
	<div id="insideCont" style="position: absolute; text-align: center; margin-top: 80px; display: none" class="wrap">
		<table class="insideDocsBlock">
			<tr>
				<td width="160" style="vertical-align: top"><img class="docsPic" src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/docs_pic.png"></td>
				<td class="docm"><h3 style="width: 100%;">Внутренняя документация</h3>
				<ul>
					<?php
					foreach ($inside_page as $value) {
						echo '<li><p><a href="/pages/' . $value -> slug . ' ">' . $value -> title . "</a></p></li>";
					}
					?>
				</ul></td>
				<td style="width: 10px; padding: 0; background-color: rgba(0,0,0,0); background-image: url('<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/triangle_white.png');"></td>
			</tr>
			<tr><td colspan="3" style="padding: 0; height: 10px; background-color: rgba(0,0,0,0)"><img style="width: 100%; opacity: .8" src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/shadow_right.png"></td></tr>
		</table>
		
		
		<table class="insideDocsBlock" style="">
			<tr>
				<td width="160" style="vertical-align: top"><img class="docsPic" src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/docs_pic.png"></td>
				<td class="docm"><h3>Документация для заявителей</h3>
				<table style="margin-left: 10px; margin-top: 20px;">
			<?php
			foreach ($doc_apl as $value) {
				echo '<tr>
				<td style="margin-bottom: 20px;">
					<a href="/uploads/documents/' .$value->file . '">
						<img src="'.Yii::app() -> request -> baseUrl.'/themes/default/web/images/base/download_doc.png" style="margin-right: 30px;">
					</a>	
						</td>
						<td><a href="/uploads/documents/' .$value->file . '">' . $value->name . '</a></td></tr>';
			}
			?>
		</table></td>
				<td style="width: 11px; padding: 0; background-color: rgba(0,0,0,0); background-image: url('<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/triangle_white.png');"></td>
			</tr>
			<tr><td colspan="3" style="padding: 0; height: 10px; background-color: rgba(0,0,0,0)"><img style="width: 100%; opacity: .8" src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/shadow_right.png"></td></tr>
		</table>
		
		
		
		
		<table class="insideDocsBlock" style="">
			<tr>
				<td width="160" style="vertical-align: top"><img class="docsPic" src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/docs_pic.png"></td>
				<td class="docm"><h3>Нормативная документация</h3>
				<table style="margin-left: 10px; margin-top: 20px;">
			<?php
			foreach ($norm_doc as $value) {
				echo '<tr>
				<td style="margin-bottom: 20px;">
					<a href="/uploads/documents/' .$value->file . '">
						<img src="'.Yii::app() -> request -> baseUrl.'/themes/default/web/images/base/download_doc.png" style="margin-right: 30px;">
					</a>	
						</td>
						<td><a href="/uploads/documents/' .$value->file . '">' . $value->name . '</a></td></tr>';
			}
			?>
		</table></td>
				<td style="width: 11px; padding: 0; background-color: rgba(0,0,0,0); background-image: url('<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/triangle_white.png');"></td>
			</tr>
			<tr><td colspan="3" style="padding: 0; height: 10px; background-color: rgba(0,0,0,0)"><img style="width: 100%; opacity: .8" src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/shadow_right.png"></td></tr>
		</table>

		
		
		
		
		
		
	</div>
	<img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/inside_bg1.png" class="insideBg">
</div>

<script type="text/javascript">
	$(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css({
				left: (ww - cw)/2,
				display: 'inline-block'
			});
            $('#crumbsBlock').css({
            	left: ((ww - cw)/2+lw-106),
            	display: 'block'
            });
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300,
				display: 'block'
			})
			$('.wrap').css({
				display: 'inline-block'
			});
		};
		$('.docm').css('width', (cw - 170));
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 150));

	})
	$(window).resize(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
            $('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300
			})
		};
		$('.docm').css('width', (cw - 170));
	})
</script>