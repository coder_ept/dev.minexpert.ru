<?php
/**
 * Created by PhpStorm.
 * User: alekseyvb
 * Date: 24.04.14
 * Time: 22:01
 */
?>
<div class="rows-fluids">

<form class="well">


<fieldset>
    <legend>Сведения об организации</legend>

   Полное название организации: <input type="text" name="orgNameFull"><br>
   Сокращенное название организации: <input type="text" name="orgNameShort"><br>
   Телефон организации: <input type="text" name="orgTel"><br>
   Генеральный директор: <input type="text" name="orgGendir"><br>
   Главный бухгалтер: <input type="text" name="orgGlavbuh"><br>
   Контактное лицо от заявителя (менеджер объекта): <input type="text" name="orgManager"><br>
   Телефон контактного лица: <input type="text" name="orgManagerTel"><br>
   e-mail контактного лица (для направления замечаний): <input type="text" name="orgManagerEmail"><br>
   Платежные реквизиты организации:<br> <textarea name="orgRekviz"></textarea><br>
</fieldset>


<fieldset>
        <legend>Информация об объекте</legend>
Полное Наименование объекта капитального строительства:<br> <textarea name="objName"></textarea><br>
Адрес объекта(указывается почтовый (строительный) адрес): <input type="text" name="objAdress"><br>
Вид документации: 
<select name="objDocType">
<option value="Проектная документация со сметой">Проектная документация со сметой</option>
<option value="проектная документация без сметы">проектная документация без сметы</option>
<option value="разделы проектной документации">разделы проектной документации</option>
<option value="Сметная документация">Сметная документация</option>
</select><br>
Вид строительства: 
<select>
<option value="новое">новое</option>
<option value="реконструкция">реконструкция</option>
<option value="капительный ремонт">капительный ремонт</option>
<option value="модернизация">модернизация</option>
</select><br>
Стади проектирования:
<select>
<option value="Проектная документация">Проектная документация</option>
<option value="проектная и рабочая документация">проектная и рабочая документация</option>
</select>
<br>
Источник финансирования: 
<select name="objFinSource">
<option value="Бюджет местный">Бюджет местный</option>
<option value="Бюджет региональный">Бюджет региональный</option>
<option value="Бюджет федеральный">Бюджет федеральный</option>
<option value="Собственные средства заказчика">Собственные средства заказчика</option>
<option value="Кредитные средства">Кредитные средства</option>
</select><br>
(!)Если выбраны кредитные средства, показать это поле: <input type="text" name="objFinSourceCredit">
<br>

Заявленная сметная стоимость: <input type="text" name="objSmetCost"><br> 
в т.ч. СМР: <input type="text" name="objSmetCostSMR"><br> 

</fieldset>



<fieldset>
        <legend>Сведения о заказчике проектных работ (заполняется в случае, если заказчик экспертизы и заказчик проекта разные организации)</legend>
   Полное наименование юридического лица: <input type="text" name="customerNameFull"><br>     
Сокращенное наименование юридического лица: <input type="text" name="customerNameShort"><br> 
Адрес организации: <input type="text" name="customerAdress"><br>  
Телефон организации: <input type="text" name="customerTel"><br>  
Контактное лицо от заявителя (менеджер объекта): <input type="text" name="customerManager"><br>  
телефон контактного лица: <input type="text" name="customerManagerTel"><br>  
e-mail контактного лица: <input type="text" name="customerManagerEmail"><br>  
</fieldset>


 <fieldset>
        <legend>Сведения о заявителе и исполнителях работ по разработке проектной документации и выполнении инженерных изысканий :</legend>

Полное наименование проектной организации(если проектировщик индивидуальный предприниматель – ФИО, №, серия, дата выдачи паспорта): <input type="text" name="projectOrgName"><br>  
Почтовый адрес: <input type="text" name="projectOrgAdress"><br> 
Свидетельство СРО: <input type="text" name="projectOrgSvidSRO_1"> от <input type="date" name="projectOrgSvidSRODate"><br>  
Телефон: <input type="text" name="projectOrgTel"><br> 
Факс: <input type="text" name="projectOrgFax"><br> 
Email: <input type="text" name="projectOrgEmail"><br> 
ФИО ГИПа: <input type="text" name="projectOrgGip"><br>  
Телефон ГИПа: <input type="text" name="projectOrgGipTel"><br>  



Полное наименование изыскательской организации: <input type="text" name="surveyOrgName"><br>  
Почтовый адрес: <input type="text" name="surveyOrgAdress"><br> 
Свидетельство СРО: <input type="text" name="surveyOrgSvidSRO_1"> от <input type="date" name="surveyOrgSvidSRODate"><br>  
Телефон: <input type="text" name="surveyOrgTel"><br> 
Факс: <input type="text" name="surveyOrgFax"><br> 



Полное наименование организации заказчика-застройщика: <input type="text" name="builderOrgName"><br>  
Почтовый адрес: <input type="text" name="builderOrgAdress"><br> 
Свидетельство СРО: <input type="text" name="builderOrgSvidSRO_1"> от <input type="date" name="builderOrgSvidSRODate"><br>  
Телефон: <input type="text" name="builderOrgTel"><br> 
Факс: <input type="text" name="builderOrgFax"><br> 
Email: <input type="text" name="builderOrgEmail"><br> 
Контактное лицо: <input type="text" name="builderManager"><br>  
телефон контактного лица: <input type="text" name="builderManagerTel"><br>  





Полное наименование заявителя (заполняется в случае, если заявитель не является застройщиком или техническим заказчиком): <input type="text" name="applicantOrgName"><br>  
Почтовый адрес: <input type="text" name="applicantOrgAdress"><br> 
Свидетельство СРО: <input type="text" name="applicantOrgSvidSRO_1"> от <input type="date" name="applicantOrgSvidSRODate"><br>  
Телефон: <input type="text" name="applicantOrgTel"><br> 
Факс: <input type="text" name="applicantOrgFax"><br> 


Руководитель ФИО: <input type="text" name="performerLeaderFIO"><br>  
должность: <input type="text" name="performerLeaderPost"><br> 
    </fieldset>







 <fieldset>
        <legend>Технико-экономические характеристики объекта капитального строительства с учетом его вида, функционального назначения и характерных особенностей</legend>

А вот тут не совсем понятно что вставлять из калькулятора, а что вписывать....<br><br>

Площадь земельного участка<br>
Площадь застройки<br>
Коэффициент застройки <br>
Коэффициент плотности застройки<br> 
Площадь здания<br>
в т.ч. общая площадь квартир (для жилых зданий)<br>
в т.ч. расчетная площадь (торговая, административных помещений и т.д. – для зданий общественного назначения)<br>
Количество квартир (для жилых зданий)<br>
в т.ч. однокомнатных<br>
двухкомнатных<br>
…
Количество этажей <br>
Этажность здания<br>
Строительный объем<br>
в т.ч. ниже отметки 0,000 (подземная часть)<br>
Площадь встроенных общественных помещений (для жилых зданий) 

    </fieldset>

</form>
</div>
<?php if(isset($data)):?>

<?php endif;?>