<?php
/* @var $this SiteController */

?>

<script type="text/javascript">
    $(document).ready(function(){
        $('#sm_slider').smSlider({
            autoPlay : true,
            delay : 6000,
            hoverPause : false,
            easing : 'swing',
            autoArr : false
        })
        var ww = $(window).width();
        if(ww<801){
        	$(".carousel-news").sliderkit({
            shownavitems:3,
            scroll:1,
            mousewheel:false,
            circular:true,
            start:2
        });
        }else{
        	$(".carousel-news").sliderkit({
            shownavitems:4,
            scroll:1,
            mousewheel:false,
            circular:true,
            start:2
        });
        };
    })
</script>


<div id="mainBanner">
    <div id="sm_slider" class="wrap">
        <ul>
            <li><a href="/online">
            	<img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/slider/slide_calc.png" class="mainSliderPic" id="sliderPic_1">
            	<div class="mainSliderTextCont" id="sliderText_1">
            		<div id="s1p1" class="mainSliderText" style="width: 370px;margin-left: 45px;"><p>Рассчитайте стоимость</p></div>
            		<div id="s1p2" class="mainSliderText"><p>проведения экспертизы</p></div>
            		<div id="s1p3" class="mainSliderText" style="width: 120px;"><p>онлайн!</p></div>
            		<div class="mainSliderLink">Рассчитать<img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/slider_link.png" style="margin-left: 5px;"></div>
            	</div>
            </a></li>
            <li><a href="/services">
            	<img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/slider/slide_minex.png" class="mainSliderPic" id="sliderPic_2">
            	<div class="mainSliderTextCont" id="sliderText_2">
            		<div id="s2p1" class="mainSliderText" style="width: 500px;margin-left: 44px;"><p>Проведение негосударственной</p></div>
            		<div id="s2p2" class="mainSliderText"><p>экспертизы проектной документации</p></div>
            		<div id="s2p3" class="mainSliderText" style="width: 400px;"><p>и инженерных изысканий</p></div>
            		<div class="mainSliderLink">Подробнее<img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/slider_link.png" style="margin-left: 5px;"></div>
            	</div>
            	
            </a></li>
            <li><a href="#callback" role="button" data-toggle="modal" >
            	<img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/slider/slide_phone.png" class="mainSliderPic" id="sliderPic_3">
            	<div class="mainSliderTextCont" id="sliderText_3">
            		<div id="s3p1" class="mainSliderText" style="width: 400px; margin-left: 44px;"><p>Позвоните прямо сейчас</p></div>
            		<div id="s3p2" class="mainSliderText" style="width: 300px;"><p>по горячей линии</p></div>
            		<div id="s3p3" class="mainSliderText" style="width: 300px;"><p>8(800)707-81-57</p></div>
            		<div class="mainSliderDesc">Вас приятно удивят сроки <br>проведения экспертизы у нас!</div>
            		<div class="mainSliderLink">
Заказать звонок<img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/slider_link.png" style="margin-left: 5px;"></div>
            	</div>
            	
            </a></li>
            <li><a href="#consult" role="button" data-toggle="modal">
            	<img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/slider/slide_call.png" class="mainSliderPic" id="sliderPic_4">
            	<div class="mainSliderTextCont" id="sliderText_4">
            		<div id="s4p1" class="mainSliderText" style="width: 250px; margin-left: 44px;"><p>Задайте вопрос</p></div>
            		<div id="s4p2" class="mainSliderText" style="width: 200px;"><p>специалисту</p></div>
            		<div id="s4p3" class="mainSliderDesc">Наши специалисты квалифицированно<br>ответят на все ваши вопросы</div>
            		<div class="mainSliderLink">
Задать вопрос<img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/slider_link.png" style="margin-left: 5px;"></div>
            	</div>
            	
            </a></li>
            <li><a href="/kontakty">
            	<img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/slider/slide_map.png" class="mainSliderPic" id="sliderPic_5">
            	<div class="mainSliderTextCont" id="sliderText_5">
            		<div id="s5p1" class="mainSliderText" style="width: 360px; margin-left: 20px;"><p>Работайте где угодно!</p></div>
            		<div id="s5p2" class="mainSliderDesc">У нас есть филиалы в городах<br>по всей России!</div>
            		<div class="mainSliderLink">Наши филиалы<img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/slider_link.png" style="margin-left: 5px;"></div>
            	</div>
            	
            </a></li>
        </ul>
    </div>
</div>

<?php $this->renderPartial('//layouts/onlines');?>

<div id="mainServices">
	<div id="mainServicesCont" class="wrap">
		<span class="mainBlockLabel">Услуги института</span>
		<?php $this->widget('application.modules.category.widgets.ServicesWidget')?>
	</div>
</div>
<table id="mainNewsTriangle" style="">
		<tr>
			<td style="background-color: #fff"></td>
			<td style="padding: 0;width: 366px"><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/news_triangle1.png"></td>
			<td style="background-color: #fff"></td>
		</tr>
	</table>
    
<div id="mainNews">
	<span class="mainBlockLabel" style="color: #fff; margin-top: 50px;">Новости</span><br>
	<div class="sliderkit carousel-news wrap">
        <div class="sliderkit-nav">
            <div class="sliderkit-nav-clip">
                <?php $this->widget('application.modules.news.widgets.LastNewsWidget') ?>
            </div>
            <div class="sliderkit-btn sliderkit-nav-btn sliderkit-nav-prev"><a href="#" title="Scroll to the left"><span>Previous</span></a></div>
            <div class="sliderkit-btn sliderkit-nav-btn sliderkit-nav-next"><a href="#" title="Scroll to the right"><span>Next</span></a></div>
        </div>
    </div>
</div>
<div id="mainAbout">
	<div id="mainAboutCont" class="wrap">

				<div id="mainAboutText">
					<h3>О компании</h3>
					<p>ООО «Межрегиональный институт экспертизы» - это коллектив профессионалов
						 строительной отрасли, основанный на кадрах, базе и опыте научно-исследовательского 
						 проектно-изыскательского института с историей работы более чем 85 лет. Наша компания 
						 аккредитована в области проведения негосударственной экспертизы проектной документации 
						 и результатов инженерных изысканий, свидетельства РОСС RU.0001.610206 и РОСС RU.0001.610160.
					</p>
					<p>
						В штате нашей компании аттестованные эксперты на право подготовки заключений экспертизы проектной
						 документации, экспертизы инженерных изысканий и экспертизы сметной документации.
					</p>
					<p>
						В соответствии с ФЗ №337 от 28 ноября 2011 года при получении разрешения на строительство заключения
						 ООО «МИНЭКС» имеют равную юридическую силу на всей территории Российской Федерации с заключениями 
						 государственной экспертизы.
					</p>
				</div>
				<div id="mainAboutList">
					<h3 style="text-align: left;">Работая с нами Вы получаете:</h3>
					<table id="mainAboutPics">
						<tr>
							<td><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/about_good.png" class="mainAboutPic"></td>
							<td>Надежного и опытного партнера в лице нашей компании;</td>
						</tr>
						<tr>
							<td><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/about_consult.png" class="mainAboutPic"></td>
							<td>консультации грамотных специалистов, работающих в области экспертизы и проектирования;</td>
						</tr>
						<tr>
							<td><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/about_care.png" class="mainAboutPic"></td>
							<td>индивидуальный подход и алгоритм оказания услуг;</td>
						</tr>
						<tr>
							<td><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/about_graph.png" class="mainAboutPic"></td>
							<td>профессиональный аудит всех разделов проектной документации, основанный на нормативах;</td>
						</tr>
						<tr>
							<td><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/about_manager.png" class="mainAboutPic"></td>
							<td>личного менеджера, сопровождающего Ваш проект;</td>
						</tr>
						<tr>
							<td><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/about_time.png" class="mainAboutPic"></td>
							<td>минимально возможные сроки проведения негосударственной экспертизы проектной документации (от 30 до 7 дней).</td>
						</tr>
						<tr>
							<td><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/web/images/base/about_special.png" class="mainAboutPic"></td>
							<td>особые условия сотрудничества для постоянных и корпоративных клиентов.</td>
						</tr>
					</table>
					
				</div>
	</div>
</div>




    









