
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
    array(
        'id' => 'form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'type' => 'pils',
        'htmlOptions' =>
            array(
                'class' => '',
                'style' => 'margin: 0!important'),
        'inlineErrors' => true,
    )
);?>

<form><input type="hidden" name="org" id="org-id" value=""></form>

<button type="button" class="btn btn-link" data-toggle="modal" data-target=".bs-example-modal-lg2">Редактировать</button>
<div class="modal hide fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <table>
                <tr>
                    <td>Организационно-правовая форма:</td>
                    <td>
                        <?= $form->dropDownList($model, 'full_type_project_organization', array(
                            "Общество с ограниченной ответственностью" => "Общество с ограниченной ответственностью",
                            "Открытое акционерное общество" => "Открытое акционерное общество",
                            "Закрытое акционерное общество" => "Закрытое акционерное общество",
                            "Индивидуальный предприниматель" => "Индивидуальный предприниматель"
                        )); ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-danger">Сохранить</button>
    </div>
</div>
<!-- получаем id организации-->
<!--<script>-->
<!--    $('#Org_title').change(function(){-->
<!--        var v = $('#Org_title :selected').val();-->
<!--        $("#org-id").val(v);-->
<!--    });-->
<!--</script>-->

<?php $this->endWidget(); ?>

