
<?php
/**
 * Отображение для page/page:
 * 
 *   @category YupeView
 *   @package  YupeCMS
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->pageTitle   = $page->title;
$this->breadcrumbs = $this->breadcrumbs;
$this->description = !empty($page->description) ? $page->description : $this->description;
$this->keywords    = !empty($page->keywords)    ? $page->keywords    : $this->keywords;
?>
<style>

<?php
if($page->title == 'О компании'){
	echo "
	#aboutMenu {margin-top: -90px!important;}
	#insideCont {margin-top: 140px!important;}
	#aboutMI1 {background-color: #474747!important;}
	#aboutMI1 a {color: #fff!important; border-bottom: none}
	 ";
} elseif ($page->title == 'Официально') {
	echo "
	#aboutMenu {margin-top: -90px!important;}
	#insideCont {margin-top: 140px!important;}
	#aboutMI2 {background-color: #474747!important;}
	#aboutMI2 a {color: #fff!important; border-bottom: none}
	 ";
} elseif ($page->title == 'Эксперты') {
	echo "
	#aboutMenu {margin-top: -90px!important;}
	#insideCont {margin-top: 140px!important;}
	#aboutMI3 {background-color: #474747!important;}
	#aboutMI3 a {color: #fff!important; border-bottom: none}
	 ";
} elseif ($page->title == 'Контакты') {
	echo "
	#aboutMenu {margin-top: -90px!important;}
	#insideCont {margin-top: 140px!important;}
	#aboutMI4 {background-color: #474747!important;}
	#aboutMI4 a {color: #fff!important; border-bottom: none}
	 ";
}
?>
</style>






<div id="crumbsBlock" style=" display: none"><p><a style="color: white;" href="/">Главная </a> ><?php if($page->category_id==2):?> <a style="color: white;" href="/documents">Документы </a> ><?php endif;?> <?=$page->title;?></p></div>

<div class="insidePage" style="background-color: #d9d9d9">
	<div id="insideCont" style="position: absolute; background-color: #fff; text-align: center; margin-top: 80px; text-align: left; display: none" class="wrap">

<!--h1><?php echo $page->title; ?></h1-->

<?php echo $page->body; ?>



</div>
<img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/inside_bg1.png" class="insideBg">
</div>

<script type="text/javascript">
	$(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
		var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css({
				left: (ww - cw)/2,
				display: 'inline-block'
			});
            $('#crumbsBlock').css({
            	left: ((ww - cw)/2+lw-106),
            	display: 'block'
            });
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300,
				display: 'block'
			})
			$('.wrap').css({
				display: 'inline-block'
			});			
		};
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 200));
		
	})
	$(window).resize(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
		var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
			$('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300
			})
		};
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 200));
		
	})
</script>