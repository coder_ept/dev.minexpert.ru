 <!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>Сведения об объекте</title>
		<meta name="description" content="">
		<meta name="author" content="AlekseyVB">

		<meta name="viewport" content="width=device-width; initial-scale=1.0">

		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<link rel="shortcut icon" href="/favicon.ico">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">
        <style>
        body {
            text-align: center;
        }
        #tab, #tab td, #tab th {
            border: 1px solid #555;
            border-collapse: collapse;
            padding: 5px 15px;
        }
        </style>
	</head>

	<body>
		<div style="width: 100%; display: inline-block; text-align: left">




<div style="clear:both; text-align: center">
    <strong>СВЕДЕНИЯ<br>
    об объекте капитального строительства, заявителе и исполнителях работ по разработке проектной<br>
    документации и выполнении инженерных изысканий
    </strong>
</div>

<p>Полное наименование объекта капитального строительства, его местонахождение:<br>
	<?=$model->name_object_capital_construction. ', '.$model->address_object?>
</p>


<p>Полное наименование проектной организации:<br>
	<?=$model->full_type_project_organization.' '.$model->full_name_project_organization?>
</p>
	<p>Почтовый адрес проектной организации:<br>
		<?=$model->mail_address_project_organization?><br>
	Допуск СРО <?=$model->statement_sro_project_organization?> от <?=date('d.m.Y',strtotime($model->statement_sro_start_project_organization))?><br>
	Тел.<?=$model->phone_project_organization?>, Факс <?=$model->fax_project_organization?> <br>
	E-mail: <?=$model->email_project_organization?>  http <?=$model->site?><br>
	ГИП <?=$model->fio_gipa?>, тел.<?=$model->phone_gipa?> <br>
	</p>
<br>



<p>Полное наименование изыскательской организации:<br>
	Геология
</p>
<p>Почтовый адрес изыскательской организации:<br>
	<?=$model->mail_address_survey_organization?><br>
Допуск СРО <?=$model->statement_sro_survey_organization?> от <?=date('d.m.Y',strtotime($model->statement_sro_start_survey_organization))?><br>
Тел.<?=$model->phone_survey_organization?>, Факс <?=$model->fax_survey_organization?> <br>
</p><br>


<p>Полное наименование изыскательской организации:<br>
	Геодезия
</p>
<p>Почтовый адрес изыскательской организации:<br>
	<?=$model->mail_address_survey2_organization?><br>
Допуск СРО <?=$model->statement_sro_survey2_organization?> от <?=date('d.m.Y',strtotime($model->statement_sro_start_survey2_organization))?><br>
Тел.<?=$model->phone_survey2_organization?>, Факс <?=$model->fax_survey2_organization?> <br>
</p><br>

<p>Полное наименование организации заказчика-застройщика:<br>
	
<?php if($model->applicant):?>
<?=$model->applicant?>	
<p>Почтовый адрес организации:<br>
	<?=$model->email_address_applicant?><br>
Допуск СРО <?=$model->statement_sro_applicant?> от <?=date('d.m.Y',strtotime($model->statement_sro_start_applicant))?><br>
Тел.<?=$model->phone_applicant?>, Факс <?=$model->fax_applicant?> <br>
E-mail: <?=$model->email_customer_buider?>  http <?=$model->site2?><br>
Контактное лицо <?=$model->contact_person_customer_buider?>, тел.<?=$model->phone_contact_person_customer_buider?> <br>
</p><br>

<?php else:?>	
<?=$model->organization_customer_buider?>
</p>
<p>Почтовый адрес организации:<br>
	<?=$model->mail_address_customer_buider?><br>
Допуск СРО <?=$model->statement_sro_customer_buider?> от <?=date('d.m.Y',strtotime($model->statement_sro_start_customer_buider))?><br>
Тел.<?=$model->phone_customer_buider?>, Факс <?=$model->fax_customer_buider?> <br>
E-mail: <?=$model->email_customer_buider?>  http <?=$model->site2?><br>
Контактное лицо <?=$model->contact_person_customer_buider?>, тел.<?=$model->phone_contact_person_customer_buider?> <br>
</p><br>
<?php endif;?>
<br>
<?php if(!empty($model->applicant)):?>
<p>Полное наименование заявителя (заполняется в случае, если заявитель не является застройщиком 
или техническим заказчиком):<br>
	<?=$model->applicant?>
</p>
<p>Почтовый адрес организации:<br>
	<?=$model->email_address_applicant?><br>
Допуск СРО <?=$model->statement_sro_applicant?> от <?=date('d.m.Y',strtotime($model->statement_sro_start_applicant))?><br>
Тел.<?=$model->phone_applicant?>, Факс <?=$model->fax_applicant?> <br>
</p>
<?php endif;?>
<p>Источник финансирования: <?=$model->getSourceFinance()?></p>

<p>Заявленная сметная стоимость: <?=$model->claimed_estimated_cost?><br>
в т.ч. СМР: <?=$model->v_t_4_SMR?>	
</p>

<p style="text-align: center"><strong>Технико-экономические характеристики объекта капитального строительства с учетом его вида, 
	функционального назначения и характерных особенностей:</strong></p>
<table id="tab">
	<tr>
		<th>Наименование</th>
		<th>Ед.изм.</th>
		<th>Величина</th>
	</tr>
	<tr>
		<td>Площадь земельного участка</td>
		<td>м2</td>
		<td><?=$model->land_area?></td>
	</tr>
	<tr>
		<td>Площадь застройки</td>
		<td>м2</td>
		<td><?=$model->built_up_area?></td>
	</tr>
	<tr>
		<td>Коэффициент застройки</td>
		<td>д. ед</td>
		<td><?=$model->building_factor?></td>
	</tr>
	<tr>
		<td>Коэффициент плотности застройки</td>
		<td>д. ед</td>
		<td><?=$model->building_density_factor?></td>
	</tr>
	<tr>
		<td>Площадь здания<br>
в т.ч. общая площадь квартир (для жилых <br>зданий)
в т.ч. расчетная площадь (торговая, <br>
административных помещений и т.д. – для <br>
зданий общественного назначения)</td>
		<td>м2</td>
		<td><?=$model->building_area?></td>
	</tr>
	<tr>
		<td>Количество квартир (для жилых зданий)<br>
в т.ч. однокомнатных<br>
двухкомнатных</td>
		<td>шт</td>
		<td><?=$model->count_apartament?></td>
	</tr>
	<tr>
		<td>Количество этажей</td>
		<td>этаж</td>
		<td><?=$model->count_floor?></td>
	</tr>
	<tr>
		<td>Этажность здания</td>
		<td>этаж</td>
		<td><?=$model->floors?></td>
	</tr>
	<tr>
		<td>Строительный объем
в т.ч. ниже отметки 0,000 (подземная часть)</td>
		<td>м3</td>
		<td><?=$model->building_volume?></td>
	</tr>
	<tr>
		<td>Площадь встроенных общественных помещений<br>
(для жилых зданий)</td>
		<td>м2</td>
		<td><?=$model->area_built_public_buildings?></td>
	</tr>
</table>
<br>
<br>
<table>
	<tr>
		<td><?=$model->a1_chiefpostt?></td>
		<td>________________</td>
		<td>&nbsp;</td>
		<td><?=$model->ceo_company?></td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td width="100">&nbsp;</td>
		<td>Ф.И.О.</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td style="text-align: center">М.П.</td>
		<td>&nbsp;</td>
	</tr>
</table>
 			
		</div>
	</body>
</html>
