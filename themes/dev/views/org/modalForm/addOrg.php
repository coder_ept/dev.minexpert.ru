<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 20.12.14
 * Time: 16:12
 */ ?>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
    array(
        'id' => 'ORG',
        'action'=>$this->createAbsoluteUrl('/org/index'),
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'type' => 'inline',
        'clientOptions'=>
                array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => false,
                ),
        'htmlOptions' =>
            array(
            ),
        'inlineErrors' => true,
    )
);?>

<?php echo $this->createAbsoluteUrl('/org/index');?>

<style>
    .modal{left:40%}
    .modal-body{max-height: 600px;}
</style>


    <button type="button" class="btn btn-link" data-toggle="modal" data-target=".bs-example-modal-lg">Добавить</button>

    <div class="modal fade hide bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Добавление новой организации</h4>
            </div>

            <div class="modal-body">
                <table>

                    <!-- Организационно-правовая форма-->
                        <tr>
                            <td>Организационно-правовая форма:</td>
                            <td>

                                <?php ?>
                                <?php echo $form->dropDownList($model, 'full_type_project_organization', array(
                                    "Общество с ограниченной ответственностью" => "Общество с ограниченной ответственностью",
                                    "Открытое акционерное общество" => "Открытое акционерное общество",
                                    "Закрытое акционерное общество" => "Закрытое акционерное общество",
                                    "Индивидуальный предприниматель" => "Индивидуальный предприниматель"
                                )); ?>
                            </td>
                        </tr>

                    <!-- Название юридического лица-->
                        <tr>
                            <td>Название юридического лица:</td>
                            <td><?php echo $form->textField($model, 'a1_name_short'); ?></td>
                        </tr>
                    <!-- Адрес организации (юридический)-->
                        <tr>
                            <td>Адрес организации (юридический):</td>
                            <td>
                                <table class="adress_table">
                                    <tr>
                                        <td>
                                            <input name="a1_index_jur" id="a1_index_jur" type="text" class="index_field" placeholder="Индекс">
                                        </td>
                                        <td><select name="a1_region_jur" id="a1_region_jur">
                                                <option>Регион</option>
                                                <option>Регион 1</option>
                                                <option>Регион 2</option>
                                            </select></td>
                                        <td><select name="a1_city_jur" id="a1_city_jur">
                                                <option>Город</option>
                                                <option>Москва</option>
                                                <option>Санкт-Питербург</option>
                                            </select></td>
                                        <td><select name="a1_street_jur" id="a1_street_jur">
                                                <option>Улица</option>
                                                <option>ул. Ленина</option>
                                            </select></td>
                                        <td><select name="a1_house_jur" id="a1_house_jur">
                                                <option>Дом</option>
                                                <option>1</option>
                                                <option>2</option>
                                            </select></td>
                                        <td><select name="a1_corpus_jur" id="a1_corpus_jur">
                                                <option>Корпус</option>
                                                <option>а</option>
                                                <option>б</option>
                                            </select></td>
                                        <td><select name="a1_apt_jur" id="a1_apt_jur">
                                                <option>Квартира</option>
                                                <option>1</option>
                                                <option>2</option>
                                            </select></td>
                                    </tr>
                                </table>
                            </td>
                            <td class="nodisplay"><?php echo $form->textField($model, 'a1_address'); ?></td>
                        </tr>
                    <!-- Адрес организации (почтовый)-->
                        <tr>
                            <td>Адрес организации (почтовый):</td>
                            <td><input type="checkbox" name="a1_post" value="1" id="a1_post" onchange="a1post()">Почтовый адрес совпадает с
                                юридическим
                                <table class="a1_post adress_table">
                                    <tr>
                                        <td>
                                            <input name="a1_index" id="a1_index_post" type="text" class="index_field" placeholder="Индекс">
                                        </td>
                                        <td><select name="a1_region_post" id="a1_region_post">
                                                <option>Регион</option>
                                                <option>Регион 1</option>
                                                <option>Регион 2</option>
                                            </select></td>
                                        <td><select name="a1_city_post" id="a1_city_post">
                                                <option>Город</option>
                                                <option>Москва</option>
                                                <option>Санкт-Питербург</option>
                                            </select></td>
                                        <td><select name="a1_street_post" id="a1_street_post">
                                                <option>Улица</option>
                                                <option>ул. Ленина</option>
                                            </select></td>
                                        <td><select name="a1_house_post" id="a1_house_post">
                                                <option>Дом</option>
                                                <option>1</option>
                                                <option>2</option>
                                            </select></td>
                                        <td><select name="a1_corpus_post" id="a1_corpus_post">
                                                <option>Корпус</option>
                                                <option>а</option>
                                                <option>б</option>
                                            </select></td>
                                        <td><select name="a1_apt_post" id="a1_apt_post">
                                                <option>Квартира</option>
                                                <option>1</option>
                                                <option>2</option>
                                            </select></td>
                                    </tr>
                                </table>
                            </td>
                            <td class="nodisplay"><?php echo $form->textField($model, 'a1_address_post'); ?></td>
                        </tr>
                    <!-- телефон организации-->
                        <tr>
                            <td>Телефон организации:</td>
                            <td><?php echo $form->textField($model, 'a1_phone'); ?></td>
                        </tr>
                    <!-- платежные реквезиты орг.-->
                        <tr>
                            <th colspan="2"> Платежные реквизиты организации</th>
                        </tr>
                        <tr>
                            <td>ИНН:</td>
                            <td><?php echo $form->textField($model, 'plat_inn', array('maxlength' => '12')); ?></td>
                        </tr>
                        <tr id="kpp" >
                            <td>КПП:</td>
                            <td><?php echo $form->textField($model, 'plat_kpp', array('maxlength' => '9')); ?></td>
                        </tr>
                        <tr >
                            <td>ОГРН:</td>
                            <td><?php echo $form->textField($model, 'plat_ogrn', array('maxlength' => '13')); ?></td>
                        </tr>
                        <tr>
                            <td>Р/С:</td>
                            <td><?php echo $form->textField($model, 'plat_rs', array('maxlength' => '20')); ?></td>
                        </tr>
                        <tr>
                            <td>Банк:</td>
                            <td><?php echo $form->textField($model, 'plat_bank'); ?></td>
                        </tr>
                        <tr >
                            <td>К/С:</td>
                            <td><?php echo $form->textField($model, 'plat_ks', array('maxlength' => '20')); ?></td>
                        </tr>
                        <tr >
                            <td>БИК:</td>
                            <td><?php echo $form->textField($model, 'plat_bik', array('maxlength' => '9')); ?></td>
                        </tr>

                    <!-- Руководитель -->
                        <tr class="zay">
                            <td colspan="2">Руководитель:</td>
                        </tr>
                        <tr class="zay">
                            <td>Фамилия:</td>
                            <td><input type="text" name="ceo_surname" id="ceo_surname"></td>
                            <td class="nodisplay"><?= $form->textField($model, 'ceo_company'); ?></td>
                        </tr>
                        <tr class="zay">
                            <td>Имя:</td>
                            <td><input type="text" name="ceo_name" id="ceo_name"></td>
                        </tr>
                        <tr class="zay">
                            <td>Отчество:</td>
                            <td><input type="text" name="ceo_patronimic" id="ceo_patronimic"></td>
                        </tr>
                        <tr class="zay">
                            <td>Должность руководителя:</td>
                            <td><?= $form->textField($model, 'a1_chiefpostt'); ?></td>
                        </tr>
                        <tr class="zay">
                            <td colspan="2">Главный бухгалтер:</td>
                        </tr>
                        <tr class="zay">
                            <td>Фамилия:</td>
                            <td><input type="text" name="buhgalter_surname" id="buhgalter_surname"></td>
                            <td class="nodisplay"><?= $form->textField($model, 'chief_buhgalter'); ?></td>
                        </tr>
                        <tr class="zay">
                            <td>Имя:</td>
                            <td><input type="text" name="buhgalter_name" id="buhgalter_name"></td>
                        </tr>
                        <tr class="zay">
                            <td>Отчество:</td>
                            <td><input type="text" name="buhgalter_patronimic" id="buhgalter_patronimic"></td>
                        </tr>

                </table>

            </div>
        </div>

        <div class="modal-footer">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>'Сохранить',
            ));
            ?>
            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>

        </div>
    </div>
<?php $this->endWidget(); ?>


<?php //$callbackModal=new CallbackForm;?>
    <!--            <div class="form">-->
    <!--                --><?php //$form=$this->beginWidget('CActiveForm', array(
//                    'id'=>'callback_form',
//                    'action'=>'#',
//                    'enableAjaxValidation'=>true,
//                    'enableClientValidation'=>true,
//                    'focus'=>array($callbackModal,'firstName'),
//
//                )); ?>
    <!---->
    <!--                <div id="callback_form_es_" class="errorSummary" style="display:none"><ul></ul></div>-->
    <!---->
    <!--                <div class="row-fluid">-->
    <!--                    --><?php //echo $form->label($callbackModal,'name',array('class'=>'span4')); ?>
    <!--                    --><?php //echo $form->textField($callbackModal,'name') ?>
    <!--                </div>-->
    <!--                <input name="referer" type="hidden" value="--><?//=Yii::app()->session['crawler']?><!--">-->
    <!--                <input name="phrase" type="hidden" value="--><?//=Yii::app()->session['phrase']?><!--">-->
    <!--                <div class="row-fluid ">-->
    <!--                    --><?php //echo $form->label($callbackModal,'time',array('class'=>'span4')); ?>
    <!--                    --><?php //echo $form->textField($callbackModal,'time') ?>
    <!--                </div>-->
    <!--                <div class="row-fluid">-->
    <!--                    --><?php //echo $form->label($callbackModal,'city',array('class'=>'span4')); ?>
    <!--                    --><?php //echo $form->dropDownList($callbackModal,'city',City::All(),array('empty'=>'Выберите город')) ?>
    <!--                </div>-->
    <!--                <div class="row-fluid">-->
    <!--                    --><?php //echo $form->label($callbackModal,'number',array('class'=>'span4')); ?>
    <!--                    --><?php //echo $form->textField($callbackModal,'number') ?>
    <!--                </div>-->
    <!--                <br><br>-->
    <!--                <div class="row-fluid">-->
    <!--                    --><?//=CHtml::ajaxSubmitButton('Отправить','/site/call',array('type'=>'post','dataType'=>'json','success'=>'
//function(res){
//    if (res.error==true){
//        $("#callback_form_es_ ul").empty();
//        $.each(res.msg,function(key,value){
//            $("#callback_form_es_ ul").append("<li>"+value+"</li>");
//        });
//        $("#callback_form_es_").show();
//    }else if(res.error==false){
//        $("#callback").modal("hide");
//        alert(res.msg);
//    }
//}'),array('id'=>'sendcall','class'=>'btn btn-large btn-info span12','data-loading-text'=>'Отправка'))?>
    <!--                </div>-->
    <!--                --><?php //$this->endWidget(); ?>
    <!--            </div>-->

