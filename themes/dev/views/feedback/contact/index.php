<?php
$this->pageTitle = 'Обратная связь';
?>
<div id="crumbsBlock"><p><a style="color: white;" href="/">Главная </a> > Обратная связь</p></div>
<div class="insidePage" id="insideFaq">
	<div id="insideCont" style="position: absolute; background-color: #fff; text-align: center; margin-top: 80px;" class="wrap">




<h1 style="font-weight: normal; margin-bottom: 50px; margin-top: 30px;">Обратная связь</h1>

<?php $this->widget('application.modules.yupe.widgets.YFlashMessages'); ?>



<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'feedback-form',
        'enableClientValidation' => true
    )); ?>

    <p class="note">Поля, отмеченные <span class="required">*</span> обязательны для заполнения</p>

    <?php echo $form->errorSummary($model); ?>

    <?php if ($model->type): ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'type'); ?>
            <?php echo $form->dropDownList($model, 'type', $module->types); ?>
            <?php echo $form->error($model, 'type'); ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name', array('style'=>'width: 500px')); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email', array('style'=>'width: 500px')); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'theme'); ?>
        <?php echo $form->textField($model, 'theme', array('style'=>'width: 500px')); ?>
        <?php echo $form->error($model, 'theme'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'text'); ?>
        <?php echo $form->textArea($model, 'text', array('style'=>'width: 500px; height: 100px;')); ?>
        <?php echo $form->error($model, 'text'); ?>
    </div>

    <?php if ($module->showCaptcha && !Yii::app()->user->isAuthenticated()): ?>
        <?php if (CCaptcha::checkRequirements()): ?>
            <div class="row">
                <?php echo $form->labelEx($model, 'verifyCode'); ?>

                <?php $this->widget('CCaptcha', array(
                    'showRefreshButton' => true,
                    'clickableImage' => true,
                    'buttonLabel' => 'обновить',
                    'buttonOptions' => array('class' => 'captcha-refresh-link')
                )); ?>

                <?php echo $form->textField($model, 'verifyCode'); ?>
                <?php echo $form->error($model, 'verifyCode'); ?>
                <div class="hint">
                    Введите цифры указанные на картинке
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>


    <div class="row submit">
        <?php echo CHtml::submitButton('Отправить сообщение',array('style'=>'height: 40px')); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>


	</div>
	<img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/inside_bg1.png" class="insideBg">
</div> 


<script type="text/javascript">
	$(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
            $('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300
			})
		};
		$('.docm').css('width', (cw - 170));
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 150));

	})
	$(window).resize(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
            $('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300
			})
		};
		$('.docm').css('width', (cw - 170));
	})
</script>