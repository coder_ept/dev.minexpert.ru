<?php $this->pageTitle = Yii::t('feedback', 'Вопросы и ответы'); ?>



<div id="crumbsBlock" style="display: none"><p><a style="color: white;" href="/">Главная </a> > Вопросы и ответы</p></div>
<div class="insidePage" id="insideFaq">
	<div id="insideCont" style="position: absolute; text-align: center; margin-top: 80px; diaplay: none" class="wrap">


<?php foreach($questing as $fq):?>
<div class="faq">
		<table class="question insideFAQBlock" id="q_<?=$fq->id?>">
			<tr>
				<td width="160" style="vertical-align: top; background-color: #fff; border-left: 1px solid rgba(0,0,0,0.1); cursor: pointer"><img src="/themes/default/web/images/base/faqq.jpg" style="padding: 40px; height: 119px; width: 112px; max-width: 112px; "/></td>
				<td style="background-color: #fff; font-size: 18px; cursor: pointer; padding-right: 50px"><?=$fq->text?>
			<span style="color: #034B95; font-size: 22px; border-bottom: 1px dotted #034B95; display: inline-block; margin-top: 20px;">Ответ</span><img src="/themes/default/web/images/base/triangle_blue.png" style="padding: 0 0 1px 4px"/>
			</td>
			<td style="width: 10px; padding: 0; background-color: rgba(0,0,0,0); background-image: url('/themes/default/web/images/base/triangle_white.png');"></td>
			</tr>
			<tr>
				<td colspan="3" style="padding: 0; height: 10px; background-color: rgba(0,0,0,0)"><img style="width: 100%; opacity: .8" src="/themes/default/web/images/base/shadow_right.png"></td>
			</tr>
			
			
		</table>
		
		<div class="answer"  id="a_<?=$fq->id?>">
			<?=$fq->answer?>
		</div>
</div>
<?php endforeach;?>
<br><br>
<a href="/feedback" id="onlineCallLink" style="text-decoration: none; padding: 15px 60px">Задать вопрос</a>
</div>
<img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/inside_bg1.png" class="insideBg">
</div>

<?php $this->renderPartial('//layouts/onlines') ?>		
   <script type="text/javascript">
	$(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
		var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css({
				left: (ww - cw)/2,
				display: 'inline-block'
			});
            $('#crumbsBlock').css({
            	left: ((ww - cw)/2+lw-106),
            	display: 'block'
            });
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300,
				display: 'block'
			})
		};
	   setInterval(function(){
	       
	
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 200));
	   },500);
			$ ('.answer').hide ();
		
	});
	$(window).resize(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
		var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
			$('#crumbsBlock').css('left', ((ww - cw)/2+lw-106)); 
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300
			})
		};
		$ ('.answer').hide ();
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 200));
		
	})

    
    $('.question').click (function () {
    	if($(this).next('div').hasClass('active')){
    		$ (this).next('div').hide('blind', resz());
    		$(this).next('div').removeClass('active');
    	} else {
    		$ ('.answer').hide ('blind');
        	$ (this).next('div').show('blind', resz());
        	$(this).next('div').addClass('active');
    	}
    	function resz(){
    		
    	   var ch = $('#insideCont').height();
           $('.insidePage').css('height', (ch + 200));
           
    	}

    });
   /*$('.question').each(function(){
   		$(this).click(function(){
   			if((this).next('div').hasClass('active')
   		})
   })*/

	
	
</script>									