<?php $this->pageTitle = $model->theme; ?>
<?php $this->keywords = implode(',',explode(' ',$model->theme));?>

<div id="crumbsBlock"><p><a style="color: white;" href="/">Главная </a> > Вопросы и ответы > <?=$model->theme?></p></div>
<br/><br/>

<h1><?php echo Yii::t('feedback', 'Вопрос и ответ #{id}',array('{id}' => $model->id));?> <?php echo CHtml::link('ЗАДАЙТЕ ВОПРОС',array('/feedback/index/'));?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
                                                    'data' => $model,
                                                    'attributes' => array(                                                        
                                                        'name',                                                                                                                                                                    
                                                        array(
                                                            'name' => 'answer',
                                                            'type' => 'raw'
                                                        ),
                                                    ),
                                               )); ?>

<br/><br/>

<?php $this->widget('application.modules.comment.widgets.CommentsListWidget', array('label' => 'Мнений','model' => $model, 'modelId' => $model->id)); ?>

<br/>

<h3>У Вас есть свое мнение по этому вопросу !? Поделитесь им!</h3>

<?php $this->widget('application.modules.comment.widgets.CommentFormWidget', array('redirectTo' => $this->createUrl('/feedback/contact/faqView/', array('id' => $model->id)), 'model' => $model, 'modelId' => $model->id)); ?>


   <script type="text/javascript">
	$(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
		var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
			$('#crumbsBlock').css('left', ((ww - cw)/2+lw-40)); 
		};
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 200));
		
	})
	$(window).resize(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
		var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
			$('#crumbsBlock').css('left', ((ww - cw)/2+lw-40)); 
		};
		
	})
</script>	