<?php
    $this->breadcrumbs = array(
        Yii::app()->getModule('page')->getCategory() => array(),
        Yii::t('PageModule.page', 'Услуги') => array('/services/default/index'),
        $model->title => array('/services/default/view', 'id' => $model->id),
        Yii::t('PageModule.page', 'Редактирование'),
    );

    $this->pageTitle = Yii::t('PageModule.page', 'Услуга - редактирование');

    $this->menu = array(
        array('icon' => 'list-alt', 'label' => Yii::t('PageModule.page', 'Список услуг'), 'url' => array('/services/default/index')),
        array('icon' => 'plus-sign', 'label' => Yii::t('PageModule.page', 'Добавить услугу'), 'url' => array('/services/default/create')),
        array('label' => Yii::t('PageModule.page', 'Новость') . ' «' . mb_substr($model->title, 0, 32) . '»'),
        array('icon' => 'pencil', 'label' => Yii::t('PageModule.page', 'Редактирование услуги'), 'url' => array(
            '/services/default/update/',
            'id' => $model->id
        )),
        array('icon' => 'eye-open', 'label' => Yii::t('PageModule.page', 'Просмотреть услугу'), 'url' => array(
            '/services/default/view',
            'id' => $model->id
        )),
        array('icon' => 'trash', 'label' => Yii::t('PageModule.page', 'Удалить услугу'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/services/default/delete', 'id' => $model->id),
            'params' => array(Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken),
            'confirm' => Yii::t('PageModule.page', 'Вы уверены, что хотите удалить услугу?'),
            'csrf' => true,
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('PageModule.page', 'Редактирование услуги'); ?><br />
        <small>&laquo;<?php echo $model->title; ?>&raquo;</small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('pages' => $pages,'model' => $model, 'languages' => $languages, 'langModels' => $langModels)); ?>
