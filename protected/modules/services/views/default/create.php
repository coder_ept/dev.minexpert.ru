<?php
    $this->breadcrumbs = array(
        Yii::app()->getModule('page')->getCategory() => array(),
        Yii::t('PageModule.page', 'Услуги') => array('/services/default/index'),
        Yii::t('PageModule.page', 'Добавление услугу'),
    );

    $this->pageTitle = Yii::t('PageModule.page', 'Добавление услуг');

    $this->menu = array(
        array('icon' => 'list-alt', 'label' => Yii::t('PageModule.page', 'Список услуг'), 'url' => array('/services/default/index')),
        array('icon' => 'plus-sign', 'label' => Yii::t('PageModule.page', 'Добавление услуги'), 'url' => array('/services/default/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('PageModule.page', 'Услуги'); ?>
        <small><?php echo Yii::t('PageModule.page', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model, 'pages' => $pages, 'languages' => $languages )); ?>