<?php
/**
 * FileDocComment
 * Page install migration
 * Класс миграций для модуля Page:
 *
 * @category YupeMigration
 * @package  YupeCMS
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m000000_000000_page_base extends YDbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->createTable(
            '{{services}}',
            array(
                'id' => 'pk',
                'category_id' => 'integer DEFAULT NULL',
                'lang' => 'char(2) DEFAULT NULL',
                'parent_id' => 'integer DEFAULT NULL',
                'creation_date' => 'datetime NOT NULL',
                'change_date' => 'datetime NOT NULL',
                'user_id' => 'integer  DEFAULT NULL',
                'change_user_id' => 'integer DEFAULT NULL',
                'name' => 'varchar(150) NOT NULL',
                'title' => 'varchar(250) NOT NULL',
                'slug' => 'varchar(150) NOT NULL',
                'body' => 'text NOT NULL',
                'keywords' => 'varchar(250) NOT NULL',
                'description' => 'varchar(250) NOT NULL',
                'status' => 'integer NOT NULL',
                'is_protected' => "boolean NOT NULL DEFAULT '0'",
                'menu_order' => "integer NOT NULL DEFAULT '0'",
            ),
            $this->getOptions()
        );

        $this->createIndex("ux_{{services}}_slug_lang", '{{services}}', "slug,lang", true);
        $this->createIndex("ix_{{services}}_status", '{{services}}', "status", false);
        $this->createIndex("ix_{{services}}_is_protected", '{{services}}', "is_protected", false);
        $this->createIndex("ix_{{services}}_user_id", '{{services}}', "user_id", false);
        $this->createIndex("ix_{{services}}_change_user_id", '{{services}}', "change_user_id", false);
        $this->createIndex("ix_{{services}}_menu_order", '{{services}}', "menu_order", false);
        $this->createIndex("ix_{{services}}_category_id", '{{services}}', "category_id", false);

        //fk
        $this->addForeignKey("fk_{{services}}_category_id", '{{services}}', 'category_id', '{{category_category}}', 'id', 'SET NULL', 'NO ACTION');
        $this->addForeignKey("fk_{{services}}_user_id", '{{services}}', 'user_id', '{{user_user}}', 'id', 'SET NULL', 'NO ACTION');
        $this->addForeignKey("fk_{{services}}_change_user_id", '{{services}}', 'change_user_id', '{{user_user}}', 'id', 'SET NULL', 'NO ACTION');
    }
 
    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys("{{services}}");
    }
}