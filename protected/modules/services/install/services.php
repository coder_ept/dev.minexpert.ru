<?php
return array(
    'module'   => array(
        'class'  => 'application.modules.services.ServicesModule',
        // Указание здесь layout'a портит отображение на фронтенде:
        //'layout' => '//layouts/column2',
    ),
    'import'    => array(
        'application.modules.services.models.*',
    ),
    'component' => array(),
    'rules'     => array(
        '/services/<slug>' => 'services/services/show',
    ),
);