<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  YupeCMS
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('map')->getCategory() => array(),
        Yii::t('map', 'Маркер') => array('/map/map/index'),
        Yii::t('map', 'Управление'),
    );

    $this->pageTitle = Yii::t('map', 'Управление маркерами');

    $this->menu = array(
        array('icon' => 'list-alt', 'label' => Yii::t('map', 'Управление Маркерами'), 'url' => array('/map/map/index')),
        array('icon' => 'plus-sign', 'label' => Yii::t('map', 'Добавить Маркер'), 'url' => array('/map/map/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('map', 'Управление'); ?>
        <small><?php echo Yii::t('map', 'маркерами'); ?></small>
    </h1>
</div>

<button class="btn btn-small dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
    <i class="icon-search">&nbsp;</i>
    <?php echo CHtml::link(Yii::t('map', 'Поиск Маркера'), '#', array('class' => 'search-button')); ?>
    <span class="caret">&nbsp;</span>
</button>

<div id="search-toggle" class="collapse out search-form">
<?php
Yii::app()->clientScript->registerScript('search', "
    $('.search-form form').submit(function() {
        $.fn.yiiGridView.update('map-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
$this->renderPartial('_search', array('model' => $model));
?>
</div>

<br/>

<p> <?php echo Yii::t('map', 'В данном разделе представлены средства управления Маркером'); ?>
</p>

<?php
 $this->widget('application.modules.yupe.components.YCustomGridView', array(
    'id'           => 'map-grid',
    'type'         => 'condensed',
    'dataProvider' => $model->search(),
    'filter'       => $model,
    'columns'      => array(
        'id',
        'name',
        'description',
        'coord1',
        'coord2',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
        ),
    ),
)); ?>
