<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  YupeCMS
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('map')->getCategory() => array(),
        Yii::t('map', 'Маркер') => array('/map/map/index'),
        Yii::t('map', 'Добавление'),
    );

    $this->pageTitle = Yii::t('map', 'Маркер - добавление');

    $this->menu = array(
        array('icon' => 'list-alt', 'label' => Yii::t('map', 'Управление Маркером'), 'url' => array('/map/map/index')),
        array('icon' => 'plus-sign', 'label' => Yii::t('map', 'Добавить Маркер'), 'url' => array('/map/map/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('map', 'Добавление'); ?>
        <small><?php echo Yii::t('map', 'маркера'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>