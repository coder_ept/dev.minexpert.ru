<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  YupeCMS
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('map')->getCategory() => array(),
        Yii::t('map', 'Маркер') => array('/map/map/index'),
        $model->name => array('/Map/view', 'id' => $model->id),
        Yii::t('map', 'Редактирование'),
    );

    $this->pageTitle = Yii::t('map', 'Маркер - редактирование');

    $this->menu = array(
        array('icon' => 'list-alt', 'label' => Yii::t('map', 'Управление Маркерами'), 'url' => array('/map/map/index')),
        array('icon' => 'plus-sign', 'label' => Yii::t('map', 'Добавить Маркер'), 'url' => array('/map/map/create')),
        array('label' => Yii::t('map', 'Маркер') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'pencil', 'label' => Yii::t('map', 'Редактирование Маркера'), 'url' => array(
            '/Map/update',
            'id' => $model->id
        )),
        array('icon' => 'eye-open', 'label' => Yii::t('map', 'Просмотреть Маркер'), 'url' => array(
            '/Map/view',
            'id' => $model->id
        )),
        array('icon' => 'trash', 'label' => Yii::t('map', 'Удалить Маркер'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/Map/delete', 'id' => $model->id),
            'confirm' => Yii::t('map', 'Вы уверены, что хотите удалить Маркер?'),
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('map', 'Редактирование') . ' ' . Yii::t('map', 'Маркера'); ?><br />
        <small>&laquo;<?php echo $model->name; ?>&raquo;</small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>