<?php
/**
 * Отображение для _form:
 *
 *   @category YupeView
 *   @package  YupeCMS
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm', array(
        'id'                     => 'map-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
        'htmlOptions'            => array('class' => 'well', 'enctype' => 'multipart/form-data'),
        'inlineErrors'           => true,
    )
);
?>

    <div class="alert alert-info">
        <?php echo Yii::t('map', 'Поля, отмеченные'); ?>
        <span class="required">*</span>
        <?php echo Yii::t('map', 'обязательны.'); ?>
    </div>

    <?php echo $form->errorSummary($model); ?>

    <div class="row-fluid control-group <?php echo $model->hasErrors('name') ? 'error' : ''; ?>">
        <?php echo $form->textFieldRow($model, 'name', array('class' => 'span3 popover-help', 'size' => 60, 'maxlength' => 255, 'data-original-title' => $model->getAttributeLabel('name'), 'data-content' => $model->getAttributeDescription('name'))); ?>
    </div>
    <div class="row-fluid control-group <?php echo $model->hasErrors('description') ? 'error' : ''; ?>">
        <?php echo $form->textAreaRow($model, 'description', array('class' => 'span5 popover-help', 'rows' => 6, 'cols' => 50, 'data-original-title' => $model->getAttributeLabel('description'), 'data-content' => $model->getAttributeDescription('description'))); ?>
    </div>
     <div class='row-fluid control-group <?php echo $model->hasErrors("image") ? "error" : ""; ?>'>
        <?php if (!$model->isNewRecord && $model->image): ?>
            <?php echo CHtml::image(Yii::app()->baseUrl . '/' . $this->yupe->uploadPath . '/map/' . $model->image, $model->name, array('width' => 300, 'height' => 300)); ?>
        <?php endif; ?>
        <?php echo  $form->fileFieldRow($model, 'image', array('class' => 'span5', 'maxlength' => 250, 'size' => 60)); ?>
    </div>
    <div class="row-fluid control-group <?php echo $model->hasErrors('coord1') ? 'error' : ''; ?>">
        <?php echo $form->textFieldRow($model, 'coord1', array('class' => 'span3 popover-help', 'size' => 60, 'maxlength' => 60, 'data-original-title' => $model->getAttributeLabel('coord1'), 'data-content' => $model->getAttributeDescription('coord1'))); ?>
    </div>
    <div class="row-fluid control-group <?php echo $model->hasErrors('coord2') ? 'error' : ''; ?>">
        <?php echo $form->textFieldRow($model, 'coord2', array('class' => 'span3 popover-help', 'size' => 60, 'maxlength' => 60, 'data-original-title' => $model->getAttributeLabel('coord2'), 'data-content' => $model->getAttributeDescription('coord2'))); ?>
    </div>

    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'       => 'primary',
            'label'      => Yii::t('map', 'Сохранить Маркер и закрыть'),
        )
    ); ?>
    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'htmlOptions'=> array('name' => 'submit-type', 'value' => 'index'),
            'label'      => Yii::t('map', 'Сохранить Маркер и продолжить'),
        )
    ); ?>

<?php $this->endWidget(); ?>