<?php
/**
 * install config for module Mail:
 *
 * @category YupeInstallConfig
 * @package  YupeCMS
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
return array(
    'module'    => array(
        'class' => 'application.modules.map.MapModule',
    ),
    'import'    => array(),
    'component' => array(
    ),
    'rules'     => array(),
);