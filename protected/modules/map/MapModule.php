<?php

class MapModule extends YWebModule
{
	public function init()
	{
	 parent::init();
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'map.models.*',
			'map.components.*',
		));
	}
    public $uploadPath = 'map';

    public function getUploadPath()
    {
        return Yii::getPathOfAlias('webroot') . '/' . Yii::app()->getModule('yupe')->uploadPath . '/' . $this->uploadPath . '/';
    }
	
    public function getCategory()
    {
        return Yii::t('mapModule.Map', 'Контент');
    }
	public function getName()
    {
        return Yii::t('MapModule.Map', 'Карта');
    }

    public function getDescription()
    {
        return Yii::t('MapModule.Map', 'Модуль для управления маркерами на карте');
    }

    public function getAuthor()
    {
        return Yii::t('MapModule.Map', 'yupe team');
    } 

    public function getAuthorEmail()
    {
        return Yii::t('MapModule.Map', 'team@yupe.ru');
    }

    public function getUrl()
    {
        return Yii::t('MapModule.blog', 'http://yupe.ru');
    }

    public function getAdminPageLink()
    {
        return '/map/map/index';
    }

    public function getIcon()
    {
        return "globe";
    }
	
    public function getNavigation()
    {
        return array(
            array('label' => Yii::t('MapModule.Map', 'Карта')),
            array('icon' => 'list-alt', 'label' => Yii::t('BlogModule.blog', 'Список маркеров'), 'url' => array('/map/map/index')),
            array('icon' => 'plus-sign', 'label' => Yii::t('BlogModule.blog', 'Добавить маркер'), 'url' => array('/map/map/create')),
           
        );
    }
}
