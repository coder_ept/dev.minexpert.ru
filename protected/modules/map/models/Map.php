<?php

/**
 * This is the model class for table "{{map}}".
 *
 * The followings are the available columns in table '{{map}}':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property double $coord1
 * @property double $coord2
 */
class Map extends YModel
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Map the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{map}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name,coord1,coord2','required'),
			array('coord1, coord2', 'numerical'),
			array('name, image', 'length', 'max'=>255),
			array('description, image', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, description, coord1, coord2', 'safe', 'on'=>'search'),
		);
	}
    public function behaviors()
    {
        $module = Yii::app()->getModule('map');
        return array(
            'imageUpload' => array(
                'class'         =>'application.modules.yupe.models.ImageUploadBehavior',
                'scenarios'     => array('insert','update'),
                'attributeName' => 'image',
                'uploadPath'    => $module !== null ? $module->getUploadPath() : null,
                'imageNameCallback' => array($this, 'generateFileName'),
                'resize' => array(
                    'quality' => 70,
                    'width' => 800,
                )
            ),
        );
    }
     public function generateFileName()
    {
        return md5($this->name . time());
    }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название маркера',
			'description' => 'Описание',
			'coord1' => 'Широта',
			'coord2' => 'Долгота',
            'image'             => Yii::t('CategoryModule.category', 'Изображение'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('coord1',$this->coord1);
		$criteria->compare('coord2',$this->coord2);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}