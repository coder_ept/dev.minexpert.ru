<?php
/**
 * Класс MapController:
 *
 *   @category YupeController
 *   @package  YupeCMS
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
class MapController extends YBackController
{
    /** 
     * Отображает Маркер по указанному идентификатору
     *
     * @param integer $id Идинтификатор Маркер для отображения
     *
     * @return nothing
     */
    public function actionView($id)
    {
        $this->render('view', array('model' => $this->loadModel($id)));
    }

    /**
     * Создает новую модель Маркера.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return nothing
     */
	
	private function rewriteMark()
	{			
		$file=Yii::app()->basePath.'/../uploads/documents/structure-all.kml';		
		$model=Map::model()->findAll();
		$xml='<?xml version="1.0" encoding="UTF-8"?>
		<kml xmlns:kml="http://earth.google.com/kml/2.2">
			<Document>
				<name>Филиалы компании</name>
				<open>1</open>';
		$xml.='<Style id="Style-Main">
				<IconStyle>
					<Icon>
						<href>'.Yii::app()->getBaseUrl(true).'/themes/default/web/images/base/mmarker.png</href>
					</Icon>
				</IconStyle>
		</Style>';
		$xml.='<Folder>
				<visibility>1</visibility>	
				<visibility>1</visibility>';
		$counter=0;
		if (!empty($model)){
			foreach($model as $value)
			{
				$xml.='<Placemark>';
					$xml.='<name>'.$value->name.'</name><styleUrl>#Style-Main</styleUrl>';
					$xml.='<description><![CDATA['.$value->description.']]></description>';
					$xml.='<Point><coordinates>'.$value->coord1.', '.$value->coord2.'</coordinates></Point>';				
				$xml.='</Placemark>';
			}
		}
		$xml.='</Folder>
			</Document>
		</kml>';
		$kml = fopen($file,"w");
		fwrite($kml,$xml);
		fclose($kml);		
	}
    public function actionCreate()
    {
        $model = new Map;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Map'])) {
            $model->attributes = $_POST['Map'];

            if ($model->save()) {
					self::rewriteMark();
                Yii::app()->user->setFlash(
                    YFlashMessages::NOTICE_MESSAGE,
                    Yii::t('map', 'Запись добавлена!')
                );
	
                if (!isset($_POST['submit-type']))
                    $this->redirect(array('update', 'id' => $model->id));
                else
                    $this->redirect(array($_POST['submit-type']));
            }
        }
        $this->render('create', array('model' => $model));
    }

    /**
     * Редактирование Маркера.
     *
     * @param integer $id Идинтификатор Маркер для редактирования
     *
     * @return nothing
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Map'])) {
            $model->attributes = $_POST['Map'];
            if ($model->save()) {
                self::rewriteMark();
                Yii::app()->user->setFlash(
                    YFlashMessages::NOTICE_MESSAGE,
                    Yii::t('map', 'Запись обновлена!')
                );

                if (!isset($_POST['submit-type']))
                    $this->redirect(array('update', 'id' => $model->id));
                else
                    $this->redirect(array($_POST['submit-type']));
            }
        }
        $this->render('update', array('model' => $model));
    }

    /**
     * Удаляет модель Маркера из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id идентификатор Маркера, который нужно удалить
     *
     * @return nothing
     */
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest)
        {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();
            self::rewriteMark();
            Yii::app()->user->setFlash(
                YFlashMessages::NOTICE_MESSAGE,
                Yii::t('map', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
            throw new CHttpException(400, Yii::t('map', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
    }

    /**
     * Управление Маркером.
     *
     * @return nothing
     */
    public function actionIndex()
    {
        $model = new Map('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Map']))
            $model->attributes = $_GET['Map'];
        $this->render('index', array('model' => $model));
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param integer идентификатор нужной модели
     *
     * @return nothing
     */
    public function loadModel($id)
    {
        $model = Map::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('map', 'Запрошенная страница не найдена.'));
        return $model;
    }

    /**
     * Производит AJAX-валидацию
     *
     * @param CModel модель, которую необходимо валидировать
     *
     * @return nothing
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'map-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
