<?php
/**
 * Created by PhpStorm.
 * User: kolizey
 * Date: 26.11.13
 * Time: 14:02
 */
class CabinetController extends YFrontController
{
	public $defaultAction = 'index';
    public function filters()
    {
        return array(
            'accessControl',
        );
    }
    public function accessRules()
    {
        return array(
        array('allow',  // allow all users
            'users'=>array('@'),
        ),
            array('deny',  // deny all not-auth users
                'users'=>array('*'),
            ),
        );
    }
    public function actionIndex()
    {
        $model=$this->loadModel(Yii::app()->user->id);
        if (isset($_POST['User']))
        {
            $model->attributes=$_POST['User'];
           // $model->birth_date=strtotime($_POST['User']['birth_date']);
            $model->save();
        }
        $this->render('cabinet',array('model'=>$model));
    }
    public function actionView($id)
    {
        $criteria=new CDbCriteria();
        $criteria->addCondition("order_id = $id");
        $data=new CActiveDataProvider('OrderDetailes',array('criteria'=>$criteria));
        $this->renderPartial('info',array('data'=>$data),false,false);
    }
    public function loadModel($id)
    {
        $model=User::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
    //public function actionMultiaction()
//    {
//        if (isset($_POST['items'])){
//            $data=$_POST['items'];
//            $counter=0;
//            foreach($data as $id)
//            {
//                $counter++;
//                Calculation::model()->deleteByPk($id);
//                Statement::model()->deleteAll("calculate_id = $id");
//            }    
//            Yii::app()->ajax->success();
//        }
//        
//    } 
    public function actionMultiaction()
    {
        //if (!Yii::app()->request->isAjaxRequest || !Yii::app()->request->isPostRequest) {
//            throw new CHttpException(404);
//        }

      //  $model = Yii::app()->request->getPost('model');
      //  $action = Yii::app()->request->getPost('do');

       // if (!isset($model, $action)) {
//            throw new CHttpException(404);
//        }

        $items = Yii::app()->request->getPost('items');
        $count=0;
        if (!is_array($items) || empty($items)) {
            Yii::app()->ajax->success();
        }
                 foreach($items as $id)
                    Statement::model()->deleteAll("calculate_id = $id");  
                $criteria = new CDbCriteria;                
                $items = array_filter($items, 'intval');
                $criteria->addInCondition('id', $items);
               // $count=Calculation::model()->deleteAll($criteria);
                             
                Yii::app()->ajax->success(
                    Yii::t(
                        'YupeModule.yupe', 'Удалено {count} записей!', array(
                            '{count}' => $count
                        )
                    )
                );
    }
    
    public function actionDelete($id)
    {
        Calculation::model()->deleteByPk($id);
        Statement::model()->deleteAll("calculate_id = $id");
    }

    /**
     * Performs the AJAX validation.
     * @param Product $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='private-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}