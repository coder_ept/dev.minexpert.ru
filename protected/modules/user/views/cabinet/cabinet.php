
<?php
/**
 * Created by PhpStorm.
 * User: kolizey
 * Date: 26.11.13
 * Time: 14:00
 */
?>
<style>
	#tabs li {
		font-size: 20px;
	}
	#tabs a {
		color: #474747;
	}
	table.table th, table.table td {
		background-color: #F9F9F9!important;
	}
	.icon {
		font-size: 20px!important;
	}
	#user-grid_c0 {
		
	}
    .but_req
    {
        font-weight: bold;
        background: rgb(255,117,42);
        background: -moz-linear-gradient(top, rgba(255,117,42,1) 0%, rgba(195,69,10,1) 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,117,42,1)), color-stop(100%,rgba(195,69,10,1)));
        background: -webkit-linear-gradient(top, rgba(255,117,42,1) 0%,rgba(195,69,10,1) 100%);
        background: -o-linear-gradient(top, rgba(255,117,42,1) 0%,rgba(195,69,10,1) 100%);
        background: -ms-linear-gradient(top, rgba(255,117,42,1) 0%,rgba(195,69,10,1) 100%);
        background: linear-gradient(to bottom, rgba(255,117,42,1) 0%,rgba(195,69,10,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff752a', endColorstr='#c3450a',GradientType=0 );
        text-shadow: 0px 1px 0px rgba(50, 50, 50, 1);
        color: white;
        padding: 8px;
        font-size: 18px;
    }
    .but_req:hover
    {
        text-decoration: none;
        background: rgb(195,69,10);
        background: -moz-linear-gradient(top, rgba(195,69,10,1) 0%, rgba(255,117,42,1) 71%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(195,69,10,1)), color-stop(71%,rgba(255,117,42,1)));
        background: -webkit-linear-gradient(top, rgba(195,69,10,1) 0%,rgba(255,117,42,1) 71%);
        background: -o-linear-gradient(top, rgba(195,69,10,1) 0%,rgba(255,117,42,1) 71%);
        background: -ms-linear-gradient(top, rgba(195,69,10,1) 0%,rgba(255,117,42,1) 71%);
        background: linear-gradient(to bottom, rgba(195,69,10,1) 0%,rgba(255,117,42,1) 71%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c3450a', endColorstr='#ff752a',GradientType=0 );
        text-shadow: none;    
    }
</style>
<script src='https://ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular.min.js'></script>
<div ng-app>
<script type="text/javascript">
	$(function() {
		$( "#tabs" ).tabs();        
		//var ww = $(window).width();
//		var cw = $('.wrap').width();
//        var lw = $('#logo').width();
//		if (ww > cw) {
//			$('.wrap').css('left', (ww - cw) / 2);
//            $('#crumbsBlock').css('left', ((ww - cw)/2+lw-106));
//		} else {
//			$('#crumbsBlock').css({
//				left: lw-106,
//				width: 300
//			})
//		};
//		//alert($('#insideCont').height());
//		var ch = $('#insideCont').height();
//		$('.insidePage').css('height', (ch + 150));
        setInterval(function(){
            var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
            $('.wrap').css({
                left: (ww - cw)/2,
                display: 'inline-block'
            });
            $('#crumbsBlock').css({
                left: ((ww - cw)/2+lw-106),
                display: 'block'
            });
        } else {
            $('#crumbsBlock').css({
                left: lw-106,
                width: 300,
                display: 'block'
            })
            $('.wrap').css({
                display: 'inline-block'
            });            
        };
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 150));
        },500);
       

	})
	$(window).resize(function() {
		var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
            $('#crumbsBlock').css('left', ((ww - cw)/2+lw-106));
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300
			})
		};
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 150));
	})
    
</script>
<div id="crumbsBlock" style="display: none"><p><a style="color: white;" href="/">Главная </a> > Личный кабинет</p></div>
<div class="insidePage" id="contactsPage" style="background-color: #d9d9d9">
	<div id="insideCont" style="position: absolute; background-color: #fff; text-align: center; margin-top: 80px; text-align: left; display: none" class="wrap">

		
<div class="inner-column">
    <div class="inner-column-right" style="padding:20px;">      
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1"><img src="/themes/default/web/images/base/cab_calc.png">Расчеты</a></li>
                <li><a href="#tabs-2"><img src="/themes/default/web/images/base/cab_ord.png">Заявки</a></li>          
				<li><a href="#tabs-3"><img src="/themes/default/web/images/base/cab_prof.png">Личная информация</a></li> 
				     
            </ul>            
            <div id="tabs-1">
               <?php $this->renderPartial('calculate'); ?>
            </div>
            <div id="tabs-2">
	           <?php $this->renderPartial('requests'); ?>         
            </div>
            <div id="tabs-3">
                <?php $this->renderPartial('private_info',array('model'=>$model)); ?>
            </div>
       </div>
   </div>
</div>

</div>
<img src="<?php echo Yii::app() -> request -> baseUrl; ?>/themes/default/web/images/base/inside_bg1.png" class="insideBg">
</div>
</div>
<?php $this->renderPartial('//layouts/onlines')?>