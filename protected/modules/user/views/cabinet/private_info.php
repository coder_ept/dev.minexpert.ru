<?php
/**
 * Created by PhpStorm.
 * User: kolizey
 * Date: 26.11.13
 * Time: 14:11
 */
?>
<script>
$(document).ready(function(){
$('#User_location').chosen();
	$('#ui-id-2').on('click',function(){
	var ww = $(window).width();
		var cw = $('.wrap').width();
        var lw = $('#logo').width();
		if (ww > cw) {
			$('.wrap').css('left', (ww - cw) / 2);
            $('#crumbsBlock').css('left', ((ww - cw)/2+lw-106));
		} else {
			$('#crumbsBlock').css({
				left: lw-106,
				width: 300
			})
		};
		var ch = $('#insideCont').height();
		$('.insidePage').css('height', (ch + 150));
	});
});
</script>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id'=> 'private-form',
    'enableAjaxValidation'   => false,
    'enableClientValidation' => true,
    'htmlOptions'            => array('class' => 'well'),
));?>
<?php echo $form->errorSummary($model); ?>
<div style="width: 100%;position: relative;margin-left: 20%;">
<table cellspacing="5" cellpadding="10" >   
    <tr>
        <td>Имя<span class="required">*</span></td>
        <td><?=$form->textField($model,'first_name', array('style'=>'width: 400px'))?></td>
    </tr>
    <tr>
        <td>Отчество<span class="required">*</span></td>
        <td><?=$form->textField($model,'middle_name', array('style'=>'width: 400px'))?></td>
    </tr>
    <tr>
        <td>Фамилия<span class="required">*</span></td>
        <td><?=$form->textField($model,'last_name', array('style'=>'width: 400px'))?></td>
    </tr>
    <tr>
        <td>E-mail<span class="required">*</span></td>
        <td><?=$form->textField($model,'email', array('style'=>'width: 400px'))?></td>
    </tr>
	<tr>
        <td>Город</td>
        <td><?=$form->dropDownList($model,'location',City::All(), array('style'=>'width: 400px'))?></td>
    </tr>
	<tr>
        <td>Название компании</td>
        <td><?=$form->textField($model,'company', array('style'=>'width: 400px'))?></td>
    </tr>
    <tr>
        <td>Дата рождения</td>
        <td>		
		<?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'birth_date',
               // 'value'=>date('d.m.Y',strtotime($model->birth_date)),
               'language'=>Yii::app()->getLanguage(),
                 'htmlOptions' => array(
                    'style' => 'width: 400px',
                    'size' => '10',         // textField size
                    'maxlength' => '10',    // textField maxlength
                ), 
                'options'=>array(
                    'dateFormat' => 'dd.mm.yy', // save to db format
                   // 'altField' => '#self_pointing_id',
                    //'altFormat' => 'dd-mm-yy', // show to user format
					'changeMonth' => 'true',
					'changeYear'=>'true',                   
                    'yearRange'=> "1914:2014",
                    
                ), 
            ));
            ?></td>
    </tr>
   
</table>
</div>
<div style="width: 100%; text-align: center">
        <?php echo CHtml::submitButton('Сохранить', array('id'=>'onlineOrderLink', 'style'=>'border: none; height: 44px; width: 240px; display: inline-block;')); ?>
    </div>
<?php $this->endWidget(); ?>
