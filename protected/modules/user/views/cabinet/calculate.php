
<style>
th#user-grid_c5 {
	width: 13%;
}
th#user-grid_c4 {
width: 15%;
}

</style>
<script>
function GetController($scope)
{
	$scope.id=1;
	$scope.printId=function(id)
	{
		$scope.id=id;
	}
}
function sendmail(id)
{
	if (confirm("Отправить заявку?"))
	{
		$.ajax({
			type:'post',
			url:'/site/call?idCall='+id,
			data:'RequestForm=1',
			success:function()
			{
				$.fn.yiiGridView.update("calculate");
                $.fn.yiiGridView.update("requests");
                alert('Заявка успешно отправлена');
			}
		});
	}
}
$(document).ready(function(){
    $('a.delete').one('click',function(){
            $.fn.yiiGridView.update("requests");
        });
});
</script>
	<div ng-controller="GetController">	
    <div style="margin: 20px 0 -80px 0px;">
            <a href="/online"  id="onlineCallLink" style="color: #fff">Рассчитать стоимость</a>
    <a href="/statement/index" id="onlineOrderLink" style="color: #fff">Подать заявку</a>
    </div>

<?php
$criteria=new CDbCriteria();
$id=Yii::app()->user->id;
$criteria->addCondition("user_id = $id");
$criteria->order='id DESC';
$dataProvider=new CActiveDataProvider('Calculation',array(
    'criteria'=>$criteria,
));
$this->widget('application.modules.yupe.components.YCustomGridView', array(
    'id'            => 'calculate',
    'type'         => 'condensed',
    'dataProvider' => $dataProvider,
       //'bulkActions'      => array(
//        /* Массив кнопок действий: */
//        'actionButtons' => array(
//            array(
//                'id'         => 'delete-comment',
//                'buttonType' => 'button',
//                'type'       => 'danger',
//                'size'       => 'small',
//                'label'      => Yii::t('CommentModule.comment', 'Удалить'),
//                /**
//                 *   Логика следующая - получаем массив выбранных эллементов в переменную values,
//                 *   далее передаём в функцию multiaction - необходимое действие и эллементы.
//                 *   Multiaction - делает ajax-запрос на actionMultiaction, где уже происходит
//                 *   обработка данных (указывая собственные действия - необходимо создавать их
//                 *   обработчики в actionMultiaction):
//                 **/
//                'click'      => 'js:function(values){ if(!confirm("' . Yii::t('CommentModule.comment', 'Вы уверены, что хотите удалить выбранные элементы?') . '")) return false; multiaction("delete", values); }',
//            ),
//        ),
//
//        // if grid doesn't have a checkbox column type, it will attach
//        // one and this configuration will be part of it
//        'checkBoxColumnConfig' => array(
//            'name' => 'id'
//        ),
//    ),
    'columns'      => array(
	array(
		'name'=>'date_create',
		'value'=>'date(\'d.m.Y\',strtotime($data->date_create))'
	),        
        array(
            'name'=>'data',
            'type'=>'html',
            'value'=>'$data->data'
        ),
        array(
            'name'=>'cost',
            'value'=>'str_replace(".",",",$data->cost)'
        ),		  

        array(
            'name'=>'status',
            'type'=>'raw',
            'value'=>'$this->grid->returnBootstrapStatusHtml($data, "status", "Status")."  ".$data->getStatus()'
        ),
        array(
            'header'=>'Заявка',
            'type'=>'raw',
            'value'=>'(empty($data->statement))?"<a href=\'/statement/index/".$data->id."\'><i class=\'icon-file\'></i> Заполнить </a>":
			"<a href=\'/statement/update/".$data->statement->id."/".$data->statement->calculate_id."\'><i class=\'icon-edit\'></i> Редактировать</a><br>
			  <a href=\'#print\' role=\'button\' ng-click=\'printId(".$data->statement->id.")\' data-toggle=\'modal\'><i class=\'icon-print\'></i> Распечатать</a><br>
			  <a style=\'cursor:pointer;\' onclick=\'return sendmail($data->id)\'><i class=\'icon-envelope\'></i> Отправить</a>	
			"'
        ),
		/* array(		
			'header'=>'',
            'type'=>'raw',
			'value'=>'(!empty($data->statement))?"<a href=\'#print\' role=\'button\' ng-click=\'printId(".$data->statement->id.")\' data-toggle=\'modal\'><i class=\'icon-print\'></i> Распечатать</a>":""'
		),
		array(		
			'header'=>'',
            'type'=>'raw',
			'value'=>'(!empty($data->statement))?"<a style=\'cursor:pointer;\' onclick=\'return sendmail($data->id)\'><i class=\'icon-envelope\'></i></a>":""'
		), */
     
       
       array(            // display a column with "view", "update" and "delete" buttons
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{delete}',
            'buttons'=>array(
                'send'=>array(
                    'label'=>'<i class="icon-envelope"></i>',                    
                    //'imageUrl'=>'',
                    'options'=>array(
                        'style'=>'float:left',
                        'title'=>'Отправить заявку',
                        'ajax' => array(
                        'type' => 'post', 
                        'url'=>'js:$(this).attr("href")', 
                        'data'=>'RequestForm=1',
                        'success' => 'js:function(data) 
                        { $.fn.yiiGridView.update("user-grid")}')
                    ),                    
                    'url'=>'"/site/call?idCall=$data->id"'
                )
            )                                   
        ),         
    ),
));

?>
<div id="print" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 style="width: 90%;" id="myModalLabel">Печать</h3>
  </div>
  <div class="modal-body">
		<i class="icon-download-alt"></i><a href="/statement/form/{{id}}" download="anketa"> Анкета</a><br>
		<i class="icon-download-alt"></i><a href="/statement/claim/{{id}}" download="zayvlenie"> Заявление</a><br>
		<i class="icon-download-alt"></i><a href="/statement/info/{{id}}" download="svedeniya"> Сведения</a><br>
		<i class="icon-download-alt"></i><a href="/statement/inventory/{{id}}" download="opis"> Опись</a><br>
		<i class="icon-download-alt"></i><a href="/statement/procuratory/{{id}}" download="doverennost"> Доверенность</a>
  </div>  
</div>

	
</div>


