
<script>
	function RequestsController($scope)
	{
		$scope.id=1;
		$scope.printId=function(id)
		{
			$scope.id=id;
		}
	}
    function sendreq(id)
    {
	if (confirm("Отправить заявку?"))
	{
		$.ajax({
			type:'post',
			url:'/site/call?idCall=0&idState='+id,
			data:'RequestForm=1',
			success:function()
			{
				$.fn.yiiGridView.update("requests");
                $.fn.yiiGridView.update("calculate");
                alert('Заявка успешно отправлена');
			}
		});
	}
    }
</script>
<style>

th#requests_c4 {
width: 15%;
}
th#requests_c2 {
width: 10%;
}
</style>
	<div ng-controller="RequestsController">
      <div style="margin: 20px 0 -80px 0px;">
            <a href="/online"  id="onlineCallLink" style="color: #fff">Рассчитать стоимость</a>
    <a href="/statement/index" id="onlineOrderLink" style="color: #fff">Подать заявку</a>
    </div>
	<?php
$criteria=new CDbCriteria();
$id=Yii::app()->user->id;
$criteria->addCondition("user_id = $id");
$criteria->order='id DESC';
$dataProvider=new CActiveDataProvider('Statement',array(
    'criteria'=>$criteria,
));
$this->widget('application.modules.yupe.components.YCustomGridView', array(
    'id'            => 'requests',
    'type'         => 'condensed',
    'dataProvider' => $dataProvider,
    'columns'      => array(
        array(
            'header'=>'Название объекта',
            'type'=>'raw',
            'value'=>'($data->calculate_id==0)?"-":$data->calculate->data'
        ),
        array(
            'header'=>'Промежуточные данные',
            'type'=>'raw',
            'value'=>'($data->calculate_id==0)?"Нет расчета":$data->calculate->data'
        ),
        array(
            'header'=>'Стоимость',
            'type'=>'raw',
            'value'=>'($data->calculate_id==0)?"-":str_replace(".",",",$data->calculate->cost)'        
        ),
        array(
            'header'=>'Дата создания заявки',
            'type'=>'raw',
            'value'=>'($data->calculate_id==0)?"-":$data->calculate->date_create'
        ),
        array(
            'header'=>'Статус',
            'type'=>'raw',
            'value'=>'($data->calculate_id==0)?"-":$data->calculate->getStatus()'
        ),
        array(
            'header'=>'Управление заявкой',
            'type'=>'raw',
            'value'=>'
			"<a href=\'/statement/update/".$data->id."/".$data->calculate_id."\'><i class=\'icon-edit\'></i> Редактировать</a><br>
			  <a href=\'#print_1\' role=\'button\' ng-click=\'printId(".$data->id.")\' data-toggle=\'modal\'><i class=\'icon-print\'></i> Распечатать</a><br>
			  <a style=\'cursor:pointer;\' onclick=\'return sendreq($data->id)\'><i class=\'icon-envelope\'></i> Отправить</a>	
			"'
        ),
        array(            // display a column with "view", "update" and "delete" buttons
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'',
            'buttons'=>array(
                'send'=>array(
                    'label'=>'<i class="icon-envelope"></i>',                    
                    //'imageUrl'=>'',
                    'options'=>array(
                        'style'=>'float:left',
                        'title'=>'Отправить заявку',
                        'ajax' => array(
                        'type' => 'post', 
                        'url'=>'js:$(this).attr("href")', 
                        'data'=>'RequestForm=1',
                        'success' => 'js:function(data) 
                        { $.fn.yiiGridView.update("user-grid")}')
                    ),                    
                    'url'=>'"/site/call?idCall=$data->id"'
                )
            )                                   
        ),         
    ),
));

?>
		<div id="print_1" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		    <h3 style="width: 90%;" id="myModalLabel">Печать</h3>
		  </div>
		  <div class="modal-body">
				<i class="icon-download-alt"></i><a href="/statement/form/{{id}}" download="anketa"> Анкета</a><br>
				<i class="icon-download-alt"></i><a href="/statement/claim/{{id}}" download="zayvlenie"> Заявление</a><br>
				<i class="icon-download-alt"></i><a href="/statement/info/{{id}}" download="svedeniya"> Сведения</a><br>
				<i class="icon-download-alt"></i><a href="/statement/inventory/{{id}}" download="opis"> Опись</a><br>
				<i class="icon-download-alt"></i><a href="/statement/procuratory/{{id}}" download="doverennost"> Доверенность</a>
		  </div>  
		</div>
	
</div>