<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'                     => 'user-form',
    'enableAjaxValidation'   => false,
    'enableClientValidation' => true,
    'type'                   => 'vertical',
    'htmlOptions'            => array('class' => 'well'),
    'inlineErrors'           => true,
)); ?>
 
    <div class="alert alert-info">
        <?php echo Yii::t('UserModule.user', 'Поля, отмеченные'); ?>
        <span class="required">*</span>
        <?php echo Yii::t('UserModule.user', 'обязательны.'); ?>
    </div>

    <?php echo $form->errorSummary($model); ?>
    <div class="row-fluid control-group <?php echo $model->hasErrors('email') ? 'error' : ''; ?>">
        <?php echo $form->textFieldRow($model, 'email', array('class' => 'popover-help span7', 'maxlength' => 150, 'size' => 60, 'data-original-title' => $model->getAttributeLabel('email'), 'data-content' => $model->getAttributeDescription('email'))); ?>
    </div>
    <div class="row-fluid control-group <?php echo $model->hasErrors('last_name') ? 'error' : ''; ?>">
        <?php echo $form->textFieldRow($model, 'last_name', array('class' => 'popover-help span7', 'maxlength' => 150, 'size' => 60, 'data-original-title' => $model->getAttributeLabel('last_name'), 'data-content' => $model->getAttributeDescription('last_name'))); ?>
    </div>
    <div class="row-fluid control-group <?php echo $model->hasErrors('first_name') ? 'error' : ''; ?>">
        <?php echo $form->textFieldRow($model, 'first_name', array('class' => 'popover-help span7', 'maxlength' => 150, 'size' => 60, 'data-original-title' => $model->getAttributeLabel('first_name'), 'data-content' => $model->getAttributeDescription('first_name'))); ?>
    </div>
    <div class="row-fluid control-group <?php echo $model->hasErrors('middle_name') ? 'error' : ''; ?>">
        <?php echo $form->textFieldRow($model, 'middle_name', array('class' => 'popover-help span7', 'maxlength' => 150, 'size' => 60, 'data-original-title' => $model->getAttributeLabel('middle_name'), 'data-content' => $model->getAttributeDescription('middle_name'))); ?>
    </div>
    <div class="row-fluid control-group <?php echo $model->hasErrors('company') ? 'error' : ''; ?>">
        <?php echo $form->textFieldRow($model, 'company', array('class' => 'popover-help span7', 'maxlength' => 150, 'size' => 60, 'data-original-title' => $model->getAttributeLabel('company'), 'data-content' => $model->getAttributeDescription('company'))); ?>
    </div>
    <div class="row-fluid control-group <?php echo $model->hasErrors('phone') ? 'error' : ''; ?>">
        <?php echo $form->textFieldRow($model, 'phone', array('class' => 'popover-help span7', 'maxlength' => 150, 'size' => 60, 'data-original-title' => $model->getAttributeLabel('phone'), 'data-content' => $model->getAttributeDescription('phone'))); ?>
    </div>
    <div class="row-fluid control-group <?php echo $model->hasErrors('registration_date') ? 'error' : ''; ?>">
        <?php echo $form->textFieldRow($model, 'registration_date', array('class' => 'popover-help span7', 'maxlength' => 150, 'size' => 60, 'data-original-title' => $model->getAttributeLabel('registration_date'), 'data-content' => $model->getAttributeDescription('registration_date'))); ?>
    </div>
    <div class="row-fluid control-group <?php echo $model->hasErrors('birth_date') ? 'error' : ''; ?>">
        <?php echo $form->labelEx($model, 'birth_date'); ?>
       <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'birth_date',
               // 'value'=>date('d.m.Y',strtotime($model->birth_date)),
               'language'=>Yii::app()->getLanguage(),
                 'htmlOptions' => array(
                    'style' => 'width: 400px',
                    'size' => '10',         // textField size
                    'maxlength' => '10',    // textField maxlength
                ), 
                'options'=>array(
                    'dateFormat' => 'dd.mm.yy', // save to db format
                   // 'altField' => '#self_pointing_id',
                    //'altFormat' => 'dd-mm-yy', // show to user format
					'changeMonth' => 'true',
					'changeYear'=>'true',                   
                    'yearRange'=> "1914:2014",
                    
                ), 
            ));
            ?>
      		
    </div>
    <div class="row-fluid control-group <?php echo $model->hasErrors('about') ? 'error' : ''; ?>">
        <div class="popover-help" data-original-title='<?php echo $model->getAttributeLabel('about'); ?>' data-content='<?php echo $model->getAttributeDescription('about'); ?>'>
            <?php echo $form->textAreaRow($model, 'about', array('class' => 'span7')); ?>
        </div>
    </div>
    <div class="row-fluid control-group <?php echo $model->hasErrors('gender') ? 'error' : ''; ?>"> 
        <?php echo $form->dropDownListRow($model, 'gender', $model->gendersList,array('class' => 'popover-help span7','data-original-title' => $model->getAttributeLabel('gender'), 'data-content' => $model->getAttributeDescription('gender'))); ?>
    </div>
    <div class="row-fluid control-group <?php echo $model->hasErrors('status') ? 'error' : ''; ?>"> 
        <?php echo $form->dropDownListRow($model, 'status', $model->statusList,array('class' => 'popover-help span7','data-original-title' => $model->getAttributeLabel('status'), 'data-content' => $model->getAttributeDescription('status'))); ?>
    </div>
    <div class="row-fluid control-group <?php echo $model->hasErrors('access_level') ? 'error' : ''; ?>">     
        <?php echo $form->dropDownListRow($model, 'access_level', $model->accessLevelsList,array('class' => 'popover-help span7','data-original-title' => $model->getAttributeLabel('access_level'), 'data-content' => $model->getAttributeDescription('access_level'))); ?>        
    </div>
	<div class="row-fluid control-group <?php echo $model->hasErrors('location') ? 'error' : ''; ?>">     
        <?php echo $form->dropDownListRow($model, 'location', City::All() ,array('class' => 'popover-help span7','data-original-title' => $model->getAttributeLabel('location'), 'data-content' => $model->getAttributeDescription('location'))); ?>        
    </div>

    <div class="row-fluid control-group <?php echo $model->hasErrors('email_confirm') ? 'error' : ''; ?>"> 
        <?php echo $form->dropDownListRow($model, 'email_confirm', $model->emailConfirmStatusList,array('class' => 'popover-help span7','data-original-title' => $model->getAttributeLabel('email_confirm'), 'data-content' => $model->getAttributeDescription('email_confirm'))); ?>
    </div>

   <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type'       => 'primary',
        'label'      => $model->isNewRecord ? Yii::t('UserModule.user', 'Добавить пользователя и продолжить') : Yii::t('UserModule.user', 'Сохранить пользователя и продолжить'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'  => 'submit',
        'htmlOptions' => array('name' => 'submit-type', 'value' => 'index'),
        'label'       => $model->isNewRecord ? Yii::t('UserModule.user', 'Добавить пользователя и закрыть') : Yii::t('UserModule.user', 'Сохранить пользователя и закрыть'),
    )); ?>

<?php $this->endWidget(); ?>
