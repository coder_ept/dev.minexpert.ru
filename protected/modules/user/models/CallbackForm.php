<?php
class CallbackForm extends CFormModel
{    
    public $name;
    public $time;
    public $number;
    public $city;
    
    public function rules()
    {
        return array(
            array('name, time, number, city', 'required'),                      
        );
    }
     public function attributeLabels()
    {
        return array(
            'name'      => Yii::t('UserModule.user', 'Имя'),
            'time'   => Yii::t('UserModule.user', 'Время(МСК)'),
            'number'=> Yii::t('UserModule.user', 'Номер телефона'),
            'city' => Yii::t('UserModule.user', 'Город'),
        );
    }
}