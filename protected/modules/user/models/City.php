
<?php

/**
 * This is the model class for table "{{city}}".
 *
 * The followings are the available columns in table '{{city}}':
 * @property integer $ID
 * @property integer $Region
 * @property integer $District
 * @property integer $Country
 * @property string $Prefix
 * @property string $Name
 * @property string $TZ
 * @property string $TimeZone
 * @property string $TimeZone2
 */
class City extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return City the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{city}}';
    }
	public static function All()
	{
		$model=self::model()->findAll(array('order'=>'Name'));
		$list=CHtml::listData($model,'ID','Name','shown');
		return $list;
	}
    public static function getName($id)
    {
        return self::model()->findByPk($id)->Name;
    }
    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Name', 'required'),
            array('Region, District, Country', 'numerical', 'integerOnly'=>true),
            array('Prefix', 'length', 'max'=>50),
            array('Name, TZ', 'length', 'max'=>128),
            array('TimeZone, TimeZone2', 'length', 'max'=>100),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('ID, Region, District, Country, Prefix, Name, TZ, TimeZone, TimeZone2', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'ID' => 'ID',
            'Region' => 'Region',
            'District' => 'District',
            'Country' => 'Country',
            'Prefix' => 'Prefix',
            'Name' => 'Name',
            'TZ' => 'Tz',
            'TimeZone' => 'Time Zone',
            'TimeZone2' => 'Time Zone2',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('ID',$this->ID);
        $criteria->compare('Region',$this->Region);
        $criteria->compare('District',$this->District);
        $criteria->compare('Country',$this->Country);
        $criteria->compare('Prefix',$this->Prefix,true);
        $criteria->compare('Name',$this->Name,true);
        $criteria->compare('TZ',$this->TZ,true);
        $criteria->compare('TimeZone',$this->TimeZone,true);
        $criteria->compare('TimeZone2',$this->TimeZone2,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}