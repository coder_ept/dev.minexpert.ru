<?php
class ConsultForm extends CFormModel
{
    public $email;
    public $name;
    public $company;
    public $theme;
    public $city;
    public $number;
    
    public function rules()
    {
        return array(
            array('name, number, company, email, theme, city', 'required'),
            array('email', 'email'),   
                
        );
    }
     public function attributeLabels()
    {
        return array(
            'name'      => Yii::t('UserModule.user', 'Имя'),
            'company'   => Yii::t('UserModule.user', 'Компания'),
            'email'=> Yii::t('UserModule.user', 'Email'),
            'theme' => Yii::t('UserModule.user', 'Тема'),
            'city' => Yii::t('UserModule.user', 'Город'),
             'number' => Yii::t('UserModule.user', 'Номер телефона'),
        );
    }
}