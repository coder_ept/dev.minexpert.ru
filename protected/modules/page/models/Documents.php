<?php

/**
 * This is the model class for table "{{documents}}".
 *
 * The followings are the available columns in table '{{documents}}':
 * @property integer $id
 * @property string $name
 * @property string $file
 * @property integer $sort
 * @property integer $category_id
 */
class Documents extends YModel
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Documents the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{documents}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('name, category_id','required'),
			array('sort, category_id', 'numerical', 'integerOnly'=>true),
			array('file', 'length', 'max'=>255),
			array('name', 'safe'),
           // array('file', 'file', 'types'=>'pdf, docx, doc, xls, xlsx, txt'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, file, sort, category_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'category'     => array(self::BELONGS_TO, 'Category', 'category_id'),
		);
	}
    public function beforeSave()
    {
        $file=$_FILES['Documents'];
        if ($file['name']['file']!="" && isset($_FILES['Documents'])){            
            $path=Yii::app()->basePath.'/../uploads/documents/';            
            $type=end(explode(".", $file['name']['file']));            
            $name=YText::translit($file['name']['file']);
            $name=str_replace($type,'',$name); 
            if (move_uploaded_file($file['tmp_name']['file'], $path.$name.'.'.$type)) 
            {
                $this->file=$name.'.'.$type;    
            }
        }else
        {
            if(!$this->isNewRecord)
                $this->file=$this->file;
            else
                $this->file="";
        }
        return parent::beforeSave();
    }
    public function generateFileName($name)
    {
        return md5($name . time());
    }
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Наименование',
			'file' => 'Файл',
			'sort' => 'Порядок сортировки',
			'category_id' => 'Категория',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('category_id',$this->category_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}