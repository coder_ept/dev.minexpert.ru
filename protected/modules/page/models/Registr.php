<?php

/**
 * This is the model class for table "{{registr}}".
 *
 * The followings are the available columns in table '{{registr}}':
 * @property integer $id
 * @property string $date_out
 * @property string $number_made
 * @property string $address
 * @property string $obj_negos_exp
 * @property string $customer
 */
class Registr extends YModel
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Registr the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{registr}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('number_made, obj_negos_exp, customer', 'length', 'max'=>255),
            array('date_out,address ', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, date_out, number_made, address, obj_negos_exp, customer', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'date_out' => 'Дата заключения',
            'number_made' => 'Номер заключения',
            'address' => 'Объект капитального строительства',
            'obj_negos_exp' => 'Объект негосударственной экспертизы',
            'customer' => 'Заказчик',
        );
    }
      public static function rdate($date_input) {
        $date=explode(".", $date_input);
    	switch ($date[1]){
            case 1: $m='января'; break;
            case 2: $m='февраля'; break;
            case 3: $m='марта'; break;
            case 4: $m='апреля'; break;
            case 5: $m='мая'; break;
            case 6: $m='июня'; break;
            case 7: $m='июля'; break;
            case 8: $m='августа'; break;
            case 9: $m='сентября'; break;
            case 10: $m='октября'; break;
            case 11: $m='ноября'; break;
            case 12: $m='декабря'; break;
        }
        return $date[0].'&nbsp;'.$m;
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('date_out',$this->date_out,true);
        $criteria->compare('number_made',$this->number_made,true);
        $criteria->compare('address',$this->address,true);
        $criteria->compare('obj_negos_exp',$this->obj_negos_exp,true);
        $criteria->compare('customer',$this->customer,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}