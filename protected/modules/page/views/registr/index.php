<?php
/* @var $this RegistrController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Реестр',
);

$this->menu=array(
    array('label'=>'Добавить реестр', 'url'=>array('create')),
    array('label'=>'Управление реестром', 'url'=>array('index')),
);
?>

<h1>Реестр</h1>

<?php $this->widget('application.modules.yupe.components.YCustomGridView', array(
    'id'           => 'page-grid',
    'type'         => 'condensed',
    'dataProvider' => $model->search(),    
    'sortField'    => 'order',
    'columns'      => array(
        'id',
        array(
            'name'=>'date_out',
            'type'=>'raw',
            'value'=>'Registr::rdate(date("d.m", strtotime($data -> date_out)))." ".date("Y",strtotime($data -> date_out))'
        ),
        'number_made',                        
        'address',
        'obj_negos_exp',
        'customer',             
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
        ),
    ),
)); ?>