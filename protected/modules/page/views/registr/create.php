<?php
/* @var $this RegistrController */
/* @var $model Registr */

  $this->breadcrumbs = array(
        Yii::app()->getModule('page')->getCategory() => array(),
        Yii::t('PageModule.page', 'Реестр') => array('/page/regist/index'),
        Yii::t('PageModule.page', 'Добавление реестра'),
    );

$this->menu=array(
    array('label'=>'Управление реестром', 'url'=>array('index')),    
);
?>
<div class="page-header">    
    <h1>Создать реестр</h1>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>