
<?php
/* @var $this RegistrController */
/* @var $model Registr */

$this->breadcrumbs=array(
    'Registrs'=>array('index'),
    $model->number_made=>array('view','id'=>$model->id),
    'Редактирование',
);

$this->menu=array(
    array('label'=>'Управление реестром', 'url'=>array('index')),
    array('label'=>'Добавить реестр', 'url'=>array('create')),
    array('label'=>'Просмотр реестра', 'url'=>array('view', 'id'=>$model->id)),    
);
?>

<div class="page-header"><h1>Редактирование реестра <br /><small>#<?php echo $model->number_made; ?></small></h1></div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>