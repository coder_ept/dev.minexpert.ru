<?php
/* @var $this RegistrController */
/* @var $model Registr */
/* @var $form CActiveForm */
?>



<?php 
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm', array(
        'id'                     => 'registr-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
        'htmlOptions'            => array('class' => 'well'),
        'inlineErrors'           => true,
    )
); ?>
    <div class="alert alert-info">
        <?php echo Yii::t('PageModule.page', 'Поля, отмеченные'); ?>
        <span class="required">*</span>
        <?php echo Yii::t('PageModule.page', 'обязательны.'); ?>
    </div>

    <?php echo $form->errorSummary($model); ?>
     <div class="row-fluid control-group <?php echo $model->hasErrors('date_out') ? 'error' : ''; ?>">
        <div class="span5 popover-help" data-original-title='<?php echo $model->getAttributeLabel('date_out'); ?>' data-content='<?php echo $model->getAttributeDescription('date_out'); ?>'>
            <?php
            echo $form->datepickerRow(
                $model, 'date_out', array(
                    'prepend' => '<i class="icon-calendar"></i>',
                    'options' => array(
                        'format'    => 'yyyy-mm-dd',
                        'weekStart' => 1,
                        'autoclose' => true,
                    ),
                    'class'   => 'span12'
                )
            ); ?>
        </div>
    </div>
    <div class="row-fluid control-group">
        <?php echo $form->labelEx($model,'number_made'); ?>
        <?php echo $form->textField($model,'number_made',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'number_made'); ?>
    </div>
    <div class="row-fluid control-group">
        <?php echo $form->labelEx($model,'obj_negos_exp'); ?>
        <?php echo $form->textField($model,'obj_negos_exp',array('class'=>'span5','size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'obj_negos_exp'); ?>
    </div>
    <div class="row-fluid control-group">
        <?php echo $form->labelEx($model,'customer'); ?>
        <?php echo $form->textField($model,'customer',array('class'=>'span5','size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'customer'); ?>
    </div>
    

    <div class="row-fluid control-group">
        <?php echo $form->labelEx($model,'address'); ?>
        <?php echo $form->textArea($model,'address',array('class'=>'span5','style'=>'height:150px;')); ?>
        <?php echo $form->error($model,'address'); ?>
    </div>
   
    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'       => 'primary',
            'label'      => $model->isNewRecord ? Yii::t('blog', 'Добавить реестр и продолжить') : Yii::t('blog', 'Сохранить реестр и продолжить'),
        )
    ); ?>

<?php $this->endWidget(); ?>