
<?php
/* @var $this RegistrController */
/* @var $data Registr */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('date_out')); ?>:</b>
    <?php echo CHtml::encode($data->date_out); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('number_made')); ?>:</b>
    <?php echo CHtml::encode($data->number_made); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
    <?php echo CHtml::encode($data->address); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
    <?php echo CHtml::encode($data->description); ?>
    <br />


</div>