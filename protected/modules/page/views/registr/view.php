<?php
/* @var $this RegistrController */
/* @var $model Registr */

$this->breadcrumbs=array(
    'Registrs'=>array('index'),
    $model->number_made,
);

$this->menu=array(
    array('label'=>'Управление реестром', 'url'=>array('index')),
    array('label'=>'Добавить реестр', 'url'=>array('create')),
    array('label'=>'Редактировать реестр', 'url'=>array('update', 'id'=>$model->id)),
    array('label'=>'Удать реестр', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы уверены что хотите удалить этот реестр?')),    
);
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('PageModule.page', 'Просмотр реестра'); ?>
        <small>&laquo;<?php echo $model->number_made; ?>&raquo;</small>
    </h1>
</div>
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
    'type' => array('condensed', 'striped', 'bordered'),
    'htmlOptions' => array('style'=>'text-align: left'),
    'data'=>$model,
    'attributes'=>array(        
        'date_out',
        'number_made',
        'address',   
        'obj_negos_exp',
        'customer',         
    ),
)); ?>