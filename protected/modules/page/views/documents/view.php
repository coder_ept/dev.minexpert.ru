<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  YupeCMS
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('page')->getCategory() => array(),
        Yii::t('page', 'Документ') => array('/page/documents/index'),
        $model->name,
    );

    $this->pageTitle = Yii::t('page', 'Документ - просмотр');

    $this->menu = array(
        array('icon' => 'list-alt', 'label' => Yii::t('page', 'Управление Документом'), 'url' => array('/page/documents/index')),
        array('icon' => 'plus-sign', 'label' => Yii::t('page', 'Добавить Документ'), 'url' => array('/page/documents/create')),
        array('label' => Yii::t('page', 'Документ') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'pencil', 'label' => Yii::t('page', 'Редактирование Документа'), 'url' => array(
            '/page/documents/update',
            'id' => $model->id
        )),
        array('icon' => 'eye-open', 'label' => Yii::t('page', 'Просмотреть Документ'), 'url' => array(
            '/page/documents/view',
            'id' => $model->id
        )),
        array('icon' => 'trash', 'label' => Yii::t('page', 'Удалить Документ'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/page/documents/delete', 'id' => $model->id),
            'confirm' => Yii::t('page', 'Вы уверены, что хотите удалить Документ?'),
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('page', 'Просмотр') . ' ' . Yii::t('page', 'Документа'); ?><br />
        <small>&laquo;<?php echo $model->name; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
    'data'       => $model,
    'attributes' => array(
        'id',
        'name',
        'file',
        'sort',
        'category_id',
    ),
)); ?>
