<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  YupeCMS
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('page')->getCategory() => array(),
        Yii::t('page', 'Документ') => array('/page/documents/index'),
        Yii::t('page', 'Управление'),
    );

    $this->pageTitle = Yii::t('page', 'Документ - управление');

    $this->menu = array(
        array('icon' => 'list-alt', 'label' => Yii::t('page', 'Управление Документом'), 'url' => array('/page/documents/index')),
        array('icon' => 'plus-sign', 'label' => Yii::t('page', 'Добавить Документ'), 'url' => array('/page/documents/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('page', 'Документ'); ?>
        <small><?php echo Yii::t('page', 'управление'); ?></small>
    </h1>
</div>


<br/>

<p> <?php echo Yii::t('page', 'В данном разделе представлены средства управления Документом'); ?>
</p>

<?php
 $this->widget('application.modules.yupe.components.YCustomGridView', array(
    'id'           => 'documents-grid',
    'type'         => 'condensed',
    'dataProvider' => $model->search(),
    'filter'       => $model,
    'columns'      => array(                
        array(
            'name'=>'name',
            'type'=>'raw',
            'value'=>'$data->name'
         ),
         array(
            'name'=>'category_id',
            'value'=>'$data->category->name'
         ),
        'file',
        'sort',
        
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
        ),
    ),
)); ?>
