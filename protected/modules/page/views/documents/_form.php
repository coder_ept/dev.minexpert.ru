<?php
/**
 * Отображение для _form:
 *
 *   @category YupeView
 *   @package  YupeCMS
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm', array(
        'id'                     => 'documents-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
        'htmlOptions'            => array('class' => 'well', 'enctype' => 'multipart/form-data'),
        'inlineErrors'           => true,
    )
);
?>

    <div class="alert alert-info">
        <?php echo Yii::t('page', 'Поля, отмеченные'); ?>
        <span class="required">*</span>
        <?php echo Yii::t('page', 'обязательны.'); ?>
    </div>

    <?php echo $form->errorSummary($model); ?>
    <div class="row-fluid control-group <?php echo $model->hasErrors('category_id') ? 'error' : ''; ?>">
        <?php echo $form->dropDownListRow($model, 'category_id', Category::getAllList(), array('empty' => Yii::t('PageModule.page', '--выберите--'), 'class' => 'span7 popover-help', 'data-original-title' => $model->getAttributeLabel('category_id'), 'data-content' => $model->getAttributeDescription('category_id'))); ?>
    </div>    
    <div class='row-fluid control-group <?php echo $model->hasErrors("file") ? "error" : ""; ?>'>
        <?php if (!$model->isNewRecord && $model->file): ?>
            <?php echo $form->textFieldRow($model, 'file', array('class' => 'span3 popover-help', 'size' => 60, 'maxlength' => 255, 'data-original-title' => $model->getAttributeLabel('file'), 'data-content' => $model->getAttributeDescription('file'))); ?>
        <?php endif; ?>
        <?php echo  $form->fileFieldRow($model, 'file', array('class' => 'span5', 'maxlength' => 250, 'size' => 60)); ?>
    </div>     
    <div class="row-fluid control-group <?php echo $model->hasErrors('name') ? 'error' : ''; ?>">
        <div class="popover-help" data-original-title='<?php echo $model->getAttributeLabel('name'); ?>' data-content='<?php echo $model->getAttributeDescription('name'); ?>'>
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php
            $this->widget(
                $this->module->editor, array(
                    'model'       => $model,
                    'attribute'   => 'name',
                    'options'     => $this->module->editorOptions,
                )
            ); ?>
        </div>
    </div>
    <div class="row-fluid control-group <?php echo $model->hasErrors('sort') ? 'error' : ''; ?>">
        <?php echo $form->textFieldRow($model, 'sort', array('class' => 'span3 popover-help', 'size' => 60, 'maxlength' => 60, 'data-original-title' => $model->getAttributeLabel('sort'), 'data-content' => $model->getAttributeDescription('sort'))); ?>
    </div>
    

    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'       => 'primary',
            'label'      => Yii::t('page', 'Сохранить Документ и закрыть'),
        )
    ); ?>
    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'htmlOptions'=> array('name' => 'submit-type', 'value' => 'index'),
            'label'      => Yii::t('page', 'Сохранить Документ и продолжить'),
        )
    ); ?>

<?php $this->endWidget(); ?>