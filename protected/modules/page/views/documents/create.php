<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  YupeCMS
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('page')->getCategory() => array(),
        Yii::t('page', 'Документ') => array('/page/documents/index'),
        Yii::t('page', 'Добавление'),
    );

    $this->pageTitle = Yii::t('page', 'Документ - добавление');

    $this->menu = array(
        array('icon' => 'list-alt', 'label' => Yii::t('page', 'Управление Документом'), 'url' => array('/page/documents/index')),
        array('icon' => 'plus-sign', 'label' => Yii::t('page', 'Добавить Документ'), 'url' => array('/page/documents/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('page', 'Документ'); ?>
        <small><?php echo Yii::t('page', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>