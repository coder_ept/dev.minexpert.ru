<?php
class Statement extends YModel
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function tableName()
	{
		return '{{statement}}';
	}
	public function rules()
	{
		return array(
            array('type_documention, type_construction, source_of_finance, count_apartament, count_floor, floors, plat_inn, plat_kpp, plat_ogrn, plat_rs, plat_ks, plat_bik', 'numerical', 'integerOnly'=>true),
            array('claimed_estimated_cost, land_area, built_up_area, building_factor, building_density_factor, building_area, building_volume, area_built_public_buildings', 'numerical'),
            array('company, short_company, phone_company, ceo_company, chief_buhgalter, manager_object, email_contact_person, phone_contact_person, payment_details_organization, name_object_capital_construction, address_object, design_stage, loan_funds, v_t_4_SMR, legal_entity, address_legal_entity_organization, phone_legal_entity_organization, manager_object_legal_entity_organization, email_object_legal_entity_organization, full_name_project_organization, mail_address_project_organization, statement_sro_project_organization, phone_project_organization, fax_project_organization, email_project_organization, fio_gipa, phone_gipa, full_type_survey_organization, full_type_survey2_organization, full_type_survey3_organization, survey_organization, mail_address_survey_organization, statement_sro_survey_organization, phone_survey_organization, fax_survey_organization, organization_customer_buider, mail_address_customer_buider, statement_sro_customer_buider, phone_customer_buider, fax_customer_buider, email_customer_buider, contact_person_customer_buider, phone_contact_person_customer_buider, applicant, email_address_applicant, statement_sro_applicant, phone_applicant, fax_applicant, chief_applicant, post_applicant, procuratory, name_ur_person_procuratory, post_fio_person_procuratory, document_person_procuratory, a1_name_full, a1_name_short, a1_address,a1_address_post, a1_phone, a1_manager,a1_manager_firstname,a1_manager_lastname, a1_manager_phone, a1_manager_email, a1_chieffio, survey2_organization, mail_address_survey2_organization, statement_sro_survey2_organization, statement_sro_start_survey2_organization, phone_survey2_organization, fax_survey2_organization, full_name_accomplice1, statement_sro_accomplice1, fio_gipa_accomplice1, address_accomplice1, full_name_accomplice2, statement_sro_accomplice2, fio_gipa_accomplice2, address_accomplice2, a1_chiefpostt, full_name_accomplice3, statement_sro_accomplice3, fio_gipa_accomplice3, address_accomplice3, phone_legal_entity_organization_contact, site, site2, firstTime, doc_company, plat_bank, full_type_project_organization', 'length', 'max'=>255),
            array('short_legal_entity', 'length', 'max'=>120),
            array('statement_sro_start_project_organization, statement_sro_start_survey_organization, statement_sro_start_customer_buider, statement_sro_start_applicant, date_procuratory, statement_sro_start_accomplice1, statement_sro_start_accomplice2, statement_sro_start_accomplice3, json_inventory', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, company, short_company, phone_company, ceo_company, chief_buhgalter, manager_object, email_contact_person, phone_contact_person, payment_details_organization, name_object_capital_construction, address_object, type_documention, type_construction, design_stage, source_of_finance, loan_funds, claimed_estimated_cost, v_t_4_SMR, legal_entity, short_legal_entity, address_legal_entity_organization, phone_legal_entity_organization, manager_object_legal_entity_organization, email_object_legal_entity_organization, full_name_project_organization, mail_address_project_organization, statement_sro_project_organization, statement_sro_start_project_organization, phone_project_organization, fax_project_organization, email_project_organization, fio_gipa, phone_gipa, full_type_survey_organization, full_type_survey2_organization, full_type_survey3_organization, survey_organization, mail_address_survey_organization, statement_sro_survey_organization, statement_sro_start_survey_organization, phone_survey_organization, fax_survey_organization, organization_customer_buider, mail_address_customer_buider, statement_sro_customer_buider, statement_sro_start_customer_buider, phone_customer_buider, fax_customer_buider, email_customer_buider, contact_person_customer_buider, phone_contact_person_customer_buider, applicant, email_address_applicant, statement_sro_applicant, statement_sro_start_applicant, phone_applicant, fax_applicant, chief_applicant, post_applicant, land_area, built_up_area, building_factor, building_density_factor, building_area, count_apartament, count_floor, floors, building_volume, area_built_public_buildings, procuratory, name_ur_person_procuratory, post_fio_person_procuratory, document_person_procuratory, date_procuratory, a1_name_full, a1_name_short, a1_address, a1_address_post, a1_phone, a1_manager,a1_manager_firstname,a1_manager_lastname, a1_manager_phone, a1_manager_email, a1_chieffio, survey2_organization, mail_address_survey2_organization, statement_sro_survey2_organization, statement_sro_start_survey2_organization, phone_survey2_organization, fax_survey2_organization, full_name_accomplice1, statement_sro_accomplice1, statement_sro_start_accomplice1, fio_gipa_accomplice1, address_accomplice1, full_name_accomplice2, statement_sro_accomplice2, statement_sro_start_accomplice2, fio_gipa_accomplice2, address_accomplice2, a1_chiefpostt, full_name_accomplice3, statement_sro_accomplice3, statement_sro_start_accomplice3, fio_gipa_accomplice3, address_accomplice3, phone_legal_entity_organization_contact, site, site2, json_inventory, firstTime, doc_company', 'safe', 'on'=>'search'),
        );
	}
	public function relations()
	{
		return array(
            'calculate'=>array(self::BELONGS_TO,'Calculation','calculate_id'),
		);
	}
    public function getTypeDocumention()
    {
        $list=array(
            1=>"проектной документации со сметой",
            2=>"проектной документации без сметы",
            3=>"разделов проектной документации",
            4=>"сметной документации",
            0=>"проектной документации без сметы и результатов инженерных изысканий"
        );
        return $list[$this->type_documention];
    }
    public function getTypeConstruction()
    {
        $list=array(
            1=>"новое",
            2=>"реконструкция",
            3=>"капительный ремонт",
            4=>"модернизация"
        );
        return $list[$this->type_construction];
    }
    public function getDesignStage()
    {
        $list=array(
            1=>"Проектная документация",
            2=>"проектная и рабочая документация",
        );
        return $list[$this->design_stage];
    }
    public function getSourceFinance()
    {
        $list=array(
            3=>"Кредитные средства Сбербанк",
            1=>"Собственные средства заказчика",
            2=>"Кредитные средства"
        );
        return $list[$this->source_of_finance];
    }
	public function attributeLabels()
	{
		return array(
            'id' => 'ID',
            'company' => 'Company',
            'short_company' => 'Short Company',
            'phone_company' => 'Phone Company',
            'ceo_company' => 'Ceo Company',
            'chief_buhgalter' => 'Chief Buhgalter',
            'manager_object' => 'Manager Object',
            'email_contact_person' => 'Email Contact Person',
            'phone_contact_person' => 'Phone Contact Person',
            'payment_details_organization' => 'Payment Details Organization',
            'name_object_capital_construction' => 'Name Object Capital Construction',
            'address_object' => 'Address Object',
            'type_documention' => 'Type Documention',
            'type_construction' => 'Type Construction',
            'design_stage' => 'Design Stage',
            'source_of_finance' => 'Source Of Finance',
            'loan_funds' => 'Loan Funds',
            'claimed_estimated_cost' => 'Claimed Estimated Cost',
            'v_t_4_SMR' => 'V T 4 Smr',
            'legal_entity' => 'Legal Entity',
            'short_legal_entity' => 'Short Legal Entity',
            'address_legal_entity_organization' => 'Address Legal Entity Organization',
            'phone_legal_entity_organization' => 'Phone Legal Entity Organization',
            'manager_object_legal_entity_organization' => 'Manager Object Legal Entity Organization',
            'email_object_legal_entity_organization' => 'Email Object Legal Entity Organization',
            'full_name_project_organization' => 'Full Name Project Organization',
            'mail_address_project_organization' => 'Mail Address Project Organization',
            'statement_sro_project_organization' => 'Statement Sro Project Organization',
            'statement_sro_start_project_organization' => 'Statement Sro Start Project Organization',
            'phone_project_organization' => 'Phone Project Organization',
            'fax_project_organization' => 'Fax Project Organization',
            'email_project_organization' => 'Email Project Organization',
            'fio_gipa' => 'Fio Gipa',
            'phone_gipa' => 'Phone Gipa',
            'full_type_survey_organization' => 'Full Type Survey Organization',
            'full_type_survey2_organization' => 'Full Type Survey 2 Organization',
            'full_type_survey3_organization' => 'Full Type Survey 3 Organization',
            'survey_organization' => 'Survey Organization',
            'mail_address_survey_organization' => 'Mail Address Survey Organization',
            'statement_sro_survey_organization' => 'Statement Sro Survey Organization',
            'statement_sro_start_survey_organization' => 'Statement Sro Start Survey Organization',
            'phone_survey_organization' => 'Phone Survey Organization',
            'fax_survey_organization' => 'Fax Survey Organization',
            'organization_customer_buider' => 'Organization Customer Buider',
            'mail_address_customer_buider' => 'Mail Address Customer Buider',
            'statement_sro_customer_buider' => 'Statement Sro Customer Buider',
            'statement_sro_start_customer_buider' => 'Statement Sro Start Customer Buider',
            'phone_customer_buider' => 'Phone Customer Buider',
            'fax_customer_buider' => 'Fax Customer Buider',
            'email_customer_buider' => 'Email Customer Buider',
            'contact_person_customer_buider' => 'Contact Person Customer Buider',
            'phone_contact_person_customer_buider' => 'Phone Contact Person Customer Buider',
            'applicant' => 'Applicant',
            'email_address_applicant' => 'Email Address Applicant',
            'statement_sro_applicant' => 'Statement Sro Applicant',
            'statement_sro_start_applicant' => 'Statement Sro Start Applicant',
            'phone_applicant' => 'Phone Applicant',
            'fax_applicant' => 'Fax Applicant',
            'chief_applicant' => 'Chief Applicant',
            'post_applicant' => 'Post Applicant',
            'land_area' => 'Land Area',
            'built_up_area' => 'Built Up Area',
            'building_factor' => 'Building Factor',
            'building_density_factor' => 'Building Density Factor',
            'building_area' => 'Building Area',
            'count_apartament' => 'Count Apartament',
            'count_floor' => 'Count Floor',
            'floors' => 'Floors',
            'building_volume' => 'Building Volume',
            'area_built_public_buildings' => 'Area Built Public Buildings',
            'procuratory' => 'Procuratory',
            'name_ur_person_procuratory' => 'Name Ur Person Procuratory',
            'post_fio_person_procuratory' => 'Post Fio Person Procuratory',
            'document_person_procuratory' => 'Document Person Procuratory',
            'date_procuratory' => 'Date Procuratory',
            'a1_name_full' => 'A1 Name Full',
            'a1_name_short' => 'A1 Name Short',
            'a1_address' => 'A1 Address',
            'a1_address_post' => 'A1 Address post',
            'a1_phone' => 'A1 Phone',
            'a1_manager' => 'A1 Manager Ot4estvo',
            'a1_manager_firstname' => 'A1 Manager FirstName',
            'a1_manager_lastname' => 'A1 Manager LastName',
            'a1_manager_phone' => 'A1 Manager Phone',
            'a1_manager_email' => 'A1 Manager Email',
            'a1_chieffio' => 'A1 Chieffio',
            'survey2_organization' => 'Survey2 Organization',
            'mail_address_survey2_organization' => 'Mail Address Survey2 Organization',
            'statement_sro_survey2_organization' => 'Statement Sro Survey2 Organization',
            'statement_sro_start_survey2_organization' => 'Statement Sro Start Survey2 Organization',
            'phone_survey2_organization' => 'Phone Survey2 Organization',
            'fax_survey2_organization' => 'Fax Survey2 Organization',
            'full_name_accomplice1' => 'Full Name Accomplice1',
            'statement_sro_accomplice1' => 'Statement Sro Accomplice1',
            'statement_sro_start_accomplice1' => 'Statement Sro Start Accomplice1',
            'fio_gipa_accomplice1' => 'Fio Gipa Accomplice1',
            'address_accomplice1' => 'Address Accomplice1',
            'full_name_accomplice2' => 'Full Name Accomplice2',
            'statement_sro_accomplice2' => 'Statement Sro Accomplice2',
            'statement_sro_start_accomplice2' => 'Statement Sro Start Accomplice2',
            'fio_gipa_accomplice2' => 'Fio Gipa Accomplice2',
            'address_accomplice2' => 'Address Accomplice2',
            'a1_chiefpostt' => 'A1 Chiefpostt',
            'full_name_accomplice3' => 'Full Name Accomplice3',
            'statement_sro_accomplice3' => 'Statement Sro Accomplice3',
            'statement_sro_start_accomplice3' => 'Statement Sro Start Accomplice3',
            'fio_gipa_accomplice3' => 'Fio Gipa Accomplice3',
            'address_accomplice3' => 'Address Accomplice3',
            'phone_legal_entity_organization_contact' => 'Phone Legal Entity Organization Contact',
            'site' => 'Site',
            'site2' => 'Site2',
            'json_inventory' => 'Json Inventory',
            'firstTime' => 'First Time',
            'doc_company' => 'Doc Company',
        );
	}
	public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('company',$this->company,true);
        $criteria->compare('short_company',$this->short_company,true);
        $criteria->compare('phone_company',$this->phone_company,true);
        $criteria->compare('ceo_company',$this->ceo_company,true);
        $criteria->compare('chief_buhgalter',$this->chief_buhgalter,true);
        $criteria->compare('manager_object',$this->manager_object,true);
        $criteria->compare('email_contact_person',$this->email_contact_person,true);
        $criteria->compare('phone_contact_person',$this->phone_contact_person,true);
        $criteria->compare('payment_details_organization',$this->payment_details_organization,true);
        $criteria->compare('name_object_capital_construction',$this->name_object_capital_construction,true);
        $criteria->compare('address_object',$this->address_object,true);
        $criteria->compare('type_documention',$this->type_documention);
        $criteria->compare('type_construction',$this->type_construction);
        $criteria->compare('design_stage',$this->design_stage,true);
        $criteria->compare('source_of_finance',$this->source_of_finance);
        $criteria->compare('loan_funds',$this->loan_funds,true);
        $criteria->compare('claimed_estimated_cost',$this->claimed_estimated_cost);
        $criteria->compare('v_t_4_SMR',$this->v_t_4_SMR,true);
        $criteria->compare('legal_entity',$this->legal_entity,true);
        $criteria->compare('short_legal_entity',$this->short_legal_entity,true);
        $criteria->compare('address_legal_entity_organization',$this->address_legal_entity_organization,true);
        $criteria->compare('phone_legal_entity_organization',$this->phone_legal_entity_organization,true);
        $criteria->compare('manager_object_legal_entity_organization',$this->manager_object_legal_entity_organization,true);
        $criteria->compare('email_object_legal_entity_organization',$this->email_object_legal_entity_organization,true);
        $criteria->compare('full_name_project_organization',$this->full_name_project_organization,true);
        $criteria->compare('mail_address_project_organization',$this->mail_address_project_organization,true);
        $criteria->compare('statement_sro_project_organization',$this->statement_sro_project_organization,true);
        $criteria->compare('statement_sro_start_project_organization',$this->statement_sro_start_project_organization,true);
        $criteria->compare('phone_project_organization',$this->phone_project_organization,true);
        $criteria->compare('fax_project_organization',$this->fax_project_organization,true);
        $criteria->compare('email_project_organization',$this->email_project_organization,true);
        $criteria->compare('fio_gipa',$this->fio_gipa,true);
        $criteria->compare('phone_gipa',$this->phone_gipa,true);
        $criteria->compare('full_type_survey_organization',$this->full_type_survey_organization,true);
        $criteria->compare('survey_organization',$this->survey_organization,true);
        $criteria->compare('mail_address_survey_organization',$this->mail_address_survey_organization,true);
        $criteria->compare('statement_sro_survey_organization',$this->statement_sro_survey_organization,true);
        $criteria->compare('statement_sro_start_survey_organization',$this->statement_sro_start_survey_organization,true);
        $criteria->compare('phone_survey_organization',$this->phone_survey_organization,true);
        $criteria->compare('fax_survey_organization',$this->fax_survey_organization,true);
        $criteria->compare('organization_customer_buider',$this->organization_customer_buider,true);
        $criteria->compare('mail_address_customer_buider',$this->mail_address_customer_buider,true);
        $criteria->compare('statement_sro_customer_buider',$this->statement_sro_customer_buider,true);
        $criteria->compare('statement_sro_start_customer_buider',$this->statement_sro_start_customer_buider,true);
        $criteria->compare('phone_customer_buider',$this->phone_customer_buider,true);
        $criteria->compare('fax_customer_buider',$this->fax_customer_buider,true);
        $criteria->compare('email_customer_buider',$this->email_customer_buider,true);
        $criteria->compare('contact_person_customer_buider',$this->contact_person_customer_buider,true);
        $criteria->compare('phone_contact_person_customer_buider',$this->phone_contact_person_customer_buider,true);
        $criteria->compare('applicant',$this->applicant,true);
        $criteria->compare('email_address_applicant',$this->email_address_applicant,true);
        $criteria->compare('statement_sro_applicant',$this->statement_sro_applicant,true);
        $criteria->compare('statement_sro_start_applicant',$this->statement_sro_start_applicant,true);
        $criteria->compare('phone_applicant',$this->phone_applicant,true);
        $criteria->compare('fax_applicant',$this->fax_applicant,true);
        $criteria->compare('chief_applicant',$this->chief_applicant,true);
        $criteria->compare('post_applicant',$this->post_applicant,true);
        $criteria->compare('land_area',$this->land_area);
        $criteria->compare('built_up_area',$this->built_up_area);
        $criteria->compare('building_factor',$this->building_factor);
        $criteria->compare('building_density_factor',$this->building_density_factor);
        $criteria->compare('building_area',$this->building_area);
        $criteria->compare('count_apartament',$this->count_apartament);
        $criteria->compare('count_floor',$this->count_floor);
        $criteria->compare('floors',$this->floors);
        $criteria->compare('building_volume',$this->building_volume);
        $criteria->compare('area_built_public_buildings',$this->area_built_public_buildings);
        $criteria->compare('procuratory',$this->procuratory,true);
        $criteria->compare('name_ur_person_procuratory',$this->name_ur_person_procuratory,true);
        $criteria->compare('post_fio_person_procuratory',$this->post_fio_person_procuratory,true);
        $criteria->compare('document_person_procuratory',$this->document_person_procuratory,true);
        $criteria->compare('date_procuratory',$this->date_procuratory,true);
        $criteria->compare('a1_name_full',$this->a1_name_full,true);
        $criteria->compare('a1_name_short',$this->a1_name_short,true);
        $criteria->compare('a1_address',$this->a1_address,true);
        $criteria->compare('a1_address_post',$this->a1_address_post,true);
        $criteria->compare('a1_phone',$this->a1_phone,true);

        $criteria->compare('a1_manager',$this->a1_manager,true);
        $criteria->compare('a1_manager_firstname',$this->a1_manager_firstname,true);
        $criteria->compare('a1_manager_lastname',$this->a1_manager_lastname,true);

        $criteria->compare('a1_manager_phone',$this->a1_manager_phone,true);
        $criteria->compare('a1_manager_email',$this->a1_manager_email,true);
        $criteria->compare('a1_chieffio',$this->a1_chieffio,true);
        $criteria->compare('survey2_organization',$this->survey2_organization,true);
        $criteria->compare('mail_address_survey2_organization',$this->mail_address_survey2_organization,true);
        $criteria->compare('statement_sro_survey2_organization',$this->statement_sro_survey2_organization,true);
        $criteria->compare('statement_sro_start_survey2_organization',$this->statement_sro_start_survey2_organization,true);
        $criteria->compare('phone_survey2_organization',$this->phone_survey2_organization,true);
        $criteria->compare('fax_survey2_organization',$this->fax_survey2_organization,true);
        $criteria->compare('full_name_accomplice1',$this->full_name_accomplice1,true);
        $criteria->compare('statement_sro_accomplice1',$this->statement_sro_accomplice1,true);
        $criteria->compare('statement_sro_start_accomplice1',$this->statement_sro_start_accomplice1,true);
        $criteria->compare('fio_gipa_accomplice1',$this->fio_gipa_accomplice1,true);
        $criteria->compare('address_accomplice1',$this->address_accomplice1,true);
        $criteria->compare('full_name_accomplice2',$this->full_name_accomplice2,true);
        $criteria->compare('statement_sro_accomplice2',$this->statement_sro_accomplice2,true);
        $criteria->compare('statement_sro_start_accomplice2',$this->statement_sro_start_accomplice2,true);
        $criteria->compare('fio_gipa_accomplice2',$this->fio_gipa_accomplice2,true);
        $criteria->compare('address_accomplice2',$this->address_accomplice2,true);
        $criteria->compare('a1_chiefpostt',$this->a1_chiefpostt,true);
        $criteria->compare('full_name_accomplice3',$this->full_name_accomplice3,true);
        $criteria->compare('statement_sro_accomplice3',$this->statement_sro_accomplice3,true);
        $criteria->compare('statement_sro_start_accomplice3',$this->statement_sro_start_accomplice3,true);
        $criteria->compare('fio_gipa_accomplice3',$this->fio_gipa_accomplice3,true);
        $criteria->compare('address_accomplice3',$this->address_accomplice3,true);
        $criteria->compare('phone_legal_entity_organization_contact',$this->phone_legal_entity_organization_contact,true);
        $criteria->compare('site',$this->site,true);
        $criteria->compare('site2',$this->site2,true);
        $criteria->compare('json_inventory',$this->json_inventory,true);
        $criteria->compare('firstTime',$this->firstTime,true);
        $criteria->compare('doc_company',$this->doc_company,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}
