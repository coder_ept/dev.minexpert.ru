<?php

/**
 * This is the model class for table "{{seismic_zones}}".
 *
 * The followings are the available columns in table '{{seismic_zones}}':
 * @property integer $id
 * @property integer $region_id
 * @property string $city
 * @property integer $a
 * @property integer $b
 */
class SeismicZones extends YModel
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SeismicZones the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{seismic_zones}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('region_id, a, b', 'numerical', 'integerOnly'=>true),
			array('city', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, region_id, city, a, b', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'region'=>array(self::BELONGS_TO, 'SeismicZonesRegion','region_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'region_id' => 'Region',
			'city' => 'City',
			'a' => 'A',
			'b' => 'B',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('region_id',$this->region_id);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('a',$this->a);
		$criteria->compare('b',$this->b);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}