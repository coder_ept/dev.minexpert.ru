<?php

/**
 * This is the model class for table "net_city".
 *
 * The followings are the available columns in table 'net_city':
 * @property integer $id
 * @property integer $country_id
 * @property string $name_ru
 * @property string $name_en
 * @property string $region
 * @property string $postal_code
 * @property string $latitude
 * @property string $longitude
 */
class Org extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public $full_type_project_organization;
    public $a1_name_short;
    public $a1_index_jur;
    public $a1_region_jur;
    public $a1_city_jur;
    public $a1_street_jur;
    public $a1_house_jur;
    public $a1_corpus_jur;
    public $a1_apt_jur;
    public $a1_post;
    public $a1_index;
    public $a1_region_post;
    public $a1_city_post;
    public $a1_street_post;
    public $a1_house_post;
    public $a1_corpus_post;
    public $a1_apt_post;
    public $a1_phone;
    public $plat_inn;
    public $plat_kpp;
    public $plat_ogrn;
    public $plat_rs;
    public $plat_bank;
    public $plat_ks;
    public $ceo_company;
    public $ceo_name;
    public $ceo_patronimic;
    public $a1_chiefpostt;
    public $buhgalter_surname;
    public $buhgalter_name;
    public $buhgalter_patronimic;
    public $a1_address;
    public $a1_address_post;
    public $plat_bik;
    public $chief_buhgalter;


    public function tableName()
    {
        return 'mexp_org';
    }

    public function rules()
    {

        return array(
            array('title', 'required'),
            array(
                'id, title, user_id,
                full_type_project_organization,
                a1_name_short,
                a1_index_jur,
                a1_address_post,
                a1_region_jur,
                a1_city_jur,
                a1_street_jur,
                a1_house_jur,
                a1_corpus_jur,
                a1_apt_jur,
                a1_post,
                a1_address
                a1_index,
                plat_bik,
                a1_region_post,
                a1_city_post,
                a1_street_post,
                a1_house_post,
                a1_corpus_post,
                a1_apt_post,
                chief_buhgalter,
                a1_phone,
                plat_inn,
                plat_kpp,
                plat_ogrn,
                plat_rs,
                plat_bank,
                plat_ks,
                ceo_company,
                ceo_name,
                ceo_patronimic,
                a1_chiefpostt,
                buhgalter_surname,
                buhgalter_name,
                buhgalter_patronimic,

                ,
               ',
                'safe', 'on'=>'search'),
            array(
                'title, user_id,
                full_type_project_organization,
                a1_name_short,
                a1_index_jur,
                a1_address,
                a1_region_jur,
                a1_city_jur,
                a1_address_post,
                a1_street_jur,
                a1_house_jur,
                a1_corpus_jur,
                a1_apt_jur,
                a1_post,
                a1_index,
                a1_region_post,
                a1_city_post,
                a1_street_post,
                a1_house_post,
                a1_corpus_post,
                a1_apt_post,
                a1_phone,
                plat_inn,
                plat_kpp,
                plat_ogrn,
                plat_rs,
                plat_bank,
                plat_ks,
                ceo_company,
                plat_bik,
                ceo_name,
                ceo_patronimic,
                a1_chiefpostt,
                buhgalter_surname,
                buhgalter_name,
                buhgalter_patronimic,
                chief_buhgalter,
                ',
                'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'title',
            'user_id' => 'user id',

            'full_type_project_organization' => 'full_type_project_organization',//Организационно-правовая форма
            'a1_name_short' => 'a1_name_short',//Название юридического лица
            'a1_index_jur' => 'a1_index_jur',//индекс
            'a1_region_jur' => 'a1_region_jur',//регион
            'a1_city_jur' => 'a1_city_jur',//город
            'a1_street_jur' => 'a1_street_jur',//улица
            'a1_house_jur' => 'a1_house_jur',//дом
            'a1_corpus_jur' => 'a1_corpus_jur',//корпус
            'a1_apt_jur' => 'a1_apt_jur',//квартира

            //Почтовый адрес совпадает с юридическим
            'a1_post' => 'a1_post',//чекбокс 1/0 - Почтовый адрес совпадает с юридическим
            'a1_index' => 'a1_index',//индекс
            'a1_region_post' => 'a1_region_post',//регион
            'a1_city_post' => 'a1_city_post',//город
            'a1_street_post' => 'a1_street_post',//улица
            'a1_house_post' => 'a1_house_post',//дом
            'a1_corpus_post' => 'a1_corpus_post',//корпус
            'a1_apt_post' => 'a1_apt_post',//квартира
            'a1_address' => 'a1_address',//квартира
            'a1_address_post' => 'a1_address_post',//квартира

            //платежная система
            'a1_phone' => 'a1_phone',//Телефон организации
            'plat_inn' => 'plat_inn',//ИНН
            'plat_kpp' => 'plat_kpp',//КПП
            'plat_ogrn' => 'plat_ogrn',//ОГРН
            'plat_rs' => 'plat_rs',//Р/С
            'plat_bank' => 'plat_bank',//Банк
            'plat_ks' => 'plat_ks',//К/С
            'plat_bik' => 'plat_bik',//К/С

            //Руководитель
            'ceo_company' => 'ceo_company',//Фамилия
            'ceo_name' => 'ceo_name',//Имя
            'ceo_patronimic' => 'ceo_patronimic',//Отчество
            'a1_chiefpostt' => 'a1_chiefpostt',//Должность руководителя

            //Главный бухгалтер
            'buhgalter_surname' => 'buhgalter_surname',//Фамилия
            'buhgalter_name' => 'buhgalter_name',//Имя
            'buhgalter_patronimic' => 'buhgalter_patronimic',//Отчество
            'chief_buhgalter' => 'chief_buhgalter',//Отчество

        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('user_id',$this->user_id,true);

        $criteria->compare('full_type_project_organization',$this->full_type_project_organization ,true);
        $criteria->compare('a1_name_short',$this->a1_name_short ,true);
        $criteria->compare('a1_index_jur',$this->a1_index_jur ,true);
        $criteria->compare('a1_region_jur',$this->a1_region_jur ,true);
        $criteria->compare('a1_city_jur',$this->a1_city_jur ,true);
        $criteria->compare('a1_street_jur',$this->a1_street_jur ,true);
        $criteria->compare('a1_house_jur',$this->a1_house_jur ,true);
        $criteria->compare('a1_corpus_jur',$this->a1_corpus_jur ,true);
        $criteria->compare('a1_apt_jur',$this->a1_apt_jur ,true);
        $criteria->compare('a1_post',$this->a1_post ,true);
        $criteria->compare('a1_index',$this->a1_index ,true);
        $criteria->compare('a1_region_post',$this->a1_region_post ,true);
        $criteria->compare('a1_city_post',$this->a1_city_post ,true);
        $criteria->compare('a1_street_post',$this->a1_street_post ,true);
        $criteria->compare('a1_house_post',$this->a1_house_post ,true);
        $criteria->compare('a1_corpus_post',$this->a1_corpus_post ,true);
        $criteria->compare('a1_apt_post',$this->a1_apt_post ,true);
        $criteria->compare('a1_phone',$this->a1_phone ,true);
        $criteria->compare('plat_inn',$this->plat_inn ,true);
        $criteria->compare('plat_kpp',$this->plat_kpp ,true);
        $criteria->compare('plat_ogrn',$this->plat_ogrn ,true);
        $criteria->compare('plat_rs',$this->plat_rs ,true);
        $criteria->compare('plat_bank',$this->plat_bank ,true);
        $criteria->compare('plat_ks',$this->plat_ks ,true);
        $criteria->compare('ceo_company',$this->ceo_company ,true);
        $criteria->compare('ceo_name',$this->ceo_name ,true);
        $criteria->compare('ceo_patronimic',$this->ceo_patronimic ,true);
        $criteria->compare('a1_chiefpostt',$this->a1_chiefpostt ,true);
        $criteria->compare('buhgalter_surname',$this->buhgalter_surname ,true);
        $criteria->compare('buhgalter_name',$this->buhgalter_name ,true);
        $criteria->compare('buhgalter_patronimic',$this->buhgalter_patronimic ,true);
        $criteria->compare('a1_address',$this->a1_address ,true);
        $criteria->compare('a1_address_post',$this->a1_address_post ,true);
        $criteria->compare('plat_bik',$this->plat_bik ,true);
        $criteria->compare('chief_buhgalter',$this->chief_buhgalter ,true);


        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return net_city the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
