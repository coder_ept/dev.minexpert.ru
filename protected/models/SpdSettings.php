<?php

/**
 * This is the model class for table "{{spd_settings}}".
 *
 * The followings are the available columns in table '{{spd_settings}}':
 * @property integer $id
 * @property double $a
 * @property double $b
 * @property string $units
 * @property integer $spd_settings_id
 * @property integer $spd_item
 * @property string $comment
 */
class SpdSettings extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SpdSettings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{spd_settings}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('a, b', 'required'),
			array('spd_settings_id, spd_item', 'numerical', 'integerOnly'=>true),
			array('a, b', 'numerical'),
			array('units, comment', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, a, b, units, spd_settings_id, spd_item, comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'name'=>array(self::BELONGS_TO,'SpdItem','spd_item'),
            'set_name'=>array(self::BELONGS_TO,'SpdSettingsDefine','spd_settings_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'a' => 'A',
			'b' => 'B',
			'units' => 'Units',
			'spd_settings_id' => 'Spd Settings',
			'spd_item' => 'Spd Item',
			'comment' => 'Comment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('a',$this->a);
		$criteria->compare('b',$this->b);
		$criteria->compare('units',$this->units,true);
		$criteria->compare('spd_settings_id',$this->spd_settings_id);
		$criteria->compare('spd_item',$this->spd_item);
		$criteria->compare('comment',$this->comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}