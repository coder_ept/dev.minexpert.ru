<?php
/**
 * Дефолтный контроллер сайта:
 *
 * @category YupeController
 * @package  YupeCMS
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_BSD
 * @version  0.5.3 (dev)
 * @link     http://yupe.ru
 *
 **/
class SiteController extends YFrontController
{
   
    public function actionIndex(){
        
        $this->render('welcome');}    
    public function actionError()
    {
        $error = Yii::app()->errorHandler->error;
        if (empty($error) || !isset($error['code']) || !(isset($error['message']) || isset($error['msg']))) {$this->redirect(array('index'));}
        if (Yii::app()->request->isAjaxRequest) {echo json_encode($error);  } else {$this->render('error',array('error' => $error));}
    }
    
    
	public function actionOnline()
	{
	   $message='
       <center><h1>Блогадарим за посещение нашего сайта</h1></center>
       <br><br>Уважаемый(ая) пользователь, Ваш расчет составляет:<br>';
       if (Yii::app()->request->isAjaxRequest){
        
            if($_POST['make_id']!=4){
                $data="Тип: ".$_POST['etap']."<br>Вариант экспертизы: ".$_POST['event']."<br>".$_POST['intermediate'];    
            }else
            {
                $data=$_POST['etap']."<br>".$_POST['event']."<br>";
                $inter_settings=SpdSettings::model()->findByPk($_POST['settings_id']);
                $data.=$inter_settings->name->category->name.'<br>'.$inter_settings->name->name.(($inter_settings->spd_settings_id!=0)?' '.$inter_settings->set_name->name:'');
                $data.="<br>".$_POST['intermediate'].' ('.$inter_settings->units.')';
            }                        
            //print_r($_POST['data']);
//            Yii::app()->end();
            //$zoneModel=SeismicZones::model()->findByPk($_POST['zone_id']);
            $zone='Место строительства: '.$_POST['region'].', '.$_POST['zone'].'.<br>';
            $ml=new Calculation;
            $ml->json_data=CJSON::encode(array_merge($_POST['data'],array('cost'=>$_POST['cost'])));
            $ml->data=$zone.$data;     
            $ml->date_create=date('Y-m-d H:i:s',time());       
            $ml->cost=$_POST['cost']; 
            $message.=$zone.$data."<br>Стоимость: ".$_POST['cost']." руб.<br><br>";
    		if (isset($_POST['email']))
    		{
    			$user=new User();
    			$email=$_POST['email'];                
                if (User::model()->count("email = '$email'")==0)
                {
    			 $pass=$user->generateRandomPassword();
    			 $ml->user_id=$user->createAccount('',$email,$pass,null,1,0,'','',$_POST['phone']);
                 $message.="Доступ в личный кабинет:<br>E-mail: $email<br>Временный пароль:  $pass<br><br>";                  
                }else
                {
                    $user_model=User::model()->find("email = '$email'");
                    $email=$user_model->email;
                    $ml->user_id=$user_model->id;
                }                                               
                $message.='С Уважением, Межрегиональный инстутут экспертизы МИНЭКС!';            				
    			Yii::app()->mail->send('info@minexpert.ru',$email, 'Результат онлайн расчет экспертизы',$message);
    		}else{
    		    $ml->user_id=Yii::app()->user->id;
               // $message.='С Уважением, Межрегиональный инстутут экспертизы МИНЭКС!';            				
    			//Yii::app()->mail->send('minexpert',Yii::app()->user->email, 'Результат онлайн расчет экспертизы',$message);
    		}                                                                                                                                                                                          
            $ml->save(false);       
        }
           
		Yii::app()->clientScript->registerScriptFile('/themes/default/web/js/calc.js');
        Yii::app()->clientScript->registerScriptFile('/themes/default/web/js/chosen.jquery.js');
        Yii::app()->clientScript->registerScriptFile('/themes/default/web/js/angular.min.js');
		$this->render('calc');
	}    
    public function actionReestr()
    {
        $model=null;
        $count=0;        
        $registrs=Registr::model()->findAll(array('order'=>'date_out DESC'));   
        if (count($registrs)>0){    
            foreach($registrs as $registr)
            {
                   $model[date('Y',strtotime($registr->date_out))][$count]=$registr;$count++;
            }                       
        } 
        $this->render('registr',array('model'=>$model));
    }
	private function getFileList($path)
	{
		$files = array();
		$dir = opendir($path);
		while(($currentFile = readdir($dir)) !== false){
			if ( $currentFile == '.' or $currentFile == '..' ){
				continue;
			}
			$files[] = iconv("windows-1251","UTF-8",$currentFile);
		}
		closedir($dir);
		return $files;
	}
    public function actionDocuments()
    {
		$inside_doc=Page::model()->public()->with('category')->findAll('category.alias = "vnutrennjaja-dokumentacija"');
		$normative_list=Documents::model()->findAll('category_id = 13');
		$document_for_applicants=Documents::model()->findAll('category_id = 14');
		
        $this->render('documents',array(
				'inside_page'=>$inside_doc,
				'norm_doc'   =>$normative_list,
				'doc_apl'       =>$document_for_applicants)
			);
    }
    public function actionPartnership()
    {
        $model=Page::model()->public()->find('slug = "partnership"');        
        $this->render('partnership',array('model'=>$model));
    } 
    public function actionServices($alias="")
    {                        
        $category=Category::model()->published()->findAll('is_main = 0');     
        $sub_cat=Category::model()->published()->findAll('is_main = 1');           
        if($alias!="")            
        {               
            $model=Category::model()->published()->find('alias = "'.$alias.'"');            
            if (Yii::app()->request->isAjaxRequest)
                $this->renderPartial('services_view',array('category'=>$category,'model'=>$model));
            else
                $this->render('services_view',array('category'=>$category,'model'=>$model));    
        }else        
        {
            $model=Category::model()->published()->findByPk(7);
            $this->render('services',array('category'=>$category,'sub_cat'=>$sub_cat,'body'=>$model->description));
        }
        
    }
    public function actionGetCategoryDesc()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $alias=$_POST['alias'];
            $model=Category::model()->find("alias = '$alias'");
            echo $model->short_description;
        }
    }
    public function actionGetSettings()
    {   
        if (isset($_POST['cat_id']) && Yii::app()->request->isAjaxRequest && (int)$_POST['cat_id']>0 && (int)$_POST['cat_id']<39){
        $cat_id=(int)$_POST['cat_id'];
        $spd=SpdItem::model()->findAll("category_id = $cat_id");          
            echo '<div id="getset"><br><select onchange="getInputData(this.value)" size="10" style="width:600px;">';
            foreach($spd as $item)
            { 
                if ($item->is_multi==1){
                echo '<optgroup label="'.$item->name.'">';
                    foreach($item->settings as $value)
                    {
                        echo '<option class="'.$value->comment.'" value="'.$value->id.'">'.$value->set_name->name.'</option>';
                    }
                echo '</optgroup>';  
                }else
                {
                    if ($item->setting->spd_settings_id>0){
                        echo '<option class="'.$item->setting->comment.'" value="'.$item->setting->id.'">'.$item->name.': '.$item->setting->set_name->name.'</option>';    
                    }else if ($item->setting->spd_settings_id==0)
                    {
                        echo '<option class="'.$item->setting->comment.'" value="'.$item->setting->id.'">'.$item->name.'</option>';
                    }
                    
                }  
            }
            echo '</select></div>';
         }else
         throw new CHttpException(404,'Page not found');  
    }
    private function getLimit($condition)
    {
        $min=0;
        $max=0;   
        if ($condition!=''){
                             
            $criteria=array();
            $condition=str_replace(' ','',$condition);        
                $condition=YText::translit($condition);                                                           
                if (strstr($condition,'ot')){
                    $min=(int)end(explode('ot',$condition));    
                }
                if (strstr($condition,'svyishe')){
                    $min=(int)end(explode('svyishe',$condition));    
                }
                if (strstr($condition,'do')){
                    $max=(int)end(explode('do',$condition));    
                }                        
                $criteria['min']=$min;                                
                if ($max>0)
                {
                    $criteria['max']=$max;
                }   
            return "<script>            
            $('#v_x').attr(".json_encode($criteria).");</script>";        
        }
    }
    public function actionGetInputData()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $id=$_POST['id'];            
            $dates=SpdSettings::model()->findByPk($id);
            echo $this->getLimit($dates->spd_settings_id>0?$dates->set_name->name:'');
            $content='<div id="d_cig" class="calcLabel">
                         Стоимость изготовления материалов инженерных изысканий,<br> в ценах 2001 года без НДС, в руб.
                      </div>
                      <input class="calcInp" value="0" type="text" id="v_cig" maxlength="14">
                      
                      <div id="d_x" class="calcLabel">
                        Введите основной показатель проектируемого объекта('.$dates->units.'):
                      </div> 
                      <input class="calcInp" type="number" onkeyup="validation(this);" id="v_x" value="0" maxlength="14">';            
            echo $content;
        }
    }
    public function actionCalculationAl()
    {
        Yii::import('application.components.Calc.php');        
        $calc = new Calc($_POST['etap'],$_POST['event'],$_POST);        
        echo $calc->calculation();        
    }
    public function actionKontakty()
    {
        Yii::import('application.modules.map.models.*');
        $models=Map::model()->findAll();
        $this->render('contact',array('models'=>$models));
    }
    public function actionCall()
    {
       $email=Settings::model()->findByPk(4)->param_value;
       $subject='';
       $body='';
       $modelConsult=new ConsultForm;
       $modelCallback=new CallbackForm;  
		if (isset($_POST['referer'])){
		   $referel=$_POST['referer'];
		   $phrase=$_POST['phrase']; 
	   }
       $result=array();
       if (isset($_POST['ConsultForm'])){
            $modelConsult->attributes=$_POST['ConsultForm'];
            $modelConsult->validate();
            if ($modelConsult->hasErrors())
            {
                $result['error']=true;
                $result['msg']=$modelConsult->getErrors();
                echo CJSON::encode($result);
                Yii::app()->end();
            }else
            {
                $result['error']=false;
                $result['msg']='Заявка успешно принята';
                $subject='Заказ консультации на тему:'.$modelConsult->theme;
                $body='Имя: '.$modelConsult->name.'<br>Email: '.$modelConsult->email.'<br>Номер телефона: '.$modelConsult->number.'<br>Город: '.City::getName($modelConsult->city).'<br>Компания: '.$modelConsult->company.
                '<br>Тема: '.$modelConsult->theme.'<br><br>Источник: '.$referel.'<br>Фраза: '.$phrase;
                self::sendMessage($email,$subject,$body);
                echo CJSON::encode($result);
                Yii::app()->end();
            }
       }else if(isset($_POST['CallbackForm'])){
            $modelCallback->attributes=$_POST['CallbackForm'];
            $modelCallback->validate();
            if ($modelCallback->hasErrors())
            {
                $result['error']=true;
                $result['msg']=$modelCallback->getErrors();
                echo CJSON::encode($result);
                Yii::app()->end();
            }else
            {
                $result['error']=false;
                $result['msg']='Заявка успешно принята';
                $subject='Заказ обратного звонка';
                $body='Имя: '.$modelCallback->name.'<br>Номер телефона: '.$modelCallback->number.
                '<br>Время: '.$modelCallback->time.'<br>Город: '.City::getName($modelCallback->city).'<br><br>Источник: '.$referel.'<br>Фраза: '.$phrase;;
                self::sendMessage($email,$subject,$body);
                echo CJSON::encode($result);
                Yii::app()->end();
            }
       }else if(isset($_POST['RequestForm'])){        
            $idcall=(int)$_GET['idCall'];
            if ($idcall>0){

                $mailer=new YiiMailer(); 
                $modelCalculation=Calculation::model()->findByPk($idcall);
                $modelCalculation->status=1;
                $modelCalculation->save(false);
                $subject='Заявка в электронном виде';    
                $body='Имя: '.$modelCalculation->user->getFullName().'<br>Email: '.$modelCalculation->user->email.
                '<br><br>Данные расчета: <br>'.$modelCalculation->data.'<br><br>Стоимость расчета: '.$modelCalculation->cost.' руб.<br>С уважением, администрация сайта МИНЭКС';
                $mailer->setFrom('info@minexpert.ru', 'minexpert');
                $mailer->setTo($email);
                $mailer->setSubject($subject);
                $mailer->setBody($body); 
                $form = file_get_contents('http://minexpert.ru/statement/form/'.$modelCalculation->statement->id);
                $claim = file_get_contents('http://minexpert.ru/statement/claim/'.$modelCalculation->statement->id);
                $info = file_get_contents('http://minexpert.ru/statement/info/'.$modelCalculation->statement->id);
                $inventory = file_get_contents('http://minexpert.ru/statement/inventory/'.$modelCalculation->statement->id);
                $procuratory = file_get_contents('http://minexpert.ru/statement/procuratory/'.$modelCalculation->statement->id);
                $mailer->AddStringAttachment($form,'Anketa.pdf', $encoding = 'base64', $type = 'application/pdf');
                $mailer->AddStringAttachment($claim,'Zayavlenie.pdf', $encoding = 'base64', $type = 'application/pdf');
                $mailer->AddStringAttachment($info,'Svedenia.pdf', $encoding = 'base64', $type = 'application/pdf');
                $mailer->AddStringAttachment($inventory,'Opis.pdf', $encoding = 'base64', $type = 'application/pdf');
                $mailer->AddStringAttachment($procuratory,'Doverennost.pdf', $encoding = 'base64', $type = 'application/pdf');
                $mailer->send();
                
                //self::sendMessage($email,$subject,$body);
            }else if(isset($_GET['idState']) && (int)$_GET['idState']>0){
                
                 $mailer=new YiiMailer(); 
                $modelStatement=Statement::model()->findByPk($_GET['idState']);
                $modelUser=User::model()->findByPk(Yii::app()->user->id);
                $subject='Заявка в электронном виде';    
                $body='Имя: '.$modelUser->getFullName().'<br>Email: '.$modelUser->email;
                $mailer->AltBody='С уважением, администрация сайта МИНЭКС';
                if ($modelStatement->calculate_id>0){
                    $modelCalculation=Calculation::model()->findByPk($modelStatement->calculate_id);
                    $body.='<br><br>Данные расчета: <br>'.$modelCalculation->data.'<br><br>Стоимость расчета: '.$modelCalculation->cost.' руб.';
                    $modelCalculation->status=1;
                    $modelCalculation->save(false);
                }                
                $mailer->setFrom('info@minexpert.ru', 'minexpert');
                $mailer->setTo($email);
                $mailer->setSubject($subject);
                $mailer->setBody($body);                 
                $form = file_get_contents('http://minexpert.ru/statement/form/'.$modelStatement->id);
                $claim = file_get_contents('http://minexpert.ru/statement/claim/'.$modelStatement->id);
                $info = file_get_contents('http://minexpert.ru/statement/info/'.$modelStatement->id);
                $inventory = file_get_contents('http://minexpert.ru/statement/inventory/'.$modelStatement->id);
                $procuratory = file_get_contents('http://minexpert.ru/statement/procuratory/'.$modelStatement->id);
                $mailer->AddStringAttachment($form,'Anketa.pdf', $encoding = 'base64', $type = 'application/pdf');
                $mailer->AddStringAttachment($claim,'Zayavlenie.pdf', $encoding = 'base64', $type = 'application/pdf');
                $mailer->AddStringAttachment($info,'Svedenia.pdf', $encoding = 'base64', $type = 'application/pdf');
                $mailer->AddStringAttachment($inventory,'Opis.pdf', $encoding = 'base64', $type = 'application/pdf');
                $mailer->AddStringAttachment($procuratory,'Doverennost.pdf', $encoding = 'base64', $type = 'application/pdf');
                $mailer->send();
            }
            echo CJSON::encode(array('ok'=>true));                        
            Yii::app()->end();
       }
    }
    public function actionGetZone($id=0)
    {
        if ($id>0 && Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(CHtml::listData(SeismicZones::model()->findAll("region_id = $id"),'id','city'));
            Yii::app()->end();                        
        }        
    }
    private function sendMessage($email,$subject,$body){
        $mailer=new YiiMailer();        
        $mailer->setFrom('info@minexpert.ru', 'minexpert');
        $mailer->setTo($email);
        // $mailer->setTo($email);
         $mailer->setSubject($subject);
         $mailer->setBody($body);
         $mailer->send();
        }
}