<?php
class StatementController extends YFrontController
{
    public $pdf;
    public $defaultAction = 'index';
//    public function filters()
//    {
//        return array(
//            'accessControl',
//        );
//    }
//    public function accessRules()
//    {
//        return array(
//        array('allow',   allow all users
//            'users'=>array('@'),
//        ),
//            array('deny',   deny all not-auth users
//                'users'=>array('*'),
//            ),
//        );
//    }
    public function init()
    {
        $this->pdf = Yii::app()->ePdf->mpdf();
        parent::init();
    }
    public function actionIndex($id=0)
    {   
		 $email=Settings::model()->findByPk(4)->param_value;
		$model = new Statement();

        //посупила новая организация
        if(isset($_POST['Org']))
        {
            $model = new Org();
            if (isset($_POST['Org']))
            {
                $model->attributes=$_POST['Org'];
                $model->user_id=Yii::app()->user->id;
                if ($model->save())
                {
                    $this->redirect('/user/cabinet/index#tabs-2');
                }
            }
            $this->render('modalForm/addOrg',array('model'=>$model,'id'=>$id));
        }


 

		if (isset($_POST['Statement']))
		{
			 $model->attributes=$_POST['Statement'];			 
			 if (isset($_POST['nameDoc'])){
				$inventory_name=$_POST['nameDoc'];
				$inventory_countItems=$_POST['countItems'];					
				for($i=1;$i<=count($inventory_name);$i++)
				{
					$inventory[$i]['nameDoc']=$inventory_name[$i];
					$inventory[$i]['countItems']=$inventory_countItems[$i];
				}	
				$model->json_inventory=CJSON::encode($inventory);
			 }			
			 $model->user_id=Yii::app()->user->id;
			 $model->calculate_id=$id;
			if ($model->save())
			{
//				$this->redirect('/user/cabinet/index#tabs-2');
			}	
			
			/* if ($id>0){
                $modelCalculation=Calculation::model()->findByPk($id);
                $modelCalculation->status=1;
                $modelCalculation->save(false);
                $subject='Заявка в электронном виде';    
                $body='Имя: '.$modelCalculation->user->getFullName().'<br>Email: '.$modelCalculation->user->email.
                '<br><br>Данные расчета: <br>'.$modelCalculation->data.'<br><br>Стоимость расчета: '.$modelCalculation->cost.' руб.';
                self::sendMessage($email,$subject,$body);
            }	 */		
		}

        $this->render('form',array('model'=>$model,'id'=>$id));
    }
	public function actionUpdate($id,$cal_id=0)
	{
		$model=$this->loadModel($id);
		if (isset($_POST['Statement']))
		{
			 $model->attributes=$_POST['Statement'];			 
			 if (isset($_POST['nameDoc'])){
				$inventory_name=$_POST['nameDoc'];
				$inventory_countItems=$_POST['countItems'];					
				for($i=1;$i<=count($inventory_name);$i++)
				{
					$inventory[$i]['nameDoc']=$inventory_name[$i];
					$inventory[$i]['countItems']=$inventory_countItems[$i];
				}	
				$model->json_inventory=CJSON::encode($inventory);
			 }
			 if ($model->save())
			{
				$this->redirect('/user/cabinet/index#tabs-2');
			}			 
		}
		  $this->render('form',array('model'=>$model,'id'=>$cal_id));
	}	
    public function actionForm($id) //Анкета
    {
//        var_dump($this->loadModel($id));
//        die();
        $this->pdf->WriteHTML($this->renderPartial('forms/anketa',array('model'=>$this->loadModel($id)),true));   
        $this->pdf->Output('anketa.pdf','D');     
	}
    public function actionClaim($id) //Заявление
    {
        $this->pdf->WriteHTML($this->renderPartial('forms/zayvlenie',array('model'=>$this->loadModel($id)),true));   
        $this->pdf->Output('zayavlenie.pdf','D'); 
    }
    public function actionInfo($id) // Сведения
    {
        $this->pdf->WriteHTML($this->renderPartial('forms/svedenia',array('model'=>$this->loadModel($id)),true));   
        $this->pdf->Output('svedeniya.pdf','D'); 
    }
	public function actionInventory($id) //Опись
	{
		$this->pdf->WriteHTML($this->renderPartial('forms/opis',array('model'=>$this->loadModel($id)),true));
		$this->pdf->Output('opis.pdf','D'); 
	}
	public function actionProcuratory($id) //Доверенность
	{
		$this->pdf->WriteHTML($this->renderPartial('forms/doverennost',array('model'=>$this->loadModel($id)),true));
		$this->pdf->Output('doverennost.pdf','D'); 
	}
	protected function loadModel($id){return Statement::model()->findByPk($id);}
	 private function sendMessage($email,$subject,$body){Yii::app()->mail->send('minexpert',$email,$subject,$body);}
}