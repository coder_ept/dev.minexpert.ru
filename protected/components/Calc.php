<?php
class Calc
{    
    //const Ki = 3.526;
    const Ki = 3.755;     
    private $type;
    private $variant;    
    private $result=0; 
    private $intermedia=array();    
    function __construct($type,$variant,$intermedia){
        $this->type=$type;
        $this->variant=$variant;
        $this->intermedia=$intermedia;
    }
     public function calculation(){
      //  if (in_array(0,$this->intermedia))        
        //    $this->Exception('Проверьте правильность заполнение формы');        
        $this->result=call_user_func(array(get_class($this), 'VariantMode'.$this->variant));                 
        return round(($this->type==1)?$this->result*self::getKc():($this->result*0.3)*self::getKc(),2);
    }
    private function Exception($msg){throw new CHttpException(500,$msg);}
    private function getPercent($sum)
    {
        $p=0;
        if ($sum > 0 && $sum <= 150000) { 
				    $p = 33.75;
    				} else if ($sum <= 250000) { $p = 29.25;
    				} else if ($sum <= 500000) { $p = 27.3;
    				} else if ($sum <= 750000) { $p = 20.22;
    				} else if ($sum <= 1000000) { $p = 16.65;
    				} else if ($sum <= 1500000) { $p = 12.69;
    				} else if ($sum <= 3000000) { $p = 11.88;
    				} else if ($sum <= 4000000) { $p = 10.98;
    				} else if ($sum <= 6000000) { $p = 8.77;
    				} else if ($sum <= 8000000) { $p = 7.07;
    				} else if ($sum <= 12000000) { $p = 6.15;
    				} else if ($sum <= 18000000) { $p = 4.76;
    				} else if ($sum <= 24000000) { $p = 4.13;
    				} else if ($sum <= 30000000) { $p = 3.52;
    				} else if ($sum <= 36000000) { $p = 3.06;
    				} else if ($sum <= 45000000) { $p = 2.62;
    				} else if ($sum <= 52500000) { $p = 2.33;
    				} else if ($sum <= 60000000) { $p = 2.01;
    				} else if ($sum <= 70000000) { $p = 1.68;
    				} else if ($sum <= 80000000) { $p = 1.56;
    				} else if ($sum <= 100000000) { $p = 1.22;
    				} else if ($sum <= 120000000) { $p = 1.04;
    				} else if ($sum <= 140000000) { $p = 0.9;
    				} else if ($sum <= 160000000) { $p = 0.8;
    				} else if ($sum <= 180000000) { $p = 0.73;
    				} else if ($sum <= 200000000) { $p = 0.66;
    				} else if ($sum <= 220000000) { $p = 0.61;
    				} else { $p = 0.58;
				}
				return ($p / 100);
    }   
    private function get_spd($id)
    {
        $interData=array();
        $model=SpdSettings::model()->findByPk($id);
        $x=$this->intermedia['x'];       
        $interData[0]=$model->a*1000;
        $interData[1]=$model->b*1000;                 
        return ($interData[0] + $interData[1] * $x)*0.4;        
    }   
    private function getKc()
    {        
        if (SeismicZones::model()->count('city = :zone_id',array(':zone_id'=>$this->intermedia['zone']))>0){
        $modelZone=SeismicZones::model()->find('city = :zone_id',array(':zone_id'=>$this->intermedia['zone']));
        $modelZone=$modelZone->a;
        if ($modelZone==7){
            return 1.15;
        }elseif($modelZone==8){
            return 1.2;
        }elseif($modelZone==9)
        {
            return 1.3;
        }else{
            return 1;
        }
        }else
            return 1;
    } 
    private function VariantMode1(){        
        return ((5 * $this->intermedia['xg'])+13000)*self::Ki;
    }
    private function VariantMode2(){        
        $kn=1;$kc=1;        
        $xg=$this->intermedia['xg'];
        $yg=$this->intermedia['yg'];        
        if(isset($this->intermedia['t1']) && $this->intermedia['t1']>0){
            $kn=($this->intermedia['t1']==2)?0.5:1;
        }        
        return (100000+($xg*35 + $yg*3.5))*  $kn * self::Ki;
    }
    private function VariantMode3()
    {
        $kn=1;$kc=1;        
        $xg=$this->intermedia['xg'];
        $yg=$this->intermedia['yg'];  
        if(isset($this->intermedia['t1']) && $this->intermedia['t1']>0){
            $kn=($this->intermedia['t1']==2)?0.5:1;
        }
        $a=(100000 + $xg*35 + $yg*3.5) * $kn * self::Ki;
        $b=($xg*5 + 13000) * self::Ki;        
        return ($a+$b)*0.9;
    }
    private function VariantMode4()
    {        
        $spd=self::get_spd($this->intermedia['exp_table']);
        $sig=$this->intermedia['sig'];                                             
        return ($sig*self::getPercent($sig) + $spd*self::getPercent($spd))*self::Ki;
    }        
}
?>